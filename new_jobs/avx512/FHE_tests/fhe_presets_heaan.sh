#!/bin/bash
# If you run scripts, change this or else - I don't want to be spammed with emails
# Parameters - GPU queue, use all available cores cause it's our computer
#$ -M nkoirala@nd.edu
#$ -m abe
#$ -q gpu@@jung_gpu
#$ -l gpu_card=1
#$ -N bootstrapping_avx512_HEAAN
#$ -pe smp 1
#Exit on error
set -e
#Set Singularity container path

CONTAINER=/scratch365/${SGE_O_LOGNAME}/ubuntu2004-cuda-nolibs.img
#And check that it's there

file $CONTAINER
#Run from root directory

EXECUTABLE=bin/main
#Bash needs quotes for space-separated values (make doesn't)

#TESTS="polyeval"
#TESTS="dot"
#TESTS="bootstrap"
#TESTS="add mult chain mult_no_relin rotate relin rescale size_ctext size_public_key size_secret_key size_params"

TESTS="add mult chain mult_no_relin rotate relin rescale size_ctext size_public_key size_secret_key size_params rotdot dot linear polyeval bootstrap"

#TESTS="add mult chain mult_no_relin rotate relin rescale size_ctext size_public_key size_secret_key size_params rotdot dot linear polyeval"
ITERATIONS="-i 5"

DIMENSION="-m 64"
DOT_LEN="-o 1024"
CUSTOM_COEFFS="-c utilities/logistic_func.txt"
#0 is basic, 1 is native
NATIVE="-n 0"
#DEPTH="-d 5"

SUBFOLDER=avx512/bootstrappingHEAAN

module load gcc/11.1.0
g++ utilities/stats.cpp -o bin/stats -Wall -Werror -pedantic -O3 -march=native

mkdir -p ./results/${SUBFOLDER}/
rm -f ./results/${SUBFOLDER}/*

#module load gcc/11.1.0 fhe-libs/avx2
#First, recompile on the machine in question

singularity exec $CONTAINER make AVX_VERSION=512 clean all

#Run the test
for i in {1..3}
do
  LIBRARIES="-l heaan"
  if [ $i -eq 1 ]
  then
     LIBRARIES="-l heaan"
  fi
 B_LEVEL="-b $i"
 echo $B_LEVEL
 singularity exec $CONTAINER $EXECUTABLE $TESTS $LIBRARIES $ITERATIONS $DIMENSION $DOT_LEN $CUSTOM_COEFFS $NATIVE $B_LEVEL > ./results/${SUBFOLDER}/results_${i}.csv 2> ./results/${SUBFOLDER}/err.txt
 echo $i >> ./results/${SUBFOLDER}/report.csv
 bin/stats < ./results/${SUBFOLDER}/results_${i}.csv >> ./results/${SUBFOLDER}/report.csv
 echo "" >> ./results/${SUBFOLDER}/report.csv
done

echo "Results sent out to results/${SUBFOLDER}/report.csv"
module unload gcc/11.1.0
