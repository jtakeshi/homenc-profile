#!/bin/bash

# If you run scripts, change this or else - I don't want to be spammed with emails
# Parameters - GPU queue, use all available cores cause it's our computer
#$ -M nkoirala@nd.edu
#$ -m abe
#$ -q gpu@@jung_gpu
#$ -l gpu_card=1
#$ -N threads_avx512
#$ -pe smp 24

#Exit on error
set -e

#Set Singularity container path
CONTAINER=/scratch365/${SGE_O_LOGNAME}/ubuntu2004-cuda-nolibs.img
#And check that it's there
file $CONTAINER
#Allow Singularity to see the GPU
export SINGULARITYENV_CUDA_VISIBLE_DEVICES=${CUDA_VISIBLE_DEVICES}

#Run from root directory
EXECUTABLE=bin/main

#Bash needs quotes for space-separated values (make doesn't)

TESTS="add mult mult_no_relin rotate relin rescale chain size_ctext size_public_key size_secret_key size_keyswitch_key size_params rotdot linear dot polyeval ctext_modulus_bits encode_error scaling_factor num_columns"

LIBRARIES="-l heaan -l helib -l palisade -l palisade_opt -l seal"
ITERATIONS="-i 50"

DIMENSION="-m 64"
CUSTOM_COEFFS="-c utilities/logistic_func.txt"
DEPTH="-d 5"
DOT_LEN="-o 1024"
DIMENSION="-m 64"
NATIVE="-n 0"

SUBFOLDER=avx512/threads

module load gcc/11.1.0
g++ utilities/stats.cpp -o bin/stats -Wall -Werror -pedantic -O3 -march=native

mkdir -p ./results/${SUBFOLDER}/
rm -f ./results/${SUBFOLDER}/*
#module load gcc/11.1.0 fhe-libs/avx2

#First, recompile on the machine in question
# HEAAN_VERSION=-gpu
singularity exec $CONTAINER make AVX_VERSION=512 clean all

#Run the test
for i in {0..24..4}
do
  if [ $i -eq 0 ]
  then
     i=1
  fi
  #THREADS="-s $i"
  SINGULARITYENV_OMP_NUM_THREADS=$i singularity exec --nv $CONTAINER $EXECUTABLE $TESTS $LIBRARIES $ITERATIONS $DIMENSION $CUSTOM_COEFFS $DOT_LEN $DIMENSION $DEPTH> ./results/${SUBFOLDER}/results_${i}.csv 2> ./results/${SUBFOLDER}/err.txt
  echo $i >> ./results/${SUBFOLDER}/report.csv
  bin/stats < ./results/${SUBFOLDER}/results_${i}.csv >> ./results/${SUBFOLDER}/report.csv
  echo "" >> ./results/${SUBFOLDER}/report.csv
done

echo "Results sent out to results/${SUBFOLDER}/report.csv"

#Unload modules
module unload gcc/11.1.0
