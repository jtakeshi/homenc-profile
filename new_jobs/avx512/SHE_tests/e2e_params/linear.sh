#!/bin/bash

# If you run scripts, change this or else - I don't want to be spammed with emails
# Parameters - GPU queue, use all available cores cause it's our computer
#$ -M nkoirala@nd.edu
#$ -m abe
#$ -q gpu@@jung_gpu
#$ -l gpu_card=1
#$ -N linear_avx512
#$ -pe smp 1

#Exit on error
set -e

#Set Singularity container path
CONTAINER=/scratch365/${SGE_O_LOGNAME}/ubuntu2004-cuda-nolibs.img
#And check that it's there
file $CONTAINER

#Run from root directory
EXECUTABLE=bin/main

#Bash needs quotes for space-separated values (make doesn't)
#Bootsrapping omitted!
TESTS="linear"
LIBRARIES="-l heaan -l helib -l palisade -l palisade_opt -l seal"
ITERATIONS="-i 50"
DIMENSION="-m 16"
DEPTH="-d 1"

SUBFOLDER=avx512/linear

module load gcc/11.1.0
g++ utilities/stats.cpp -o bin/stats -Wall -Werror -pedantic -O3 -march=native

mkdir -p ./results/${SUBFOLDER}/
rm -f ./results/${SUBFOLDER}/*
#module load gcc/11.1.0 fhe-libs/avx2

#First, recompile on the machine in question
singularity exec $CONTAINER make AVX_VERSION=512 clean all

#Run the test
for i in {16..96..10}
do
  DIMENSION="-m $i"
  singularity exec $CONTAINER $EXECUTABLE $TESTS $LIBRARIES $ITERATIONS $DIMENSION $DEPTH> ./results/${SUBFOLDER}/results_${i}.csv 2> ./results/${SUBFOLDER}/err.txt
  echo $i >> ./results/${SUBFOLDER}/report.csv
  bin/stats < ./results/${SUBFOLDER}/results_${i}.csv >> ./results/${SUBFOLDER}/report.csv
  echo "" >> ./results/${SUBFOLDER}/report.csv
done

echo "Results sent out to results/${SUBFOLDER}/report.csv"

#Unload modules
module unload gcc/11.1.0
