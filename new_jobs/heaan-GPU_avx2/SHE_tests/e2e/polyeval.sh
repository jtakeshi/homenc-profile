#!/bin/bash

# If you run scripts, change this or else - I don't want to be spammed with emails
# Parameters - GPU queue, use all available cores cause it's our computer
#$ -M nkoirala@nd.edu
#$ -m abe
#$ -q gpu@@jung_gpu
#$ -l gpu_card=1
#$ -N polyeval_avx2_GPU
#$ -pe smp 1

#Exit on error
set -e

#Set Singularity container path
CONTAINER=/scratch365/${SGE_O_LOGNAME}/ubuntu2004-cuda-nolibs.img
#And check that it's there
file $CONTAINER

export SINGULARITYENV_CUDA_VISIBLE_DEVICES=${CUDA_VISIBLE_DEVICES}

#Run from root directory
EXECUTABLE=bin/main

#Bash needs quotes for space-separated values (make doesn't)
#Bootsrapping omitted!
TESTS="polyeval"
LIBRARIES="-l heaan"
ITERATIONS="-i 50"
DEPTH="-d 1"
#COEFFICIENT="-c 4"
#0 is basic, 1 is native
NATIVE="-n 0"

SUBFOLDER=avx2_GPU/e2e_params/polyeval

module load gcc/11.1.0
g++ utilities/stats.cpp -o bin/stats -Wall -Werror -pedantic -O3 -march=native

mkdir -p ./results/${SUBFOLDER}/
rm -f ./results/${SUBFOLDER}/*
#module load gcc/11.1.0 fhe-libs/avx2

#First, recompile on the machine in question
singularity exec $CONTAINER make AVX_VERSION=2 HEAAN_TYPE=-gpu clean all

#Run the test
for i in {4..16..2}
do
  COEFFICIENT="-c $i"
  tD=$(python utilities/get_log.py $i)
  DEPTH="-d $tD"
  singularity exec --nv $CONTAINER $EXECUTABLE $TESTS $LIBRARIES $ITERATIONS $COEFFICIENT $NATIVE $DEPTH > ./results/${SUBFOLDER}/results_${i}.csv 2> ./results/${SUBFOLDER}/err.txt
  echo $i >> ./results/${SUBFOLDER}/report.csv
  bin/stats < ./results/${SUBFOLDER}/results_${i}.csv >> ./results/${SUBFOLDER}/report.csv
  echo "" >> ./results/${SUBFOLDER}/report.csv
done

echo "Results sent out to results/${SUBFOLDER}/report.csv"

#Unload modules
module unload gcc/11.1.0


