#!/usr/bin/bash
# Jonathan S. Takeshita
# University of Notre Dame
set -e
set -x

SLEEPTIME=30 #Check every 30 seconds so as to not spam the system
LINE_COUNT=1

# Input should be from STDIN. Each line has a list of space-separated job script names (full path required!).
# Scripts on the same line will be submitted simultaneously. Each line will be submitted in order.
# Not yet tested.

# https://stackoverflow.com/questions/6980090/how-to-read-from-a-file-or-standard-input-in-bash
while read SC; do
  echo "Starting jobs [$SC] (line $LINE_COUNT) at $(date)"
  JOB_ARRAY=($SC)
  for JOB in "${JOB_ARRAY[@]}"
  do
    qsub $SC
  done  
  sleep $SLEEPTIME
  QUEUE_OUTPUT=$(qstat -u $LOGNAME) 
  while [ ! -z "$QUEUE_OUTPUT" ]; do
    sleep $SLEEPTIME
    QUEUE_OUTPUT=$(qstat -u $LOGNAME) 
  done
  echo "Finished jobs [$SC] (line $LINE_COUNT) at $(date)"
  ((LINE_COUNT++))
done < "${1:-/dev/stdin}"  
