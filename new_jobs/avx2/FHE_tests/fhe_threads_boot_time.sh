#!/bin/bash

# If you run scripts, change this or else - I don't want to be spammed with emails
# Parameters - GPU queue, use all available cores cause it's our computer
#$ -M nkoirala@nd.edu
#$ -m abe
#$ -q gpu@@jung_gpu
#$ -l gpu_card=1
#$ -N threads_bootstrap_avx2
#$ -pe smp 24

#Exit on error
set -e

#Set Singularity container path
CONTAINER=/scratch365/${SGE_O_LOGNAME}/ubuntu2004-cuda-nolibs.img
#And check that it's there
file $CONTAINER
#Allow Singularity to see the GPU
export SINGULARITYENV_CUDA_VISIBLE_DEVICES=${CUDA_VISIBLE_DEVICES}

#Run from root directory
EXECUTABLE=bin/main

#Bash needs quotes for space-separated values (make doesn't)
#Bootsrapping omitted!
TESTS="bootstrap"
LIBRARIES="-l palisade -l heaan -l palisade_opt"
#Currently, HEAAN and PALISADE are the only libraries with bootstrapping exposed

ITERATIONS="-i 50"
B_LEVEL="-b 2"
#TODO set me to something larger

SUBFOLDER=avx2/bootstrapping/threads

module load gcc/11.1.0
g++ utilities/stats.cpp -o bin/stats -Wall -Werror -pedantic -O3 -march=native

mkdir -p ./results/${SUBFOLDER}/
rm -f ./results/${SUBFOLDER}/*
#module load gcc/11.1.0 fhe-libs/avx2

#First, recompile on the machine in question
# HEAAN_VERSION=-gpu
singularity exec $CONTAINER make AVX_VERSION=2 clean all

#Run the test
for i in {0..24..4}
do
  if [ $i -eq 0 ]
  then
     i=1
  fi
  SINGULARITYENV_OMP_NUM_THREADS=$i singularity exec --nv $CONTAINER $EXECUTABLE $TESTS $LIBRARIES $ITERATIONS $B_LEVEL> ./results/${SUBFOLDER}/results_${i}.csv 2> ./results/${SUBFOLDER}/err.txt
  echo $i >> ./results/${SUBFOLDER}/report.csv
  bin/stats < ./results/${SUBFOLDER}/results_${i}.csv >> ./results/${SUBFOLDER}/report.csv
  echo "" >> ./results/${SUBFOLDER}/report.csv
done

echo "Results sent out to results/${SUBFOLDER}/report.csv"

#Unload modules
module unload gcc/11.1.0
