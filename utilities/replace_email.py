#!/usr/bin/python3
import sys

def main():
  if len(sys.argv) != 2:
    print('ERROR: argue exactly one email to be used', file=sys.stderr)
    sys.exit(1)
  email = sys.argv[1]
  target = '#$ -M'
  for line in sys.stdin:
    if line.startswith(target):
      oline = target + ' ' + email
      print(oline)
    else:
      print(line.strip())  

if __name__ == '__main__':
  main()
