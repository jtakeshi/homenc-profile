#include <ciphertext-ser.h>
#include <cmath>
#include <cstdint>
#include <cryptocontext.h>
#include <cstdint>
#include <palisade.h>
#include <palisade/pke/palisade.h>
#include <scheme/ckks/ckks-ser.h>
#include <utils/serial.h>

// For bootstrapping only - might want to hide this with a flag
#include "dlt-palisade.h"

using std::unique_ptr;
using namespace std;

// Code stolen as needed from PALISADEProfiler

uint32_t getDegreeFromBSLevel(const unsigned int lev) {
  if (lev > 0 && lev <= 3) {
    return 1 << (14 + lev);
  }
  throw std::runtime_error("Bad BS level");
  return 0;
}

int main(int argc, char **argv) {
  lbcrypto::SecurityLevel sec = lbcrypto::SecurityLevel::HEStd_128_classic;
  lbcrypto::CryptoContext<lbcrypto::DCRTPoly> context;
  unique_ptr<lbcrypto::LPKeyPair<lbcrypto::DCRTPoly>> keyPair;

  for(size_t b_level = 2; b_level <= 3; b_level++){
    cout << "\t Level " << b_level << endl;
    uint32_t n = getDegreeFromBSLevel(b_level);
    if (n < (1 << 16)) {
      throw std::runtime_error("Need n >= 2^16");
    }
    uint32_t slots = n / 2;
    uint32_t levelsRemaining = 4;

    // Code examples shamelessly stolen from the PALISADE CKKS Bootstrapping
    // example
    std::vector<uint32_t> dim1 = {0, 0};
    std::vector<uint32_t> levelBudget = {4, 4};
#if NATIVEINT == 128
    lbcrypto::RescalingTechnique rescaleTech = lbcrypto::APPROXRESCALE;
#else
    lbcrypto::RescalingTechnique rescaleTech = lbcrypto::EXACTRESCALE;
#endif
    // computes how many levels are needed for
    uint32_t approxModDepth = 9;
    uint32_t r = 6;
    MODE mode = OPTIMIZED;
    if (mode == OPTIMIZED) {
      if (rescaleTech == lbcrypto::APPROXRESCALE) {
        approxModDepth += r - 1;
      } else {
        approxModDepth += r;
      }
    }

    usint depth =
        levelsRemaining + approxModDepth + levelBudget[0] + levelBudget[1];
#if NATIVEINT == 128
    usint dcrtBits = 78;
#else
    usint dcrtBits = 59;
#endif
    lbcrypto::DltCryptoContext<lbcrypto::DCRTPoly> boot_cc =
        lbcrypto::DltCryptoContextFactory<lbcrypto::DCRTPoly>::
            genCryptoContextCKKS(depth, dcrtBits, slots,
                                 lbcrypto::HEStd_128_classic, n, rescaleTech,
                                 lbcrypto::HYBRID, 3, 2,
#if NATIVEINT == 128
                                 89,
#else
                                 60,
#endif
                                 0, mode);

    // Turn on features
    boot_cc->Enable(ENCRYPTION);
    boot_cc->Enable(SHE);
    boot_cc->Enable(LEVELEDSHE);
    boot_cc->Enable(ADVANCEDSHE);
    boot_cc->Enable(FHE);

    const shared_ptr<lbcrypto::LPCryptoParametersCKKS<lbcrypto::DCRTPoly>>
        cryptoParams = std::static_pointer_cast<
            lbcrypto::LPCryptoParametersCKKS<lbcrypto::DCRTPoly>>(
            boot_cc->GetCryptoParameters());
    boot_cc->EvalBTSetup(levelBudget, dim1, slots);
    auto boot_keys = boot_cc->KeyGen();
    int32_t sparseFlag = (slots < n / 2) ? 1 : 0;
    boot_cc->EvalBTKeyGen(boot_keys.secretKey, sparseFlag);
    boot_cc->EvalMultKeyGen(boot_keys.secretKey);

    // Get a random vector
    std::vector<std::complex<double>> input(slots);
    for (auto & x : input) {
      x = 1;
    }

    size_t encodedLength = input.size();
    lbcrypto::Plaintext plaintext1 =
        boot_cc->MakeCKKSPackedPlaintext(input, 1, depth - 1);

    auto ct = boot_cc->Encrypt(boot_keys.publicKey, plaintext1);
    for (size_t i = 0; i < 20; i++) {
      ct = boot_cc->EvalMult(ct, ct);
      boot_cc->RescaleInPlace(ct);

      auto ciphertextAfter = boot_cc->EvalBT(ct);
      lbcrypto::Plaintext pt;
      context->Decrypt(keyPair->secretKey, ciphertextAfter, &pt);
      vector<double> enc_result;
      enc_result = pt->GetRealPackedValue();

      cout << "Completed " << i << " multiplications, result after bootstrapping is " << enc_result[0] << endl;
    }
  }

  

  return 0;
}