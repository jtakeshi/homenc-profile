// g++ helibdemo.cpp -std=c++17 -Wall -Werror /usr/local/lib/libseal-3.7.a -I
// /usr/local/include/SEAL-3.7/ -pthread -Wl,-rpath=/usr/local/lib/ -I
// /usr/local/helib_pack/include/ -I /usr/local/helib_pack/include/helib/ -L
// /usr/local/lib -Wl,-rpath=/usr/local/helib_pack/lib/ -lntl -lgmp
// /usr/local/helib_pack/lib/libhelib.a

#include <cassert>
#include <helib/helib.h>
#include <stdio.h>

using namespace std;
using namespace helib;

// write a loop to chain multiply ciphertext and print the details and find out
// when it crashes

int main(int argc, char **argv) {

  int m, bits, columns;
  assert(argc == 4);
  m = atoi(argv[1]);
  bits = atoi(argv[2]);
  columns = atoi(argv[3]);

  // To get started, we need to choose some parameters.  This is done by
  // initializing a Context object.  Since there are a lot of parameters, many
  // of them optional, HElib provides a "builder pattern" than lets you provide
  // these parameters "by name".

  Context context =

      // initialize a Context object using the builder pattern

      ContextBuilder<CKKS>()

          .m(m)
          // m is the "cyclotomic index". For CKKS, m must be a power of 2.  As
          // m increases, you get more security and more slots, but the
          // performance degrades and the size of a ciphertext increases. See
          // table below for more information.

          .bits(bits)
          // bits specifies the number of bits in the "ciphertext modulus".  As
          // bits increases, you get less security, but you can perform deeper
          // homomorphic computations; in addition, the size of a ciphertext
          // increases.  See table below for more information. Also see
          // 02_depth.cpp for more information about how depth and bits are
          // related.

          //.precision(20)
          // precision specifies the number of bits of precision when data is
          // encoded, encrypted, or decrypted.  More precisely, each of these
          // operations are designed to add an error term of at most
          // 2^{-precision} to each slot.  As precision increases, the allowed
          // depth of homomorphic computations decreases (but security and
          // performance are not affected).  It is not recommended to use
          // precision greater than about 40 or so.

          .c(columns)
          // c specifies the number of columns in key-switching matrices.  Yes,
          // it sounds very technical, and it is.  However, all you have to know
          // about this parameter is that as c increases, you get a little more
          // security, but performance degrades and the memory requirement for
          // the public key increases. c must be at least 2 and it is not
          // recommended to set c higher than 8.  See table below for more
          // information.

          .build();
  // last step of the builder pattern

  size_t slot_count = context.getNSlots();
  srand(time(NULL));
  vector<double> args;
  args.clear();
  args.resize(slot_count);

  for (size_t i = 0; i < args.size(); i++) {
    // args[i] = (double)rand() / RAND_MAX;
    args[i] = 1.0f;
  }
  helib::Ptxt<helib::CKKS> p(context, args);

  helib::SecKey secret_key(context);
  secret_key.GenSecKey();

  helib::PubKey public_key(secret_key);
  helib::Ctxt c(public_key);
  public_key.Encrypt(c, p);

  helib::PtxtArray pt(context);
  vector<double> enc_result;

  unsigned int depth = 100;

  for (unsigned int j = 0; j < depth; j++) {
    c *= c;
    cout << "c.capacity=" << c.capacity() << " ";
    cout << "c.errorBound=" << c.errorBound() << "\n";
    cout << "securityLevel=" << context.securityLevel() << "\n";

    pt.decrypt(c, secret_key);
    pt.store(enc_result);

    cout << "First element: " << enc_result[0] << "\n";
    cout << "Current Depth: " << j << "\n";
    cout << "\n" << endl;
  }

  // The following table lists settings of m, bits, and c that yield (at least)
  // 128-bit security.  It is highly recommended to only use settings from this
  // table.
  //
  //	m	bits	c
  //	16384	119	2
  //	32768	358	6
  //	32768	299	3
  //	32768	239	2
  //	65536	725	8
  //	65536	717	6
  //	65536	669	4
  //	65536	613	3
  //	65536	558	2
  //	131072	1445	8
  //	131072	1435	6
  //	131072	1387	5
  //	131072	1329	4
  //	131072	1255	3
  //	131072	1098	2
  //	262144	2940	8
  //	262144	2870	6
  //	262144	2763	5
  //	262144	2646	4
  //	262144	2511	3
  //	262144	2234	2

  return 0;
}
