#include <iostream>
#include <vector>
#include <cmath>
#include <string>
#include <sstream>
#include <exception>

using namespace std;

template<typename T>
T mean(const vector<T> & arr){
  T ret = 0;
  for(const T & x : arr){
    ret += x;
  }
  return ret / arr.size();
}

template<typename T>
T stdev(const vector<T> & arr, const T precomputed_mean){
  T ret = 0;
  for(const T & x : arr){
    T tmp = x - precomputed_mean;
    ret += tmp*tmp;
  }
  return sqrt(ret / arr.size());
}

template<typename T>
T stdev(const vector<T> & arr){
  T m = mean(arr);
  return stdev(arr, m);
}

int main(int argc, char ** argv){

  static const unsigned int RESERVE_GUESS = 100000;

  string line;
  vector<long double> dat;
  dat.reserve(RESERVE_GUESS);
  vector<string> labels;
  while(getline(cin, line)){
    
    stringstream ss(line);
    string sval, colname;    
    while(getline(ss, sval, ',')){
      try{
        long double d = std::stold(sval);
        dat.push_back(d);
      }
      catch(std::invalid_argument & e){
        labels.push_back(sval);
        continue;
      }
    }
    long double avg = mean(dat);
    long double sd = stdev(dat, avg);
    for(const string & s : labels){
      cout << s << ',';
    }
    cout << avg << ',' << sd << endl;
    //Doesn't change capacity
    dat.clear();
    labels.clear();
  }

  return 0;
}
