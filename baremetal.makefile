# I don't think we need binfhe - TODO try taking it out
#
# Default to AVX2
AVX_VERSION=2
FHE_LIBS_DIR=/afs/crc.nd.edu/group/dsp-lab/fhe-libraries/avx${AVX_VERSION}/local
PALISADE_INCLUDES=-I ${FHE_LIBS_DIR}/palisade/include/palisade -I ${FHE_LIBS_DIR}/palisade/include/palisade/cereal -I ${FHE_LIBS_DIR}/palisade/include/palisade/core -I ${FHE_LIBS_DIR}/palisade/include/palisade/pke -I ${FHE_LIBS_DIR}/palisade/include/
PALISADE_DYNAMIC_LIBS_LOCATION=-L ${FHE_LIBS_DIR}/palisade/lib
PALISADE_DYNAMIC_LIBS=-l PALISADEcore -l PALISADEpke 
PALISADE_FLAGS=${PALISADE_INCLUDES} ${PALISADE_DYNAMIC_LIBS_LOCATION} ${PALISADE_DYNAMIC_LIBS}

# Assume all libraries are installed to system
SEAL_INCLUDES=-I ${FHE_LIBS_DIR}/seal/include/SEAL-3.7
SEAL_LIB=${FHE_LIBS_DIR}/seal/lib64/libseal-3.7.a
SEAL_FLAGS=$(SEAL_INCLUDES) $(SEAL_LIB)

HEAAN_LOCATION=~/heaan-v3.2.1-avx512dq/

HEAAN_INCLUDES=-I ${FHE_LIBS_DIR}/heaan/include/
HEAAN_LIB_LOCATION=-L ${FHE_LIBS_DIR}/heaan/lib
HEAAN_LIB=-l HEaaN
HEAAN_FLAGS=$(HEAAN_INCLUDES) $(HEAAN_LIB_LOCATION) $(HEAAN_LIB) 

CXX=g++
# -pedantic left out for use of uint128_t
CFLAGS=-std=c++17 -O3 -pthread -Wall -Werror -fopenmp
SRC=src
INCLUDE=include
BINARY=bin

# Make sure the "all" rule is listed first in the file!
all: main
PROFILE_INCLUDES= $(INCLUDE)/HEAANProfiler.h $(INCLUDE)/HElibProfiler.h $(INCLUDE)/homenc-profile.h $(INCLUDE)/PALISADEProfiler.h $(INCLUDE)/Profiler.h $(INCLUDE)/SEALProfiler.h
main: $(SRC)/main.cpp $(PROFILE_INCLUDES)
	mkdir -p $(BINARY)
	$(CXX) $< $(CFLAGS) -o $(BINARY)/$@ $(PALISADE_FLAGS) $(HEAAN_FLAGS) $(SEAL_FLAGS) -DNHELIB 

# $(PALISADE_DYNAMIC_LIBS) $(HEANN_LIB) $(SEAL_LIB) 

#Temporarily disable HEAAN for this quick test

avx512: AVX_VERSION=512
avx512: HEAAN_FLAGS= -DNHEAAN 
avx512: all

# We asssume SEAL and PALISADE (and later HElib), the open-source libraries, are available
noheaan: HEAAN_FLAGS= -DNHEAAN
noheaan: all

# Need -f or an error is thrown
clean:
	rm -f bin/*
