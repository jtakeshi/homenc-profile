# I don't think we need binfhe - TODO try taking it out
#
# Default to AVX2
AVX_VERSION=2

FHE_LIBS_DIR=$(abspath ${HOME}/.fhe-libs/avx${AVX_VERSION})

PALISADE_BASE_DIR=${FHE_LIBS_DIR}/palisade
PALISADE_INCLUDES=-I ${PALISADE_BASE_DIR}/include -I ${PALISADE_BASE_DIR}/include/palisade -I ${PALISADE_BASE_DIR}/include/palisade/cereal -I ${PALISADE_BASE_DIR}/include/palisade/core -I ${PALISADE_BASE_DIR}/include/palisade/pke -I ${PALISADE_BASE_DIR}/include/palisade/duality
PALISADE_DYNAMIC_LIBS_LOCATION=-L ${PALISADE_BASE_DIR}/lib
PALISADE_LIBPATH=-Wl,-rpath=${PALISADE_BASE_DIR}/lib
PALISADE_DYNAMIC_LIBS=-l PALISADEcore -l PALISADEpke -l PALISADEduality 
PALISADE_FLAGS=${PALISADE_INCLUDES} ${PALISADE_DYNAMIC_LIBS_LOCATION} ${PALISADE_LIBPATH} ${PALISADE_DYNAMIC_LIBS}

# Assume all libraries are installed to system
SEAL_DIR=${FHE_LIBS_DIR}/seal
SEAL_INCLUDES=-I ${SEAL_DIR}/include/SEAL-3.7
SEAL_LIB=${SEAL_DIR}/lib/libseal-3.7.a
SEAL_FLAGS=$(SEAL_INCLUDES) $(SEAL_LIB)

HEAAN_TYPE=
HEAAN_DIR=${FHE_LIBS_DIR}/heaan${HEAAN_TYPE}
HEAAN_INCLUDES=-I ${HEAAN_DIR}/include/HEaaN -I ${HEAAN_DIR}/include
HEAAN_LIB_LOCATION=-L ${HEAAN_DIR}/lib
HEAAN_LIB=-l HEaaN
HEAAN_LIBPATH=-Wl,-rpath=${HEAAN_DIR}/lib
HEAAN_FLAGS=$(HEAAN_INCLUDES) $(HEAAN_LIB_LOCATION) $(HEAAN_LIB) $(HEAAN_LIBPATH)

HELIB_DIR=${FHE_LIBS_DIR}/helib
HELIB_INCLUDES=-I ${HELIB_DIR}/include -I ${HELIB_DIR}/include/helib
HELIB_LIB_LOCATION=-L ${HELIB_DIR}/lib -Wl,-rpath=${HELIB_DIR}/lib
HELIB_LIB=-l helib
# Use -rpath for NTL - not sure why it's being picky
HELIB_FLAGS=$(HELIB_INCLUDES) $(HELIB_LIB_LOCATION) $(HELIB_LIB) -lntl -lgmp -Wl,-rpath=/usr/local/lib/

CXX=g++
# -pedantic left out for use of uint128_t
CFLAGS=-std=c++17 -pthread -Wall -Werror -fopenmp -O3

SRC=src
INCLUDE=include
BINARY=bin

# Make sure the "all" rule is listed first in the file!
all: main
PROFILE_INCLUDES= $(INCLUDE)/HEAANProfiler.h $(INCLUDE)/HElibProfiler.h $(INCLUDE)/homenc-profile.h $(INCLUDE)/PALISADEProfiler.h $(INCLUDE)/Profiler.h $(INCLUDE)/SEALProfiler.h
main: $(SRC)/main.cpp $(PROFILE_INCLUDES)
	mkdir -p $(BINARY)
	echo "SEAL flags:"
	echo $(SEAL_FLAGS)
	$(CXX) $< $(CFLAGS) -o $(BINARY)/$@ $(PALISADE_FLAGS) $(HEAAN_FLAGS) $(SEAL_FLAGS) $(HELIB_FLAGS)
	
debug: CFLAGS=-std=c++17 -pthread -Wall -Werror -fopenmp -ggdb3
debug: all

avx512: AVX_VERSION=512

# We asssume SEAL and PALISADE and HElib, the open-source libraries, are available
noheaan: HEAAN_FLAGS= -DNHEAAN
noheaan: all

nohelib: HELIB_FLAGS= -DNHELIB
nohelib: all

submit: 
	tar -czvf profile.tar.gz README.md src/ utilities/ include/ jobs/ containers/manifests/

# Need -f or an error is thrown
clean:
	rm -f bin/main
