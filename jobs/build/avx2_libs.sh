#!/bin/bash

# If you run scripts, change this or else - I don't want to be spammed with emails
# Parameters - GPU queue, use all available cores cause it's our computer
#$ -M jtakeshi@nd.edu
#$ -m abe
#$ -q gpu@@jung_gpu
#$ -l gpu_card=1
#$ -N build_avx2
#$ -pe smp 12

# Exit upon errors
set -e
# Print all commands as run
set -o xtrace

#Set this to one of 2 or 512
VERSION=2

BASEDIR=~/.fhe-libs/avx${VERSION}

SEAL_DIR=$BASEDIR/seal
PALISADE_DIR=$BASEDIR/palisade
HELIB_DIR=$BASEDIR/helib
HEAAN_DIR=$BASEDIR/heaan
HEAAN_GPU_DIR=$BASEDIR/heaan-gpu
HEXL_DIR=$BASEDIR/hexl

CONTAINER=/scratch365/$LOGNAME/ubuntu2004-cuda-nolibs.img

mkdir -p $SEAL_DIR $PALISADE_DIR $HELIB_DIR $HEAAN_DIR $HEAAN_GPU_DIR $HEXL_DIR

# Now install HELib proper
# First, install HEXL
if [ "$VERSION" == "512" ]; then
  cd ~
  rm -rf hexl
  git clone https://github.com/intel/hexl --branch 1.2.1
  cd hexl
  singularity exec $CONTAINER cmake -S . -B build/ -DCMAKE_INSTALL_PREFIX=$HEXL_DIR/lib/cmake/hexl-1.2.1
  singularity exec $CONTAINER cmake --build build
  singularity exec $CONTAINER cmake --install build
fi  

cd ~
rm -rf HElib
git clone https://github.com/homenc/HElib.git
cd HElib
mkdir build
cd build
if [ "$VERSION" == "512" ]; then
  HELIB_AVX_OPTIONS=-DUSE_INTEL_HEXL=ON -DHEXL_DIR=$HEXL_DIR
else
  HELIB_AVX_OPTIONS=""
fi    
singularity exec $CONTAINER cmake .. -DCMAKE_INSTALL_PREFIX=$HELIB_DIR $HELIB_AVX_OPTIONS
singularity exec $CONTAINER make -j 12
singularity exec $CONTAINER make install

# Install SEAL
cd ~
rm -rf SEAL
git clone https://github.com/microsoft/SEAL.git
cd SEAL/
if [ "$VERSION" == "512" ]; then
  SEAL_AVX_OPTIONS=-DSEAL_USE_INTEL_HEXL=ON
else
  SEAL_AVX_OPTIONS=""
fi   
singularity exec $CONTAINER cmake -S . -B build -DSEAL_USE_CXX17=-ON -DSEAL_THROW_ON_TRANSPARENT_CIPHERTEXT=OFF -DSEAL_USE_INTRIN=ON -DSEAL_USE__ADDCARRY_U64=ON -DSEAL_USE__SUBBORROW_U64=ON -DSEAL_USE___BULTIN_CLZLL=ON -DSEAL_USE___INT128=ON -DSEAL_DEFAULT_PRNG=Blake2xb -DSEAL_USE_GAUSSIAN_NOISE=ON -DCMAKE_INSTALL_PREFIX=$SEAL_DIR $SEAL_AVX_OPTIONS
singularity exec $CONTAINER cmake --build build
singularity exec $CONTAINER cmake --install build

# Install PALISADE
cd ~
#rm -rf palisade-development
#git clone https://gitlab.com/palisade/palisade-development.git
#cd palisade-development
#cd /afs/crc.nd.edu/group/dsp-lab/ckks-bootstrapping/
rm -rf ckks-bootstrapping
cp -r /afs/crc.nd.edu/group/dsp-lab/ckks-bootstrapping ./
cd ckks-bootstrapping
rm -rf build
mkdir build
cd build
if [ "$VERSION" == "512" ]; then
  PALISADE_AVX_OPTIONS=-DWITH_INTEL_HEXL=ON 
else
  PALISADE_AVX_OPTIONS=""
fi   
pwd
ls -lh
singularity exec $CONTAINER cmake .. -DWITH_NATIVEOPT=ON -DCMAKE_INSTALL_PREFIX=$PALISADE_DIR $PALISADE_AVX_OPTIONS
singularity exec $CONTAINER make -j 12
singularity exec $CONTAINER make install
