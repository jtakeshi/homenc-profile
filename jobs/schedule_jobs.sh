#!/usr/bin/bash
# Jonathan S. Takeshita (with help from Scott Hampton)
# University of Notre Dame
set -e
set -x

LINE_COUNT=0
JOB_COUNT=0

# Input should be from STDIN. Each line has a list of space-separated job script names (full path required!).
# Scripts on the same line will be submitted simultaneously. Each line will be submitted in order.
# Not yet tested.

WAIT_ON=""  #Contains both the -hold_jid flag and comma-delimited arguments

# https://stackoverflow.com/questions/6980090/how-to-read-from-a-file-or-standard-input-in-bash
while read SC; do
  echo $SC
  LINE_COUNT=$((LINE_COUNT + 1))
  echo "Starting jobs [$SC] (line $LINE_COUNT) at $(date)"
  JOB_ARRAY=($SC)
  JOBS_THIS_LINE=""
  for JOB in "${JOB_ARRAY[@]}"
  do
    JOB_UID=j_${JOB_COUNT}
    qsub -N "$JOB_UID" $WAIT_ON $JOB 
    JOB_COUNT=$((JOB_COUNT + 1))
    JOBS_THIS_LINE="${JOBS_THIS_LINE}"$JOB_UID","  #Append to a list - trailing comma ok?
  done  
  WAIT_ON="-hold_jid $JOBS_THIS_LINE"
done < "${1:-/dev/stdin}"  

echo "Submitted $JOB_COUNT jobs from $LINE_COUNT lines"
