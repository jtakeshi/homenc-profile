#!/bin/bash

# If you run scripts, change this or else - I don't want to be spammed with emails
# Parameters - GPU queue, use all available cores cause it's our computer
#$ -M jtakeshi@nd.edu
#$ -m abe
#$ -q gpu@@jung_gpu
#$ -l gpu_card=1
#$ -N sin_avx2
#$ -pe smp 1

#Exit on error
set -e

#Set Singularity container path
CONTAINER=/scratch365/${SGE_O_LOGNAME}/ubuntu1804.img
#And check that it's there
file $CONTAINER

#Run from root directory
EXECUTABLE=bin/main

#Bash needs quotes for space-separated values (make doesn't)
#Bootsrapping omitted!
TESTS="add mult mult_no_relin rotate relin rescale size_ctext size_public_key size_secret_key size_params"
LIBRARIES="-l palisade -l seal -l heaan -l helib"
ITERATIONS="-i 1"
DEPTH="-d 1"

SUBFOLDER=avx2

mkdir -p ./results/${SUBFOLDER}/
#module load gcc/11.1.0 fhe-libs/avx2

#First, recompile on the machine in question
singularity exec $CONTAINER make clean all

#Run the test
singularity exec $CONTAINER $EXECUTABLE $TESTS $LIBRARIES $ITERATIONS $DEPTH > ./results/${SUBFOLDER}/results.csv 2> ./results/${SUBFOLDER}/err.txt

#Unload modules
#module unload gcc/11.1.0 fhe-libs/avx2
