#!/bin/bash

# If you run scripts, change this or else - I don't want to be spammed with emails
# Parameters - GPU queue, use all available cores cause it's our computer
#$ -M jtakeshi@nd.edu
#$ -m abe
#$ -q gpu@@jung_gpu
#$ -pe smp 24
#$ -N alltest
#

#Exit on error
set -e

#Run from root directory
EXECUTABLE=bin/main

TESTS=all
LIBRARIES=-l all
ITERATIONS=-i 3

SUBFOLDER=smalltest

CONTAINER=ubuntu1804.sin

mkdir -p ./results/${SUBFOLDER}/
#This is the most up-to-date version easily installed in Ubuntu 18.04
module load gcc/10.3.0

#First, recompile on the machine in question
export SINGULARITYENV_CUDA_VISIBLE_DEVICES=${CUDA_VISIBLE_DEVICES}
singularity exec --nv /scratch365/$LOGNAME/$CONTAINER make clean all

#Run the test
singularity exec --nv $EXECUTABLE $TESTS $LIBRARIES $ITERATIONS > ./results/${SUBFOLDER}/results.csv 2> ./results/${SUBFOLDER}/err.txt
