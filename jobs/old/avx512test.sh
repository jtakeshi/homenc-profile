#!/bin/bash

# If you run scripts, change this or else - I don't want to be spammed with emails
# Parameters - GPU queue, use all available cores cause it's our computer
#$ -M jtakeshi@nd.edu
#$ -m abe
#$ -q gpu@@jung_gpu
#$ -l gpu_card=1
#$ -N avx512
#$ -pe smp 1

#Exit on error
set -e

#Run from root directory
EXECUTABLE=bin/main

#Bash needs quotes fori space-separated values (make doesn't)
TESTS="add mult"
LIBRARIES="-l palisade -l seal"
ITERATIONS="-i 100"
DEPTH="-d 2"

SUBFOLDER=avx512

mkdir -p ./results/${SUBFOLDER}/
module load gcc/11.1.0 fhe-libs/avx512

#First, recompile on the machine in question
make clean noheaan

#Run the test
$EXECUTABLE $TESTS $LIBRARIES $ITERATIONS $DEPTH > ./results/${SUBFOLDER}/results.csv 2> ./results/${SUBFOLDER}/err.txt

#Unload modules
module unload gcc/11.1.0 fhe-libs/avx512
