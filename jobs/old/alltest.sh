#!/bin/bash

# If you run scripts, change this or else - I don't want to be spammed with emails
# Parameters - GPU queue, use all available cores cause it's our computer
#$ -M jtakeshi@nd.edu
#$ -m abe
#$ -q gpu@@jung_gpu
#$ -pe smp 24
#$ -N alltest
#

#Exit on error
set -e

#Run from root directory
EXECUTABLE=bin/main

TESTS=all
LIBRARIES=-l all
ITERATIONS=-i 3

SUBFOLDER=smalltest

mkdir -p ./results/${SUBFOLDER}/
module load gcc/11.1.0

#First, recompile on the machine in question
make clean all

#Run the test
$EXECUTABLE $TESTS $LIBRARIES $ITERATIONS > ./results/${SUBFOLDER}/results.csv 2> ./results/${SUBFOLDER}/err.txt
