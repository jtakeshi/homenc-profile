#!/bin/bash

# If you run scripts, change this or else - I don't want to be spammed with emails
# Parameters - GPU queue, use all available cores cause it's our computer
#$ -M jtakeshi@nd.edu
#$ -m abe
#$ -q gpu@@jung_gpu
#$ -l gpu_card=1
#$ -N cleanup_avx2
#$ -pe smp 1

#Exit on error
set -e

#Set Singularity container path
CONTAINER=/scratch365/${SGE_O_LOGNAME}/ubuntu1804.img
#And check that it's there
file $CONTAINER

#Run from root directory
EXECUTABLE=bin/main

#Bash needs quotes for space-separated values (make doesn't)
#Bootsrapping omitted!
TESTS="size_params"
#LIBRARIES="-l palisade -l seal -l heaan -l helib"
#Currently, HEAAN is the only library with bootstrapping exposed
LIBRARIES="-l helib"
ITERATIONS="-i 1"
DEPTH="-d 1"


SUBFOLDER=avx2/cleanup

module load gcc/11.1.0
g++ utilities/stats.cpp -o bin/stats -Wall -Werror -pedantic -O3 -march=native

mkdir -p ./results/${SUBFOLDER}/
rm -f ./results/${SUBFOLDER}/*
#module load gcc/11.1.0 fhe-libs/avx2

#First, recompile on the machine in question
singularity exec $CONTAINER make clean all

#Run the test
for i in {40..55..5}
do
  SCALE="-s $i"
  echo "scale $i      singularity exec $CONTAINER $EXECUTABLE $TESTS $LIBRARIES $ITERATIONS $DEPTH $SCALE" >> ./results/${SUBFOLDER}/report.csv
  singularity exec $CONTAINER $EXECUTABLE $TESTS $LIBRARIES $ITERATIONS $DEPTH $SCALE > ./results/${SUBFOLDER}/results_${i}.csv 2> ./results/${SUBFOLDER}/err.txt
  bin/stats < ./results/${SUBFOLDER}/results_${i}.csv >> ./results/${SUBFOLDER}/report.csv
  echo "" >> ./results/${SUBFOLDER}/report.csv
done

#Run the test
for i in {1..19}
do
  DEPTH="-d $i"
  singularity exec $CONTAINER $EXECUTABLE $TESTS $LIBRARIES $ITERATIONS $DEPTH > ./results/${SUBFOLDER}/results_${i}.csv 2> ./results/${SUBFOLDER}/err.txt
  echo "depth $i" >> ./results/${SUBFOLDER}/report.csv
  bin/stats < ./results/${SUBFOLDER}/results_${i}.csv >> ./results/${SUBFOLDER}/report.csv
  echo "" >> ./results/${SUBFOLDER}/report.csv
done

echo "Results sent out to results/${SUBFOLDER}/report.csv"

#Unload modules
module unload gcc/11.1.0
