#!/bin/bash

# If you run scripts, change this or else - I don't want to be spammed with emails
# Parameters - GPU queue, use all available cores cause it's our computer
#$ -M jtakeshi@nd.edu
#$ -m abe
#$ -q gpu@@jung_gpu
#$ -l gpu_card=1
#$ -N bootstrapping_avx2
#$ -pe smp 1

#Exit on error
set -e

#Set Singularity container path
CONTAINER=/scratch365/${SGE_O_LOGNAME}/ubuntu1804.img
#And check that it's there
file $CONTAINER

#Run from root directory
EXECUTABLE=bin/main

#Bash needs quotes for space-separated values (make doesn't)
TESTS="bootstrap"
LIBRARIES="-l heaan"
ITERATIONS="-i 50"
DEPTH="-d 1"

SUBFOLDER=avx2/bootstrapping

module load gcc/11.1.0
g++ utilities/stats.cpp -o bin/stats -Wall -Werror -pedantic -O3 -march=native

mkdir -p ./results/${SUBFOLDER}/
rm -f ./results/${SUBFOLDER}/*
#module load gcc/11.1.0 fhe-libs/avx2

#First, recompile on the machine in question
singularity exec $CONTAINER make clean all

#Run the test
for i in {1..3}
do
  B_LEVEL="-b $i"
  singularity exec $CONTAINER $EXECUTABLE $TESTS $LIBRARIES $ITERATIONS $DEPTH $B_LEVEL > ./results/${SUBFOLDER}/results_${i}.csv 2> ./results/${SUBFOLDER}/err.txt
  echo $i >> ./results/${SUBFOLDER}/report.csv
  bin/stats < ./results/${SUBFOLDER}/results_${i}.csv >> ./results/${SUBFOLDER}/report.csv
  echo "" >> ./results/${SUBFOLDER}/report.csv
done

echo "Results sent out to results/${SUBFOLDER}/report.csv"

module unload gcc/11.1.0
