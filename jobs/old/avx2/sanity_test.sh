#!/bin/bash

# If you run scripts, change this or else - I don't want to be spammed with emails
# Parameters - GPU queue, use all available cores cause it's our computer
#$ -M jtakeshi@nd.edu
#$ -m abe
#$ -q gpu@@jung_gpu
#$ -l gpu_card=1
#$ -N sanity
#$ -pe smp 1

#Exit on error
set -e

#Set Singularity container path
CONTAINER=/scratch365/${SGE_O_LOGNAME}/ubuntu2004-cuda-nolibs.img
#And check that it's there
file $CONTAINER

ls ~/.fhe-libs/avx2/palisade/lib/
file ~/.fhe-libs/avx2/palisade/lib/libPALISADEcore.so.1

#Run from root directory
EXECUTABLE=bin/main

#Bash needs quotes for space-separated values (make doesn't)
#Bootsrapping omitted!
TESTS="add mult mult_no_relin rotate relin rescale size_ctext size_public_key size_secret_key size_params"
#LIBRARIES="-l palisade -l seal -l heaan -l helib"
#Currently, HEAAN is the only library with bootstrapping exposed
LIBRARIES="-l all"
ITERATIONS="-i 50"
DEPTH="-d 1"


SUBFOLDER=avx2/sanity

module load gcc/11.1.0
g++ utilities/stats.cpp -o bin/stats -Wall -Werror -pedantic -O3 -march=native

mkdir -p ./results/${SUBFOLDER}/
rm -f ./results/${SUBFOLDER}/*
#module load gcc/11.1.0 fhe-libs/avx2

#First, recompile on the machine in question
singularity exec $CONTAINER make clean all

#Run the test
for i in {40..55..5}
do
  SCALE="-s $i"
  echo "$i      singularity exec $CONTAINER $EXECUTABLE $TESTS $LIBRARIES $ITERATIONS $DEPTH $SCALE" >> ./results/${SUBFOLDER}/report.csv
  singularity exec $CONTAINER $EXECUTABLE $TESTS $LIBRARIES $ITERATIONS $DEPTH $SCALE > ./results/${SUBFOLDER}/results_${i}.csv 2> ./results/${SUBFOLDER}/err.txt
  bin/stats < ./results/${SUBFOLDER}/results_${i}.csv >> ./results/${SUBFOLDER}/report.csv
  echo "" >> ./results/${SUBFOLDER}/report.csv
done

echo "Results sent out to results/${SUBFOLDER}/report.csv"

#Unload modules
module unload gcc/11.1.0
