#!/bin/bash

# If you run scripts, change this or else - I don't want to be spammed with emails
# Parameters - GPU queue, use all available cores cause it's our computer
#$ -M nkoirala@nd.edu
#$ -m abe
#$ -q gpu@@jung_gpu
#$ -l gpu_card=1
#$ -N avx_SHE_tests
#$ -pe smp 1

#Exit on error
set -e

#move it to your folder in CRC
CONTAINER=/scratch365/${SGE_O_LOGNAME}/ubuntu2004-cuda-nolibs.img
file $CONTAINER

EXECUTABLE=bin/main

TESTS="add mult mult_no_relin rotate relin rescale size_ctext size_public_key size_secret_key size_params rotdot"
#LIBRARIES="-l palisade -l seal -l heaan -l helib"
LIBRARIES="-l all"
ITERATIONS="-i 50"

# compile the executable before running
singularity exec $CONTAINER make AVX_VERSION=2 clean all

module load gcc/11.1.0
g++ utilities/stats.cpp -o bin/stats -Wall -Werror -pedantic -O3 -march=native
module unload gcc/11.1.0

SUBFOLDER=avx2/varying_depth

mkdir -p ./results/${SUBFOLDER}/
rm -f ./results/${SUBFOLDER}/*

#First, recompile on the machine in question
singularity exec $CONTAINER make clean all

# 1 is default otherwise 1..UpperLimit..Delta
for i in {1..19}
do
  DEPTH="-d $i"
  echo "$i      singularity exec $CONTAINER $EXECUTABLE $TESTS $LIBRARIES $ITERATIONS $DEPTH" >> ./results/${SUBFOLDER}/report.csv
  singularity exec $CONTAINER $EXECUTABLE $TESTS $LIBRARIES $ITERATIONS $DEPTH > ./results/${SUBFOLDER}/results_${i}.csv 2> ./results/${SUBFOLDER}/err.txt
  bin/stats < ./results/${SUBFOLDER}/results_${i}.csv >> ./results/${SUBFOLDER}/report.csv
  echo "" >> ./results/${SUBFOLDER}/report.csv
done


echo "Results sent out to results/${SUBFOLDER}/report.csv"
