#!/bin/bash

# If you run scripts, change this or else - I don't want to be spammed with emails
# Parameters - GPU queue, use all available cores cause it's our computer
#$ -M jtakeshi@nd.edu
#$ -m abe
#$ -q gpu@@jung_gpu
#$ -l gpu_card=1
#$ -N threads_avx2
#$ -pe smp 1

#Exit on error
set -e

#Set Singularity container path
CONTAINER=/scratch365/${SGE_O_LOGNAME}/ubuntu1804.img
#And check that it's there
file $CONTAINER
#Allow Singularity to see the GPU
export SINGULARITYENV_CUDA_VISIBLE_DEVICES=${CUDA_VISIBLE_DEVICES}

#Run from root directory
EXECUTABLE=bin/main

#Bash needs quotes for space-separated values (make doesn't)
#Bootsrapping omitted!
TESTS="add mult bootstrapping rotate size_ctext size_public_key size_secret_key size_bootstrap_key"
#LIBRARIES="-l palisade -l seal -l heaan -l helib"
#Currently, HEAAN is the only library with bootstrapping exposed
LIBRARIES="-l heaan"
ITERATIONS="-i 50"
DEPTH="-d 1" # TODO set me to something larger


SUBFOLDER=avx2/threads

module load gcc/11.1.0
g++ utilities/stats.cpp -o bin/stats -Wall -Werror -pedantic -O3 -march=native

mkdir -p ./results/${SUBFOLDER}/
rm -f ./results/${SUBFOLDER}/*
#module load gcc/11.1.0 fhe-libs/avx2

#First, recompile on the machine in question
singularity exec $CONTAINER make clean all

#Run the test
for i in {1..24..4}
do
  THREADS="-s $i"
  SINGULARITYENV_OMP_NUM_THREADS=$i singularity exec --nv $CONTAINER $EXECUTABLE $TESTS $LIBRARIES $ITERATIONS $DEPTH > ./results/${SUBFOLDER}/results_${i}.csv 2> ./results/${SUBFOLDER}/err.txt
  echo $i >> ./results/${SUBFOLDER}/report.csv
  bin/stats < ./results/${SUBFOLDER}/results_${i}.csv >> ./results/${SUBFOLDER}/report.csv
  echo "" >> ./results/${SUBFOLDER}/report.csv
done

echo "Results sent out to results/${SUBFOLDER}/report.csv"

#Unload modules
module unload gcc/11.1.0
