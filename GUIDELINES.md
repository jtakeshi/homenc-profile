# Guidelines for programming

## Languages and tools

- Use standard C++, with no dependencies besides what we test.
- Use C++17.
- Compile with -Wall, -Werror, -pedantic if at all possible.

## Best practices - organization and teamwork
- Use separate header, source, and binary directories.
- Use standard input and standard output for input and output files as much as possible.
- In some projects, I prefer header-only libraries. In this project, we will probably follow a more standard organization.
- Use separate branches for separate work, and send in merge requests.

## Best pratices - code

### C++
- Pass by const reference for non-primitive types.
- Don't use pointers unnecessarily if modern C++ can be used for non-critical code.
- Use const and class member permissions to ensure variables are not modified unintentionally.
- NEVER use pointers to an object stored in an STL container.
- ALWAYS use braces. See https://blog.codecentric.de/en/2014/02/curly-braces/
- Use standard functionality when available.
  - Use GNU getopt for input parsing.

### Python
- When appending, use .append() - += will treat its input as an iterable instead of a single variable  
- Organize your code, don't just script.
- Use a defined main function instead of having everything at top-level.


## Style
- Use 2-space indentation. (Set your editor to interpret a tab as 2 spaces.) Makefiles must use tabs.

## Performance
- Avoid repeated indexing into maps.
- For many small outputs, consider writing to a buffer and flushing the buffer at the end of the program, instead of writing to stdout.

