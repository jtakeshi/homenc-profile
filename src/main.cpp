// g++ src/main.cpp -std=c++17 -Wall -Werror /usr/local/lib/libseal-3.7.a -I
// /usr/local/include/SEAL-3.7/

// g++ src/main.cpp -std=c++17 -Wall -Werror -pedantic
// /usr/local/lib/libseal-3.6.a -I /usr/local/include/SEAL-3.6/ -pthread g++
// src/main.cpp -std=c++17 -Wall -Werror /usr/local/lib/libseal-3.6.a -I
// /usr/local/include/SEAL-3.6/ -pthread -Wl,-rpath=/usr/local/lib/ -l HEaaN -I
// /usr/local/include/HEaaN/
#include <ctime>
#include <deque>
#include <getopt.h>
#include <iostream>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include "../include/homenc-profile.h"

using namespace std;

// https://www.codevscolor.com/c-plus-plus-print-current-date-day-time

string getTime() {
  time_t tt;
  // Don't try to free st, as it points to a library-internal object
  struct tm *st = NULL;
  time(&tt);
  st = localtime(&tt);
  string ret = asctime(st);
  // Replace trailing newline
  ret.back() = ' ';
  return ret;
}

vector<double> rand_coeffs(const unsigned int deg,
                           const unsigned int seed = 5) {
  srand(seed);
  vector<double> ret(deg);
  for (double &d : ret) {
    d = ((double)rand()) / ((double)RAND_MAX);
    d *= 2;
    d -= 1.0f;
  }
  return ret;
}

// Bootstrapping level requires an argument, because getopt is picky with how it
// handles optional arguments

static struct option long_options[] = {
    {"library", required_argument, 0, 'l'},
    {"iterations", required_argument, 0, 'i'},
    {"verbose", no_argument, 0, 'v'},
    {"scale-bits", required_argument, 0, 's'},
    {"depth", required_argument, 0, 'd'},
    {"bootstrap", required_argument, 0, 'b'},
    {"coeff", required_argument, 0, 'c'},
    {"dimension", required_argument, 0, 'm'},
    {"native-alg", required_argument, 0, 'n'},
    {"dot-len", required_argument, 0, 'o'},
    {NULL, 0, 0, 0}};

int main(int argc, char **argv) {

  // Use sets to automatically filter out duplicates
  set<string> libs_to_test;
  set<string> tests;
  unsigned int iterations = 0;
  unsigned int verbose = 0;
  unsigned int scale_bits = 0;
  unsigned int depth = 0;
  unsigned int bootstrap = 0;
  unsigned int dimension = 0;
  vector<double> polynomial_coeffs;
  bool native_alg = true;
  unsigned int dot_len = 1;

  int c;
  int option_index = 0;
  // Don't forget == -1
  while ((c = getopt_long(argc, argv, "l:i:vs:d:b:c:m:n:o:", long_options,
                          &option_index)) != -1) {
    switch (c) {
    case 'l': {
      string lib_to_test = string(optarg);
      if (lib_to_test == "all") {
        for (const auto &x : PROF_MAP) {
          libs_to_test.insert(x.first);
        }
      } else {
        libs_to_test.insert(lib_to_test);
      }
      break;
    }
    case 'i': {
      iterations = atoi(optarg);
      break;
    }
    case 'v': {
      verbose++;
      break;
    }
    case 's': {
      scale_bits = atoi(optarg);
      break;
    }
    case 'd': {
      depth = atoi(optarg);
      break;
    }
    // Argue a number from 1-3 (currently) for bootstrapping.
    // Each Profiler translates this into a set of bootstrap-capable parameters,
    // if they can do bootstrapping. Default is 0 for using only SHE.
    case 'b': {
      bootstrap = atoi(optarg);
      break;
    }
    // Argue either a filename containing polynomial coefficients, or an integer
    // specifying the degree of the polynomial. If the degree is given, then
    // coefficients from -1 to 1 will be randomly chosen. The polynomial
    // variable will be set at 1.
    case 'c': {
      if (!polynomial_coeffs.empty()) {
        cout << "ERROR: more than one argument of polynomial coefficients made"
             << endl;
        return 1;
      }
      try {
        // stoi will throw on non-numeric strings, which is allowed for
        unsigned int poly_depth = std::stoi(optarg);
        polynomial_coeffs = rand_coeffs(poly_depth);
      } catch (std::invalid_argument &e) {
        std::ifstream ifs(optarg);
        double d;
        while (ifs >> d) {
          polynomial_coeffs.push_back(d);
        }
      }
      assert(polynomial_coeffs.size() >= 2);
      break;
    }
    case 'm': {
      dimension = atoi(optarg);
      break;
    }
    case 'n': {
      native_alg = (bool)atoi(optarg);
      break;
    }
    case 'o': {
      dot_len = atoi(optarg);
      break;
    }
    default: {
      cout << "Unrecognized option, exiting: " << c << endl;
      return 1;
    }
    }
  }

  // TODO parse by ':' to get iterations
  std::string all = "all";
  for (int idx = optind; idx < argc; idx++) {
    if (argv[idx] == all) {
      for (const std::string &s : AVAILABLE_TESTS) {
        tests.insert(s);
      }
      break;
    }
    string test_to_run = argv[idx];
    tests.insert(test_to_run);
  }

#ifndef NPALISADE
  bool optimize_palisade = false;
  optimize_palisade = tests.find(PALISADE_OPT_STR) != tests.end();
  HEParams hp_palisade_opt(depth, bootstrap, scale_bits, 0, 0, 0,
                           optimize_palisade);
#endif

  TestParams tp(iterations, dimension);
  HEParams hp(depth, bootstrap, scale_bits, 0, 0, 0);
  tp.polynomial_coeffs = polynomial_coeffs;
  tp.native_alg = native_alg;
  tp.dot_len = dot_len;

  // Get tests as a vector of pair of string and test params
  // This can be modified later if there should be different parameters for each
  // test
  vector<pair<string, TestParams>> tests_vec;
  tests_vec.reserve(tests.size());
  for (const std::string &x : tests) {
    tests_vec.emplace_back(pair<string, TestParams>(x, tp));
  }

  // Check for correct parameters for bootstrapping
  if (std::find(tests.begin(), tests.end(), TEST_BOOTSTRAP) != tests.end() &&
      hp.depth() < 3) {
    cerr << "#Warning: attempting bootstrapping with depth " << hp.depth()
         << endl;
  }

  // Check for correct parameters for linear transformation
  if (std::find(tests.begin(), tests.end(), TEST_LINEAR) != tests.end() &&
      ((tp.dimension() > TestParams::DIMENSION_MAX) || (!tp.dimension()))) {
    cerr << "#Warning: attempting linreg with dimension " << tp.dimension()
         << endl;
  }

  // Check that a polynomial is specified for poly_eval
  if (std::find(tests.begin(), tests.end(), TEST_POLY_EVAL) != tests.end() &&
      tp.polynomial_coeffs.empty()) {
    cerr << "#ERROR: need coefficients specified for poly_eval" << endl;
    return 1;
  }

  // Progress outputs with verbose - prepend with '#' and the time, and use
  // std::endl to flush
  if (verbose) {
    cout << "# " << getTime() << " Parsed input, running tests: ";
    for (const auto &x : tests_vec) {
      cout << x.first << ' ';
    }
    cout << endl;
  }

  // Now run the tests!
  // This gets things in .csv form
  // There are other functions too
  if (verbose) {
    cout << "# " << getTime() << " Testing these libraries: ";
    for (const string &x : libs_to_test) {
      if (verbose) {
        cout << x << ' ';
      }
      cout << endl;
    }
  }

  for (const string &x : libs_to_test) {
    if (verbose) {
      cout << x << ' ';
    }
#ifndef NPALISADE
    std::shared_ptr<Profiler> pro =
        ProfilerFactory(x, (x == PALISADE_OPT_STR) ? hp_palisade_opt : hp);
#else
    std::shared_ptr<Profiler> pro = ProfilerFactory(x, hp);
#endif
    if (verbose) {
      cout << "# " << getTime() << " Starting tests for "
           << pro->profiler_name() << endl;
    }
    stringstream ss = pro->csv_output(tests_vec);
    if (verbose) {
      cout << "# " << getTime() << " Finished tests for "
           << pro->profiler_name() << endl;
    }
    cout << ss.str(); // Includes newline at end
  }

  return 0;
}
