# Homomorphic Encryption Profiling
This project aims to profile different FHE libraries.

# For developers
## How to compile
I highly recommend using this process, though other methods will probably work.
1. Create a directory `~/.fhe-libs` (or whichever other one you want to use)
2. Use the scripts in `jobs/build/` to install the FHE libraries PALISADE, HElib, and SEAL. You'll have to figure out HEAAN on your own - the proprietary libraries we used can't be redistributed or publicly released. The new Docker images should be usable, though there may be minor API changes.
3. Use the Makefile to compile: `make all`. You can argue `noheaan` or `nohelib` to build without those libraries. (Use `make submit` to build a tarball of all relevant files.)

## How to run
There are a *lot* of options and flags. For examples, see the `jobs/` directory.

Unflagged arguments are tests to run, e.g., `rotate` for testing rotation. See `include/Profiler.h` for a list of these tests.

The other flagged arguments are taken:
- -l: Argue a library (one of `seal`, `palisade`, `palisade_opt`, `heaan`, `helib`) to test. This can and should be argued multiple times. Or, argue `all` to profile all available libraries.
- -i: Specify the number of iterations to run tests for.
- -v: Increases verbosity. Most output is not intended to be human-readable, and should be run through the program given in `utilities/stats.cpp`.
- -s: Specify the number of scale bits.
- -d: Specify the computation depth.
- -b: Specify the use of bootstrapping-capable FHE parameters (PALISADE and HEAAN only). Should be one of 1, 2, 3, corresponding to N=15, 16, 17.
- -c: When testing polynomial evaluation, specify either a degree of a polynomial to randomly generate coefficients for when testing, or a filename with whitespace-delimited coefficients.
- -m: Specify the dimension for linear transform.
- -n: Specify whether built-in algorithms are used when possible (e.g., PALISADE's polynomial evaluation). On by default.
- -d: Specify the length of a dot product.

Also, `OMP_NUM_THREADS` can be used to specify the number of threads to use. 

## How to add a new function to test
1. Add the function signature in `include/Profiler.h` and the headers for each library. Functions should take in a `TestParams`, and return a `TestResult`.
2. Add a new constant test name to `include/Profiler.h` by adding a new `const static string`, and add this string to `AVAILABLE_TESTS`.
3. Add the function call to `Profiler::run_test_internal` (also found in `include/Profiler.h`).
4. Implement the function in each profiler's header.

## How to add a new library
1. In `include/homenc-profile.h`, include the name of your file where you implement tests, add the library to `ProfilerType`, give it a `const static string` name, add the `string` -> `ProfilerType` to `PROF_MAP`, and add the appropriate construction to `ProfilerFactory`. It is highly recommended to use a preprocessor flag around any new definitions/code relating to your library, as this allows you and others to compile the profiler without your library.
2. In the file you specified in step 1, implement your Profiler, which should conform to the interface defined in `include/Profiler.h`. (This file should also go in `include/`.)
