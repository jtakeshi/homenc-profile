#ifndef SEALPROFILER_H
#define SEALPROFILER_H

#include <cassert>
#include <cmath>
#include <cstdint>

#include <seal/seal.h>

#include "Profiler.h"
#include "linearAlgebra.h"

// /usr/local/lib/libseal-3.6.a
// /usr/local/include/SEAL-3.6/seal/seal.h

using namespace std;
using namespace std::chrono;
using std::unique_ptr;

class SEALProfiler : public Profiler {
private:
  unique_ptr<seal::EncryptionParameters> parms;
  unique_ptr<seal::SEALContext> context;
  unique_ptr<seal::SecretKey> secret_key;
  unique_ptr<seal::PublicKey> public_key;
  unique_ptr<seal::Encryptor> encryptor;
  unique_ptr<seal::Evaluator> evaluator;
  unique_ptr<seal::Decryptor> decryptor;
  unique_ptr<seal::CKKSEncoder> ckks_encoder;
  unique_ptr<seal::RelinKeys> relin_keys;
  unique_ptr<seal::GaloisKeys> galois_keys;
  unique_ptr<seal::KeyGenerator> keygen;

  // https://stackoverflow.com/questions/2704521/generate-random-double-numbers-in-c
  // Also returns original inputs
  seal::Ciphertext rand_ctext(vector<double> &args) const {
    size_t slot_count = ckks_encoder->slot_count();
    srand(time(NULL));
    args.clear();
    args.resize(slot_count);
    for (size_t i = 0; i < args.size(); i++) {
      // args[i] = (double)rand() / RAND_MAX;
      args[i] = rand_plain_val();
    }
    seal::Plaintext p;
    ckks_encoder->encode(args, this->_params.scale(), p);

    seal::Ciphertext c;
    encryptor->encrypt(p, c);
    return c;
  }

  seal::Ciphertext custom_ctext(vector<double> &args,
                                vector<double> &row) const {
    size_t slot_count = ckks_encoder->slot_count();
    args.clear();
    args.resize(slot_count);

    int row_size = row.size();

    for (size_t i = 0; i < args.size(); i++) {
      args[i] = row[i % row_size];
    }
    seal::Plaintext p;
    ckks_encoder->encode(args, this->_params.scale(), p);

    seal::Ciphertext c;
    encryptor->encrypt(p, c);
    return c;
  }

public:
  SEALProfiler(const HEParams &params_in) : Profiler(params_in) {

    parms = unique_ptr<seal::EncryptionParameters>(
        new seal::EncryptionParameters(seal::scheme_type::ckks));

    // Get modulus chain
    // https://github.com/microsoft/SEAL/blob/main/native/examples/4_ckks_basics.cpp
    unsigned int desired_depth = params_in.depth();
    std::vector<int> modulus_chain_bits(desired_depth + 2,
                                        params_in.scale_bits());
    modulus_chain_bits.front() = modulus_chain_bits.back() =
        params_in.edge_modulus_bits();

    // Total bits in the ciphertext modulus
    unsigned int ctext_modulus_bits = (2 * params_in.edge_modulus_bits()) +
                                      (desired_depth)*params_in.scale_bits();
    // Get an appropriate polynomial modulus degree
    unsigned int log_poly_mod_deg = 10;
    while (seal::CoeffModulus::MaxBitCount(
               1 << log_poly_mod_deg,
               static_cast<seal::sec_level_type>(params_in.security())) <
           (int)ctext_modulus_bits) {
      log_poly_mod_deg++;
    }

    parms->set_poly_modulus_degree(1 << log_poly_mod_deg);
    parms->set_coeff_modulus(
        seal::CoeffModulus::Create(1 << log_poly_mod_deg, modulus_chain_bits));

    context = unique_ptr<seal::SEALContext>(new seal::SEALContext(*parms));
    keygen = unique_ptr<seal::KeyGenerator>(new seal::KeyGenerator(*context));

    // Need to use pointers here because SEAL needs an existing object to write
    // to

    seal::SecretKey *tmp_sk = new seal::SecretKey;
    *tmp_sk = keygen->secret_key();
    secret_key = unique_ptr<seal::SecretKey>(tmp_sk);

    seal::PublicKey *tmp_pk = new seal::PublicKey;
    keygen->create_public_key(*tmp_pk);
    public_key = unique_ptr<seal::PublicKey>(tmp_pk);

    seal::RelinKeys *tmp_rlk = new seal::RelinKeys;
    keygen->create_relin_keys(*tmp_rlk);
    relin_keys = unique_ptr<seal::RelinKeys>(tmp_rlk);

    seal::GaloisKeys *tmp_glk = new seal::GaloisKeys;

    vector<int> steps;
    int slot_count = 1 << (log_poly_mod_deg - 1);
    for (int i = 1; i <= slot_count / 2; i <<= 1) {
      steps.push_back(i);
    }

    keygen->create_galois_keys(steps, *tmp_glk);
    galois_keys = unique_ptr<seal::GaloisKeys>(tmp_glk);

    encryptor =
        unique_ptr<seal::Encryptor>(new seal::Encryptor(*context, *public_key));
    evaluator = unique_ptr<seal::Evaluator>(new seal::Evaluator(*context));
    decryptor =
        unique_ptr<seal::Decryptor>(new seal::Decryptor(*context, *secret_key));
    ckks_encoder =
        unique_ptr<seal::CKKSEncoder>(new seal::CKKSEncoder(*context));

    // #ifdef PROF_DEBUG
    //     seal::print_parameters(*context);
    //     cout << endl;
    // #endif
  }

  void reset_params(const HEParams &params_in) { return; }

  string profiler_name() const { return "SEALProfiler"; }

  // Tests should take in a set of parameters and return a list of long double,
  // representing runtimes for each function
  TestResult test_linear_transform(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = ckks_encoder->slot_count();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    size_t dimension = tparams.dimension(); // max. 96

    vector<vector<double>> input_matrix = generateUVector(dimension);
    // vector<vector<double>> input_matrix
    // {
    //   {0, 1, 2, 3, 4},
    //   {5, 6, 7, 8, 9},
    //   {10, 11, 12, 13, 14},
    //   {15, 16, 17, 18, 19},
    //   {20, 21, 22, 23, 24}
    // };

    double input_size = input_matrix[0].size();
    vector<vector<double>> diagonal_matrix(input_size,
                                           vector<double>(input_size));

    diagonal_matrix = getDiagonalMatrix(input_matrix);

#ifdef PROF_DEBUG
    cout << "Diagonal matrix" << endl;

    double dsize = diagonal_matrix[0].size();
    for (size_t i = 0; i < dsize; i++) {
      for (size_t j = 0; j < dsize; j++) {
        cout << '(' << diagonal_matrix[i][j] << ")";
      }
      cout << "\n";
    }
#endif

    vector<vector<double>> transpose_matrix(input_size,
                                            vector<double>(input_size));

    for (size_t i = 0; i < input_size; ++i)
      for (size_t j = 0; j < input_size; ++j) {
        transpose_matrix[j][i] = diagonal_matrix[i][j];
      }

#ifdef PROF_DEBUG
    cout << "Diagonal-transpose_matrix" << endl;

    for (size_t i = 0; i < input_size; i++) {
      for (size_t j = 0; j < input_size; j++) {
        cout << '(' << transpose_matrix[i][j] << ")";
      }
      cout << "\n";
    }
#endif

    // unsigned int iters = tparams.iterations();
    unsigned int dot_len = input_size;

    vector<seal::Ciphertext> diagonalCt_vec;
    diagonalCt_vec.resize(dot_len);

    vector<vector<double>> args2(diagonalCt_vec.size());
    for (size_t i = 0; i < diagonalCt_vec.size(); i++) {

      diagonalCt_vec[i] =
          this->custom_ctext(args2[i], transpose_matrix[i % dimension]);
    }

    std::vector<double> input_vector;

    input_vector.reserve(dimension);

    for (size_t i = 0; i < dimension; i++) {
      input_vector.push_back(i + 0.001 * i);
    }

    // vector<double> input_vector{0, 1, 2, 3, 4};
    vector<seal::Ciphertext> inputCt_vec;
    inputCt_vec.resize(dot_len);
    vector<vector<double>> args3(inputCt_vec.size());

    for (size_t i = 0; i < inputCt_vec.size(); i++) {

      inputCt_vec[i] = this->custom_ctext(args3[i], input_vector);
    }

    // Generate new rotation keys, as needed
    vector<int> lintrans_steps;
    lintrans_steps.reserve(diagonalCt_vec.size() - 1);
    for (size_t i = 1; i < diagonalCt_vec.size(); i++) {
      if (std::find(lintrans_steps.begin(), lintrans_steps.end(), (int)i) ==
          lintrans_steps.end()) {
        lintrans_steps.push_back((int)i);
      }
    }
    seal::GaloisKeys *lintrans_glk = new seal::GaloisKeys;
    this->keygen->create_galois_keys(lintrans_steps, *lintrans_glk);

    //
    seal::Ciphertext ct_prime;
    for (unsigned int k = 0; k < iters; k++) {

      start = steady_clock::now();
      // doing the actual multplication and addition of diagonals and input
      // vector
      vector<seal::Ciphertext> ct_result(diagonalCt_vec.size());
      evaluator->multiply(inputCt_vec[0], diagonalCt_vec[0], ct_result[0]);
      seal::Ciphertext temp_rot;

      // takes n rotations, multiplications, and additions, and has depth of one
      // multiplication, one rotation, and n additions
      for (size_t i = 1; i < diagonalCt_vec.size(); i++) {

        evaluator->rotate_vector(inputCt_vec[i], i, *lintrans_glk, temp_rot);
        evaluator->multiply(temp_rot, diagonalCt_vec[i], ct_result[i]);
      }

      // look into the add_many function
      evaluator->add_many(ct_result, ct_prime);
      end = steady_clock::now();

      seal::Plaintext result_pt;
      decryptor->decrypt(ct_prime, result_pt);

      std::vector<double> vec_result;
      ckks_encoder->decode(result_pt, vec_result);

      std::vector<double> realResult =
          getLinearTransformVector(input_matrix, input_vector);

      vector<double> errors(dimension);
      for (size_t j = 0; j < dimension; j++) {
        errors[j] = abs(realResult[j] - vec_result[j]) / abs(realResult[j]);
      }

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

    delete lintrans_glk;
    lintrans_glk = nullptr;

#ifdef PROF_DEBUG
    seal::Plaintext result_pt;
    decryptor->decrypt(ct_prime, result_pt);

    std::vector<double> vec_result;
    ckks_encoder->decode(result_pt, vec_result);
    for (size_t i = 0; i < input_size; i++) {

      std::cout << "Result: " << vec_result[i] << endl;
    }
    cout << "Slot count:: " << slot_count << endl;
    cout << "Dimension:: " << dimension << endl;
    std::vector<double> realResult =
        getLinearTransformVector(input_matrix, input_vector);

#endif

    return ret;
  }

  // implemented using
  // https://github.com/MarwanNour/SEAL-FYP-Logistic-Regression/blob/master/polynomial.cpp
  TestResult test_poly_eval(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = ckks_encoder->slot_count();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // to be implemented ----->>>>  get x, degree and coeffs vector from the
    // arguments
    double x = 1.0f;
    // depth has to be ceil(log2(degree))
    const vector<double> &coeffs = tparams.polynomial_coeffs;
    size_t degree = coeffs.size() - 1;

    // size_t degree = 7;
    // vector<double> coeffs(degree + 1);

#ifdef PROF_DEBUG
    cout << "Degree of Polynomial = " << degree << endl;
    cout << "x = " << x << endl;
#endif

    seal::Plaintext ptx;
    ckks_encoder->encode(x, this->_params.scale(), ptx);
    seal::Ciphertext ctx;
    encryptor->encrypt(ptx, ctx);

    vector<seal::Plaintext> plain_coeffs(degree + 1);
    for (size_t i = 0; i < degree + 1; i++) {
      // coeffs[i] = (double)rand() / RAND_MAX;
      ckks_encoder->encode(coeffs[i], this->_params.scale(), plain_coeffs[i]);
#ifdef PROF_DEBUG
      cout << "x^" << i << " * (" << coeffs[i] << ")"
           << ", ";
#endif
    }

    seal::Plaintext plain_result;
    vector<double> result;

    vector<seal::Ciphertext> powers(degree + 1);

    // start timing here

    for (unsigned int k = 0; k < iters; k++) {
      start = steady_clock::now();

      powers[1] = ctx;

      vector<int> levels(degree + 1, 0);
      levels[1] = 0;
      levels[0] = 0;

      for (size_t i = 2; i <= degree; i++) {
        // compute x^i
        int minlevel = i;
        int cand = -1;
        for (size_t j = 1; j <= i / 2; j++) {
          int k = i - j;
          //
          int newlevel = max(levels[j], levels[k]) + 1;
          if (newlevel < minlevel) {
            cand = j;
            minlevel = newlevel;
          }
        }
        levels[i] = minlevel;
        // use cand
        if (cand < 0)
          throw runtime_error("error");
#ifdef PROF_DEBUG
        cout << "levels " << i << " = " << levels[i] << endl;
#endif
        // cand <= i - cand by definition
        seal::Ciphertext temp = powers[cand];
        evaluator->mod_switch_to_inplace(temp, powers[i - cand].parms_id());

        evaluator->multiply(temp, powers[i - cand], powers[i]);
        evaluator->relinearize_inplace(powers[i], *relin_keys);
        evaluator->rescale_to_next_inplace(powers[i]);
      }

      seal::Ciphertext enc_result;
      encryptor->encrypt(plain_coeffs[0], enc_result);
      seal::Ciphertext temp;

#ifdef PROF_DEBUG
      for (size_t i = 1; i <= degree; i++) {
        decryptor->decrypt(powers[i], plain_result);
        ckks_encoder->decode(plain_result, result);
        std::cout << "power  = " << result[0] << endl;
      }
#endif

      // result += a[i]*x[i]
      for (size_t i = 1; i <= degree; i++) {

        evaluator->mod_switch_to_inplace(plain_coeffs[i], powers[i].parms_id());
        evaluator->multiply_plain(powers[i], plain_coeffs[i], temp);
        evaluator->rescale_to_next_inplace(temp);

        evaluator->mod_switch_to_inplace(enc_result, temp.parms_id());
        enc_result.scale() = this->_params.scale();
        temp.scale() = this->_params.scale();
        evaluator->add_inplace(enc_result, temp);
      }
      end = steady_clock::now();

      decryptor->decrypt(enc_result, plain_result);
      ckks_encoder->decode(plain_result, result);
      double realResult = getPlainPolyEval(x, coeffs);

#ifdef PROF_DEBUG
      cout << "Evaluation done" << endl;
      cout << "Calculated result : " << result[0] << endl;
      cout << "Real result : " << realResult << endl;
#endif
      // cout << "Actual : " << result[0] << ", Expected : " << expected_result
      // << ", diff : " << abs(result[0] - expected_result) << endl;

      /*
      vector<double> errors;
      errors.push_back(abs(result[0] - realResult));
      */

      double error = abs(result[0] - realResult) / abs(realResult);

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.push_back(error);
    }

    return ret;
  }

  TestResult test_rotdot_product(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = ckks_encoder->slot_count();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

#if PROF_DEBUG
    size_t dimension = slot_count;
    cout << "Dimension:: " << dimension << endl;
    cout << "Slotcount: " << slot_count << endl;
#endif

    std::vector<double> inputs;

    inputs.reserve(slot_count);

    for (size_t i = 0; i < slot_count; i++) {
      inputs.push_back(i + 0.001 * i);
    }

    seal::Plaintext pt;
    ckks_encoder->encode(inputs, this->_params.scale(), pt);

    // 500D weight vector
    std::vector<double> weights;
    weights.reserve(slot_count);

    for (size_t i = 0; i < slot_count; i++) {
      weights.push_back((slot_count & 1) ? -1.0 : 2.00);
    }

    seal::Plaintext weight_pt;
    ckks_encoder->encode(weights, this->_params.scale(), weight_pt);
    seal::Ciphertext temp_ct;
    seal::Ciphertext ct;
    seal::Ciphertext weight_ct;
    encryptor->encrypt(weight_pt, weight_ct);

    for (unsigned int k = 0; k < iters; k++) {

      encryptor->encrypt(pt, ct);

      start = steady_clock::now();

      evaluator->multiply(ct, weight_ct, ct);
      evaluator->relinearize_inplace(ct, *relin_keys);
      // evaluator->multiply_plain_inplace(ct, weight_pt); -> does a SIMD
      // multiplication , for C-P dot product only
      evaluator->rescale_to_next_inplace(ct);

      // ----------------------- Algorithm 1 --------------------------
      // Sum slots of ciphertext using D rotations
      // vector<seal::Ciphertext> rotations_output(dimension);
      //
      // for(size_t steps = 0; steps < dimension; steps++)
      // {
      //   evaluator->rotate_vector(ct, steps, *galois_keys,
      //   rotations_output[steps]);
      // }
      // evaluator->add_many(rotations_output, ct);

      // ------------------------- Algorithm 2 ------------------------------
      // Sum the slots of ciphertext using log(D) rotations
      for (size_t i = 1; i <= slot_count / 2; i <<= 1) {
        evaluator->rotate_vector(ct, i, *galois_keys, temp_ct);
        evaluator->add_inplace(ct, temp_ct);
      }

      end = steady_clock::now();

      seal::Plaintext result_pt;
      decryptor->decrypt(ct, result_pt);

      std::vector<double> vec_result;
      ckks_encoder->decode(result_pt, vec_result);

      double realResult = std::inner_product(inputs.begin(), inputs.end(),
                                             weights.begin(), 0.0);

      /*
      vector<double> errors;
      errors.push_back(abs(vec_result[0] - realResult));
      */

      double error = abs(vec_result[0] - realResult) / abs(realResult);

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.push_back(error);
    }

#ifdef PROF_DEBUG
    seal::Plaintext result_pt;
    decryptor->decrypt(ct, result_pt);

    std::vector<double> vec_result;
    ckks_encoder->decode(result_pt, vec_result);

    std::cout << "Result: " << vec_result[0] << endl;
    cout << "True result: "
         << std::inner_product(inputs.begin(), inputs.end(), weights.begin(),
                               0.0)
         << endl;
#endif

    return ret;
  }

  // FHE Primitivies
  TestResult test_add(const TestParams &tparams) const {
    // Probably should have a block like this at the start of every runtime test
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = ckks_encoder->slot_count();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // Setup
    vector<double> arg0, arg1;
    seal::Ciphertext c0, c1, c2;

    // Run and time tests
    for (unsigned int i = 0; i < iters; i++) {
      // Get random input
      c0 = this->rand_ctext(arg0);
      c1 = this->rand_ctext(arg1);
      assert(c0.size() == c1.size());
      // Run the test
      start = steady_clock::now();
      evaluator->add(c0, c1, c2);
      end = steady_clock::now();
      // Get the result of the homomorphic computation
      seal::Plaintext pt;
      decryptor->decrypt(c2, pt);
      vector<double> enc_result;
      ckks_encoder->decode(pt, enc_result);
      // Get result done in the plain as well, and find the error
      vector<double> errors(arg0.size());
      for (size_t j = 0; j < arg0.size(); j++) {
        double realResult = arg0[j] + arg1[j];
        errors[j] = abs(realResult - enc_result[j]) / abs(realResult);
      }
      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

    return ret;
  }

  TestResult test_plain_add(const TestParams &tparams) const {
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;

    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = ckks_encoder->slot_count();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // Setup
    vector<double> arg0(this->ckks_encoder->slot_count(), 1.0f);
    seal::Ciphertext c0, c1;
    seal::Plaintext p1;

    c0 = this->rand_ctext(arg0);
    ckks_encoder->encode(arg0, this->_params.scale(), p1);

    for (unsigned int i = 0; i < iters; i++) {
      c0 = this->rand_ctext(arg0);

      start = steady_clock::now();
      evaluator->add_plain(c0, p1, c1);
      end = steady_clock::now();

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
    }

    return ret;
  }

  TestResult test_plain_mult(const TestParams &tparams) const {
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;

    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = ckks_encoder->slot_count();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // Setup
    vector<double> arg0(this->ckks_encoder->slot_count(), 1.0f);
    seal::Ciphertext c0, c1;
    seal::Plaintext p1;

    c0 = this->rand_ctext(arg0);
    ckks_encoder->encode(arg0, this->_params.scale(), p1);

    for (unsigned int i = 0; i < iters; i++) {
      c0 = this->rand_ctext(arg0);

      start = steady_clock::now();
      evaluator->multiply_plain(c0, p1, c1);
      end = steady_clock::now();

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
    }

    return ret;
  }

  TestResult test_chain(const TestParams &tparams) const {
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = ckks_encoder->slot_count();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // Setup
    vector<double> arg0;
    seal::Ciphertext c0;

    unsigned int depth = this->_params.depth() - 1;

    for (unsigned int i = 0; i < iters; i++) {
      // Get random input
      c0 = this->rand_ctext(arg0);

      // Run the test
      start = steady_clock::now();
      for (unsigned int j = 0; j < depth; j++) {
        evaluator->square_inplace(c0);
        evaluator->relinearize_inplace(c0, *relin_keys);
        evaluator->rescale_to_next_inplace(c0);
      }
      end = steady_clock::now();
      // Do the plain computation
      for (unsigned int j = 0; j < depth; j++) {
        for (double &d : arg0) {
          d *= d;
        }
      }

      // Get the result of the homomorphic computation
      seal::Plaintext pt;
      decryptor->decrypt(c0, pt);
      vector<double> enc_result;
      ckks_encoder->decode(pt, enc_result);

      // Get result done in the plain as well, and find the error
      vector<double> errors(arg0.size());
      for (size_t j = 0; j < arg0.size(); j++) {
        errors[j] = abs((arg0[j]) - enc_result[j]) / abs(arg0[j]);
      }
      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

    return ret;
  }

  // Includes relinearization and rescaling
  TestResult test_multiply(const TestParams &tparams) const {
    // Probably should have a block like this at the start of every runtime test
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = ckks_encoder->slot_count();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // Setup
    vector<double> arg0, arg1;
    seal::Ciphertext c0, c1, c2;

    // Run and time tests
    for (unsigned int i = 0; i < iters; i++) {
      // Get random input
      c0 = this->rand_ctext(arg0);
      c1 = this->rand_ctext(arg1);
      assert(c0.size() == c1.size());

      // Run the test
      start = steady_clock::now();
      evaluator->multiply(c0, c1, c2);
      evaluator->relinearize_inplace(c2, *relin_keys);
      evaluator->rescale_to_next_inplace(c2);
      end = steady_clock::now();

      // Get the result of the homomorphic computation
      seal::Plaintext pt;
      decryptor->decrypt(c2, pt);
      vector<double> enc_result;
      ckks_encoder->decode(pt, enc_result);

      // Get result done in the plain as well, and find the error
      vector<double> errors(arg0.size());
      for (size_t j = 0; j < arg0.size(); j++) {
        double realResult = arg0[j] * arg1[j];
        errors[j] = abs(realResult - enc_result[j]) / abs(realResult);
      }
      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

    return ret;
  }

  TestResult test_multiply_no_relin(const TestParams &tparams) const {
    // Probably should have a block like this at the start of every runtime test
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = ckks_encoder->slot_count();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // Setup
    vector<double> arg0, arg1;
    seal::Ciphertext c0, c1, c2;

    // Run and time tests
    for (unsigned int i = 0; i < iters; i++) {
      // Get random input
      c0 = this->rand_ctext(arg0);
      c1 = this->rand_ctext(arg1);
      assert(c0.size() == c1.size());

      // Run the test
      start = steady_clock::now();
      evaluator->multiply(c0, c1, c2);
      end = steady_clock::now();

      // Get the result of the homomorphic computation
      seal::Plaintext pt;
      decryptor->decrypt(c2, pt);
      vector<double> enc_result;
      ckks_encoder->decode(pt, enc_result);

      // Get result done in the plain as well, and find the error
      vector<double> errors(arg0.size());
      for (size_t j = 0; j < arg0.size(); j++) {
        double realResult = arg0[j] * arg1[j];
        errors[j] = abs(realResult - enc_result[j]) / abs(realResult);
      }
      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

    return ret;
  }

  TestResult test_rotate(const TestParams &tparams) const {
    // Probably should have a block like this at the start of every runtime test
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = ckks_encoder->slot_count();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    seal::Ciphertext c, dest;
    vector<double> args;

    const size_t rotate_amount = 1;

    for (unsigned int i = 0; i < iters; i++) {
      // size_t rotate_amount = rand() % ckks_encoder->slot_count();
      c = rand_ctext(args);
      start = steady_clock::now();
      evaluator->rotate_vector(c, rotate_amount, *galois_keys, dest);
      end = steady_clock::now();

      seal::Plaintext pt;
      decryptor->decrypt(dest, pt);
      vector<double> hom_rotated;
      ckks_encoder->decode(pt, hom_rotated);
      vector<double> errors(args.size());
      assert(args.size() == slot_count);
      assert(hom_rotated.size() == slot_count);
      for (size_t j = 0; j < errors.size(); j++) {
        errors[j] =
            abs(hom_rotated[j] - args[(j + rotate_amount) % slot_count]) /
            abs(args[(j + rotate_amount) % slot_count]);
      }
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
    }

    return ret;
  }

  TestResult test_relin(const TestParams &tparams) const {
    // Probably should have a block like this at the start of every runtime test
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = ckks_encoder->slot_count();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // Setup
    vector<double> arg0, arg1;
    seal::Ciphertext c0 = this->rand_ctext(arg0);
    seal::Ciphertext c1 = this->rand_ctext(arg1);
    seal::Ciphertext c2, c3;
    evaluator->multiply(c0, c1, c2);

    // Run and time tests
    for (unsigned int i = 0; i < iters; i++) {
      start = steady_clock::now();
      evaluator->relinearize(c2, *relin_keys, c3);
      end = steady_clock::now();
      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      // Get the result of the homomorphic computation
      seal::Plaintext pt;
      decryptor->decrypt(c3, pt);
      vector<double> enc_result;
      ckks_encoder->decode(pt, enc_result);

      // Get result done in the plain as well, and find the error
      vector<double> errors(arg0.size());
      for (size_t j = 0; j < arg0.size(); j++) {
        double realResult = arg0[j] * arg1[j];
        errors[j] = abs(realResult - enc_result[j]) / abs(realResult);
      }
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

    return ret;
  }

  TestResult test_rescale(const TestParams &tparams) const {
    // Probably should have a block like this at the start of every runtime test
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = ckks_encoder->slot_count();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // Setup
    vector<double> args;
    seal::Ciphertext c0 = this->rand_ctext(args);
    seal::Ciphertext c1 = this->rand_ctext(args);
    seal::Ciphertext c2, cmult;
    evaluator->multiply(c0, c1, cmult);
    evaluator->relinearize_inplace(cmult, *relin_keys);

    // Run and time tests
    for (unsigned int i = 0; i < iters; i++) {
      start = steady_clock::now();
      evaluator->rescale_to_next(cmult, c2);
      end = steady_clock::now();
      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
    }

    return ret;
  }

  // No bootstrapping for SEAL
  TestResult test_bootstrap(const TestParams &tparams) const {
    TestResult t;
    return t;
  }

  TestResult test_encode_error(const TestParams &tparams) const {

    unsigned int iters = tparams.iterations();
    TestResult ret;
    // Make sure to reserve!
    unsigned int slot_count = ckks_encoder->slot_count();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // Setup
    vector<double> arg0;
    seal::Ciphertext c0;

    // Run and time tests
    for (unsigned int i = 0; i < iters; i++) {
      // Get random input
      c0 = this->rand_ctext(arg0);

      // Get the result of the homomorphic computation
      seal::Plaintext pt;
      decryptor->decrypt(c0, pt);
      vector<double> enc_result;
      ckks_encoder->decode(pt, enc_result);
      // Get result done in the plain as well, and find the error
      vector<double> errors(arg0.size());
      for (size_t j = 0; j < arg0.size(); j++) {
        errors[j] = abs(arg0[j] - enc_result[j]) / abs(arg0[j]);
      }

      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

    return ret;
  }

  // End-to-end functions

  seal::Ciphertext single_dot(const seal::Ciphertext &ct0,
                              seal::Ciphertext &ct1) const {
    seal::Ciphertext ret, tmp;
    unsigned int slot_count = ckks_encoder->slot_count();
    evaluator->multiply(ct0, ct1, ret);
    evaluator->relinearize_inplace(ret, *relin_keys);
    evaluator->rescale_to_next_inplace(ret);
    for (size_t i = 1; i <= slot_count / 2; i <<= 1) {
      evaluator->rotate_vector(ret, i, *galois_keys, tmp);
      evaluator->add_inplace(ret, tmp);
    }
    return ret;
  }

  TestResult test_dot(const TestParams &tparams) const {
    // Probably should have a block like this at the start of every runtime test
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    // Make sure to reserve!
    unsigned int slot_count = ckks_encoder->slot_count();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    unsigned int dot_vec_len =
        tparams.dot_len / slot_count + (tparams.dot_len % slot_count ? 1 : 0);
    vector<seal::Ciphertext> v1, v2;
    v1.resize(dot_vec_len);
    v2.resize(dot_vec_len);

    for (unsigned int j = 0; j < iters; j++) {
      seal::Ciphertext dot_result, tmp;
      vector<vector<double>> args1(v1.size());
      vector<vector<double>> args2(v2.size());

      for (size_t i = 0; i < v1.size(); i++) {
        v1[i] = this->rand_ctext(args1[i]);
        v2[i] = this->rand_ctext(args2[i]);
      }

      start = steady_clock::now();
      for (size_t i = 0; i < v1.size(); i++) {
        if (!i) {
          dot_result = single_dot(v1[i], v2[i]);
        } else {
          tmp = single_dot(v1[i], v2[i]);
          evaluator->add_inplace(dot_result, tmp);
        }
      }
      // Not needed - single_dot will do this
      /*
      evaluator->relinearize_inplace(dot_result, *relin_keys);
      evaluator->rescale_to_next_inplace(dot_result);
      */
      end = steady_clock::now();

      seal::Plaintext pt;
      decryptor->decrypt(dot_result, pt);
      vector<double> hom_rotated;
      ckks_encoder->decode(pt, hom_rotated);

      double realResult = 0.0f;
      // Plain result for errors
      for (size_t i = 0; i < v1.size(); i++) {
        realResult += plain_dot(args1[i], args2[i]);
      }

      for (size_t i = 0; i < v1.size(); i++) {
        ret.errors.push_back(abs(realResult - hom_rotated.at(i)) /
                             abs(realResult));
      }

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
    }

    return ret;
  }

  // Sizes
  // Returns only a single element in their vector
  // Technically there's a precision loss with floating-point, but the results
  // shouldn't be nearly large enough for issues
  TestResult ciphertext_size(const TestParams &tparams) const {
    stringstream ss;
    vector<double> tmp;
    seal::Ciphertext c = this->rand_ctext(tmp);
    auto bytes_written = c.save(ss);
    TestResult t;
    t.runtimes = {(long double)bytes_written};
    return t;
  }

  TestResult secret_key_size(const TestParams &tparams) const {
    stringstream ss;
    auto bytes_written = secret_key->save(ss);
    TestResult t;
    t.runtimes = {(long double)bytes_written};
    return t;
  }

  TestResult public_key_size(const TestParams &tparams) const {
    stringstream ss;
    auto bytes_written = public_key->save(ss);
    TestResult t;
    t.runtimes = {(long double)bytes_written};
    return t;
  }

  // No bootstrapping for SEAL
  TestResult bsk_size(const TestParams &tparams) const {
    TestResult t;
    t.runtimes = {0.0};
    return t;
  }

  // KSKs not public in SEAL
  TestResult ksk_size(const TestParams &tparams) const {
    TestResult t;
    t.runtimes = {0.0};
    return t;
  }

  TestResult params_size(const TestParams &tparams) const {
    stringstream ss;
    auto bytes_written = parms->save(ss);
    TestResult t;
    t.runtimes = {(long double)bytes_written};
    return t;
  }

  TestResult ctext_modulus_bits() const {
    TestResult t;
    const auto &context_data = (*context).key_context_data();
    unsigned int cnt = context_data->total_coeff_modulus_bit_count();
    t.runtimes = {(long double)cnt};
    return t;
  }

  TestResult scaling_factor() const {
    TestResult t;
    vector<double> arg0;
    seal::Ciphertext c0;
    c0 = this->rand_ctext(arg0);
    const auto scale_bits = log2(c0.scale());
    t.runtimes = {(long double)scale_bits};
    return t;
  }

  TestResult num_columns() const {
    TestResult t;
    unsigned int cols =
        context->first_context_data()->parms().coeff_modulus().size();

    t.runtimes = {(long double)cols};
    return t;
  }
};

#endif
