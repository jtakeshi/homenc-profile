#ifndef HOMENC_PROFILE_H
#define HOMENC_PROFILE_H

#include <algorithm>
#include <exception>
#include <map>
#include <string>

using std::map;
using std::string;

#include "Profiler.h"

Profiler::~Profiler() {}

#ifndef NSEAL
#include "SEALProfiler.h"
#endif

#ifndef NPALISADE
#include "PALISADEProfiler.h"
#endif

#ifndef NHELIB
#include "HElibProfiler.h"
#endif

#ifndef NHEAAN
#include "HEAANProfiler.h"
#endif

enum ProfilerType {
#ifndef NSEAL
  SEAL,
#endif
#ifndef NPALISADE
  PALISADE,
  PALISADE_OPT,
#endif
#ifndef NHELIB
  HELIB,
#endif
#ifndef NHEAAN
  HEAAN
#endif
};

#ifndef NSEAL
const static string SEAL_STR = "seal";
#endif
#ifndef NPALISADE
const static string PALISADE_STR = "palisade";
const static string PALISADE_OPT_STR = "palisade_opt";
#endif
#ifndef NHELIB
const static string HELIB_STR = "helib";
#endif
#ifndef NHEAAN
const static string HEAAN_STR = "heaan";
#endif

const static map<string, ProfilerType> PROF_MAP = {
#ifndef NSEAL
    {SEAL_STR, SEAL},
#endif
#ifndef NPALISADE
    {PALISADE_STR, PALISADE},
    {PALISADE_OPT_STR, PALISADE_OPT},
#endif
#ifndef NHELIB
    {HELIB_STR, HELIB},
#endif
#ifndef NHEAAN
    {HEAAN_STR, HEAAN}
#endif
};

string available_libraries() {
  string ret = "";
  for (const auto &it : PROF_MAP) {
    ret += it.first;
    ret += ' ';
  }
  return ret;
}

std::shared_ptr<Profiler> ProfilerFactory(const ProfilerType pt,
                                          const HEParams &parms) {
  switch (pt) {
#ifndef NSEAL
  case SEAL: {
    return std::shared_ptr<Profiler>(new SEALProfiler(parms));
  }
#endif
#ifndef NPALISADE
  case PALISADE: {
    return std::shared_ptr<Profiler>(new PALISADEProfiler(parms));
  }
  case PALISADE_OPT: {
    return std::shared_ptr<Profiler>(new PALISADEProfiler(parms));
  }
#endif
#ifndef NHELIB
  case HELIB: {
    return std::shared_ptr<Profiler>(new HElibProfiler(parms));
  }
#endif
#ifndef NHEAAN
  case HEAAN: {
    return std::shared_ptr<Profiler>(new HEAANProfiler(parms));
  }
#endif
  default: {
    throw std::runtime_error("Invalid ProfilerType");
  }
  }
}

std::shared_ptr<Profiler> ProfilerFactory(const string &ptype,
                                          const HEParams &parms) {
  if (PROF_MAP.find(ptype) == PROF_MAP.end()) {
    string errstr = "Invalid profiler type: ";
    errstr += ptype;
    throw std::runtime_error(errstr);
  }
  return ProfilerFactory(PROF_MAP.find(ptype)->second, parms);
}

#endif
