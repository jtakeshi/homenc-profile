#ifndef PROFILER_H
#define PROFILER_H

#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <exception>
#include <fstream>
#include <functional>
#include <sstream>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

using std::exception;
using std::pair;
using std::string;
using std::stringstream;
using std::unordered_map;
using std::vector;
// using namespace std::placeholders; //Needed for std::bind

// What time type to use
using measure_typ = std::chrono::nanoseconds;

// Define function strings here!
const static string TEST_ADD = "add";
const static string TEST_MULT = "mult";
const static string TEST_MULT_NO_RELIN = "mult_no_relin";
const static string TEST_ROTATE = "rotate";
const static string TEST_RELIN = "relin";
const static string TEST_RESCALE = "rescale";
const static string TEST_BOOTSTRAP = "bootstrap";
const static string TEST_CHAIN = "chain";
const static string TEST_ENCODE_ERROR = "encode_error";
const static string TEST_PLAIN_ADD = "plain_add";
const static string TEST_PLAIN_MULT = "plain_mult";

const static string SIZE_CTEXT = "size_ctext";
const static string SIZE_PUBLIC_KEY = "size_public_key";
const static string SIZE_SECRET_KEY = "size_secret_key";
const static string SIZE_BSK = "size_bootstrap_key";
const static string SIZE_KSK = "size_keyswitch_key";
const static string SIZE_PARAMS = "size_params";
const static string SIZE_SCALING_FACTOR = "scaling_factor";

const static string TEST_DOT = "dot";
const static string TEST_LINEAR = "linear";
const static string TEST_ROT_DOT = "rotdot";
const static string TEST_POLY_EVAL = "polyeval";

const static string CTEXT_MODULUS_BITS = "ctext_modulus_bits";
const static string NUM_COLUMNS = "num_columns";

const static std::set<string> AVAILABLE_TESTS = {
    TEST_ADD,           TEST_MULT,         TEST_MULT_NO_RELIN,  TEST_ROTATE,
    TEST_BOOTSTRAP,     TEST_RELIN,        TEST_RESCALE,        TEST_BOOTSTRAP,
    SIZE_CTEXT,         SIZE_PUBLIC_KEY,   SIZE_SECRET_KEY,     SIZE_BSK,
    SIZE_KSK,           SIZE_PARAMS,       SIZE_SCALING_FACTOR, TEST_DOT,
    TEST_LINEAR,        TEST_ROT_DOT,      TEST_POLY_EVAL,      TEST_CHAIN,
    CTEXT_MODULUS_BITS, TEST_ENCODE_ERROR, NUM_COLUMNS,         TEST_PLAIN_ADD,
    TEST_PLAIN_MULT};

// Encapsulates the basic parameters we want to vary
class HEParams {
private:
  unsigned int _security = 128;
  unsigned int _depth = 1;
  unsigned int _threads = 1;
  double _scale = 0;
  unsigned int _scale_bits = 40;
  unsigned int _edge_modulus_bits = 60;
  unsigned int _bootstrap = 0;
  bool _optimized = false;

public:
  const static unsigned int SCALE_BITS_DEFAULT = 40;
  const static unsigned int EDGE_BITS_DEFAULT = 60;

  // Argue 0 to leave a parameter unspecified
  HEParams(const unsigned int depth_in = 1, const unsigned int boostrap_in = 0,
           const unsigned int scale_bits_in = SCALE_BITS_DEFAULT,
           const unsigned int edge_bits_in = EDGE_BITS_DEFAULT,
           const unsigned int security_in = 128,
           const unsigned int threads_in = 1, const bool optimized_in = false) {

    _optimized = optimized_in;

    if (depth_in) {
      _depth = depth_in;
    }
    if (security_in) {
      _security = security_in;
    }
    if (threads_in) {
      _threads = threads_in;
    }
    if (scale_bits_in) {
      _scale_bits = scale_bits_in;
    }
    _scale = std::pow(2.0, _scale_bits);
    if (edge_bits_in) {
      _edge_modulus_bits = edge_bits_in;
    }
    _bootstrap = boostrap_in;

    // Yeah I hardcoded it, come at me
    if ((_security != 128) && (_security != 192) && (_security != 256)) {
      string errstr = "Invalid security level: ";
      errstr += std::to_string(_security);
      throw std::runtime_error(errstr);
    }
  }

  inline unsigned int bootstrap() const { return _bootstrap; }
  inline unsigned int security() const { return _security; }
  inline unsigned int depth() const { return _depth; }
  inline unsigned int threads() const { return _threads; }
  inline unsigned int scale_bits() const { return _scale_bits; }
  inline double scale() const { return _scale; }
  inline unsigned int edge_modulus_bits() const { return _edge_modulus_bits; }
  inline bool optimized() const { return _optimized; }
};

// Parameters for testing
class TestParams {
private:
  unsigned int _iterations = 0;
  unsigned int _dimension = 0;
  /*
    unsigned int _mat_height = 0;
    unsigned int _mat_width = 0;
    vector<vector<double> > _reg_x;
    vector<double> _reg_y;
  */

public:
  vector<double> polynomial_coeffs;
  bool native_alg = true;
  unsigned int dot_len = 1;

  const static unsigned int ITERATIONS_DEFAULT = 50;
  const static unsigned int DIMENSION_MAX = 96;

  // Don't default construct - will set to delete later
  TestParams(){};

  TestParams(const unsigned int iters_in = 1, const unsigned int dims_in = 0)
      : _iterations(iters_in), _dimension(dims_in) {}

  TestParams(const string &s) {
    // TODO initialize directly from CLI argument
  }

  unsigned int iterations() const { return _iterations; }
  /*
    unsigned int dot_len() const {return 10;}
    unsigned int matrix_height() const {return _mat_height;}
    unsigned int matrix_width() const {return _mat_width;}
  */
  unsigned int dimension() const { return _dimension; }

  /*
    void read_reg_csv(std::istream & is, bool reload = false){
      const static char DELIM = ',';
      if(reload){
        _reg_x.clear();
        _reg_y.clear();
      }
      else{
        if(_reg_x.size() && _reg_y.size()){
          return;
        }
        else{
          if(_reg_x.size() || _reg_y.size()){
            throw std::runtime_error("Linreg x and y not both (non)empty");
          }
        }
      }

      if(_reg_x.size() || _reg_y.size()){
        throw std::runtime_error("Linreg x and y not both (non)empty");
      }

      //Not really efficient
      string line, val_str;
      while(getline(is, line)){
        std::stringstream ss(line);
        _reg_x.resize(_reg_x.size() + 1);
        while(getline(is, val_str, DELIM)){
          std::stringstream dbl_ss(val_str);
          double d;
          dbl_ss >> d;
          _reg_x.back().push_back(d);
        }
        double tmp = _reg_x.back().back();
        _reg_y.push_back(tmp);
      }
      return;
    }

    void read_reg_csv(const string & fname, bool reload = false){
      std::ifstream ifs(fname);
      if(!ifs.good()){
        string errstr = "Failed to open ";
        errstr += fname;
        throw std::runtime_error(errstr);
      }
      read_reg_csv(ifs, reload);
      return;
    }
  */
};

struct TestResult {
  vector<long double> runtimes;
  vector<double> errors;
  unsigned int batchsize = 0;
};

// Main class of this file.
class Profiler {
private:
  // Inefficient, but string comparisons aren't what we're timing
  inline TestResult run_test_internal(const string &testname,
                                      const TestParams &tp) const {
    if (testname == TEST_ADD) {
      return test_add(tp);
    }
    if (testname == TEST_MULT) {
      return test_multiply(tp);
    }
    if (testname == TEST_MULT_NO_RELIN) {
      return test_multiply_no_relin(tp);
    }
    if (testname == TEST_ROTATE) {
      return test_rotate(tp);
    }
    if (testname == TEST_RELIN) {
      return test_relin(tp);
    }
    if (testname == TEST_RESCALE) {
      return test_rescale(tp);
    }
    if (testname == TEST_BOOTSTRAP) {
      return test_bootstrap(tp);
    }
    if (testname == SIZE_CTEXT) {
      return ciphertext_size(tp);
    }
    if (testname == SIZE_BSK) {
      return bsk_size(tp);
    }
    if (testname == SIZE_PUBLIC_KEY) {
      return public_key_size(tp);
    }
    if (testname == SIZE_SECRET_KEY) {
      return secret_key_size(tp);
    }
    if (testname == SIZE_PARAMS) {
      return params_size(tp);
    }
    if (testname == SIZE_SCALING_FACTOR) {
      return scaling_factor();
    }
    if (testname == TEST_DOT) {
      return test_dot(tp);
    }
    if (testname == TEST_LINEAR) {
      return test_linear_transform(tp);
    }
    if (testname == TEST_ROT_DOT) {
      return test_rotdot_product(tp);
    }
    if (testname == TEST_POLY_EVAL) {
      return test_poly_eval(tp);
    }
    if (testname == TEST_BOOTSTRAP) {
      return test_bootstrap(tp);
    }
    if (testname == TEST_CHAIN) {
      return test_chain(tp);
    }
    if (testname == CTEXT_MODULUS_BITS) {
      return ctext_modulus_bits();
    }
    if (testname == TEST_ENCODE_ERROR) {
      return test_encode_error(tp);
    }
    if (testname == NUM_COLUMNS) {
      return num_columns();
    }
    if (testname == TEST_PLAIN_ADD) {
      return test_plain_add(tp);
    }
    if (testname == TEST_PLAIN_MULT) {
      return test_plain_mult(tp);
    }

    string errstr = "Test not recognized: ";
    errstr += testname;
    throw std::runtime_error(errstr);
  }

protected:
  HEParams _params;

public:
  Profiler(const HEParams &params_in) : _params(params_in) {}

  virtual ~Profiler();

  // Per-library function to set parameters
  // This can be called in each subclass's constructor
  virtual void reset_params(const HEParams &params_in) = 0;

  virtual string profiler_name() const = 0;

  static inline bool test_valid(const string &testname) {
    return AVAILABLE_TESTS.find(testname) != AVAILABLE_TESTS.end();
  }

  // Using run_tests is preferred - you can check all arguments before spending
  // time running them
  TestResult run_test(const string &testname, const TestParams &tparams,
                      bool check = true) const {
    if (check && !test_valid(testname)) {
      string errstr = "Function ";
      errstr += testname;
      errstr += " not registered!";
      throw std::runtime_error(errstr);
    }

    return run_test_internal(testname, tparams);
  }

  // It is assumed that TestParams is a small-ish and easily copied object
  vector<TestResult>
  run_tests(const vector<pair<string, TestParams>> &tparams_vec) const {
    vector<string> invalid_tests;
    for (const auto &x : tparams_vec) {
      if (!test_valid(x.first)) {
        invalid_tests.push_back(x.first);
      }
    }
    if (!invalid_tests.empty()) {
      string errstr = "Functions not registered: ";
      for (const string &x : invalid_tests) {
        errstr += x;
        errstr += ' ';
      }
      throw std::runtime_error(errstr);
    }
    vector<TestResult> ret(tparams_vec.size());
    for (size_t i = 0; i < tparams_vec.size(); i++) {
      // Already checked all tests
      ret[i] = run_test(tparams_vec[i].first, tparams_vec[i].second, false);
    }
    return ret;
  }

  stringstream
  csv_output(const vector<pair<string, TestParams>> &tparams_vec) const {
    vector<TestResult> result = run_tests(tparams_vec);
    if (result.size() != tparams_vec.size()) {
      string errstr = "Mismatched size: ";
      errstr += std::to_string(result.size());
      errstr += ' ';
      errstr += std::to_string(tparams_vec.size());
      throw std::logic_error(errstr);
    }
    stringstream ss;
    for (size_t i = 0; i < tparams_vec.size(); i++) {
      bool some_output = false;
      // Runtimes
      if (result[i].runtimes.size()) {
        some_output = true;
        ss << this->profiler_name() << ',' << tparams_vec[i].first
           << ",runtimes,";
        for (const long double d : result[i].runtimes) {
          ss << d << ',';
        }
      }
      ss << '\n';
      // Errors
      if (result[i].errors.size()) {
        some_output = true;
        ss << this->profiler_name() << ',' << tparams_vec[i].first
           << ",errors,";
        for (const long double d : result[i].errors) {
          ss << d << ',';
        }
        ss << '\n';
      }
      // Batchsize
      if (result[i].batchsize && some_output) {
        ss << this->profiler_name() << ',' << tparams_vec[i].first
           << ",batchsize," << result[i].batchsize << '\n';
      }
      // TODO find throughput from latency and batch size
    }
    return ss;
  }

  // Use me in a usage() function
  static inline string available_tests() {
    string ret = "";
    for (const string &x : AVAILABLE_TESTS) {
      ret += x;
      ret += ' ';
    }
    return ret;
  }

  // Tests should take in a set of parameters and return a list of long double,
  // representing runtimes for each function

  // FHE Primitivies
  virtual TestResult test_add(const TestParams &tparams) const = 0;
  virtual TestResult test_multiply(const TestParams &tparams) const = 0;
  virtual TestResult test_chain(const TestParams &tparams) const = 0;
  virtual TestResult
  test_multiply_no_relin(const TestParams &tparams) const = 0;
  virtual TestResult test_rotate(const TestParams &tparams) const = 0;
  virtual TestResult test_relin(const TestParams &tparams) const = 0;
  virtual TestResult test_rescale(const TestParams &tparams) const = 0;
  virtual TestResult test_bootstrap(const TestParams &tparams) const = 0;
  virtual TestResult test_encode_error(const TestParams &tparams) const = 0;
  virtual TestResult test_plain_add(const TestParams &tparams) const = 0;
  virtual TestResult test_plain_mult(const TestParams &tparams) const = 0;

  // End-to-end functions
  virtual TestResult test_dot(const TestParams &tparams) const = 0;
  virtual TestResult test_linear_transform(const TestParams &tparams) const = 0;
  virtual TestResult test_rotdot_product(const TestParams &tparams) const = 0;
  virtual TestResult test_poly_eval(const TestParams &tparams) const = 0;
  // Sizes
  // Returns only a single element in their vector
  // Technically there's a precision loss with floating-point, but the results
  // shouldn't be nearly large enough for issues
  virtual TestResult ciphertext_size(const TestParams &tparams) const = 0;
  virtual TestResult secret_key_size(const TestParams &tparams) const = 0;
  virtual TestResult public_key_size(const TestParams &tparams) const = 0;
  virtual TestResult bsk_size(const TestParams &tparams) const = 0;
  virtual TestResult params_size(const TestParams &tparams) const = 0;
  virtual TestResult ctext_modulus_bits() const = 0;
  virtual TestResult scaling_factor() const = 0;
  virtual TestResult num_columns() const = 0;
};

inline double plain_dot(const vector<double> &v1, const vector<double> &v2) {
  size_t len = std::min(v1.size(), v2.size());
  double ret = 0.0;
  for (size_t i = 0; i < len; i++) {
    ret += v1[i] * v2[i];
  }
  return ret;
}

static const double CENTER_DEFAULT = 1.0f;
static const double RADIUS_DEFAULT = 0.0f;

// Returns a number randomly chosen from [center - radius, center + radius]
inline double rand_plain_val(const double center = CENTER_DEFAULT,
                             const double radius = RADIUS_DEFAULT) {

  // https://stackoverflow.com/questions/686353/random-float-number-generation
  // srand (static_cast <unsigned> (time(0)));

  if (radius == 0.0f) {
    return center;
  }
  double rand_val = (center - radius) +
                    static_cast<double>(rand()) /
                        (static_cast<double>(RAND_MAX / ((center + radius) -
                                                         (center - radius))));
  return rand_val;
}

#endif
