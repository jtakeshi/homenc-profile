// g++ src/main.cpp -std=c++17 -O3 -Wall -Werror -fopenmp
// /usr/local/lib/libseal-3.7.a -I /usr/local/include/SEAL-3.7/ -I
// /usr/local/include/palisade/ -I /usr/local/include/palisade/cereal -I
// /usr/local/include/palisade/core -I /usr/local/include/palisade/pke -I
// /usr/local/include/palisade/ -L /usr/local/lib/ -l PALISADEcore -l
// PALISADEpke -DNHELIB -D NHEAAN

#ifndef PALISADEPROFILER_H
#define PALISADEPROFILER_H

#include "Profiler.h"
#include "SEALProfiler.h"
#include "linearAlgebra.h"
#include <cassert>
#include <ciphertext-ser.h>
#include <cmath>
#include <cryptocontext.h>
#include <cstdint>
#include <palisade.h>
#include <palisade/pke/palisade.h>
#include <scheme/ckks/ckks-ser.h>
#include <utils/serial.h>

// For bootstrapping only - might want to hide this with a flag
#include "dlt-palisade.h"

using namespace std::chrono;
using std::unique_ptr;
using namespace std;

bool IsPowerOfTwo(ulong x) { return (x != 0) && ((x & (x - 1)) == 0); }

lbcrypto::SecurityLevel params_security(const HEParams &p) {
  lbcrypto::SecurityLevel sec;
  switch (p.security()) {
  case 128: {
    sec = lbcrypto::SecurityLevel::HEStd_128_classic;
    break;
  }
  case 192: {
    sec = lbcrypto::SecurityLevel::HEStd_192_classic;
    break;
  }
  case 256: {
    sec = lbcrypto::SecurityLevel::HEStd_256_classic;
    break;
  }
  default: {
    std::string errstr = "Unrecognized security level: ";
    errstr += std::to_string(p.security());
    throw std::runtime_error(errstr);
  }
  }
  return sec;
}

void assign_optimized_params(const unsigned int depth, unsigned int &digits,
                             unsigned int &log2_N) {
  if (depth < 3) {
    digits = 3;
    log2_N = 13;
  } else if (depth <= 7) {
    digits = 8;
    log2_N = 14;
  } else if (depth <= 15) {
    digits = 6;
    log2_N = 15;
  } else if (depth <= 17) {
    digits = 9;
    log2_N = 15;
  } else if (depth == 18) {
    digits = 19;
    log2_N = 15;
  } else if (depth == 19) {
    digits = 20;
    log2_N = 15;
  }

  if (depth > 19) {
    throw runtime_error("Depth exceeded maximum size");
  }
}

class PALISADEProfiler : public Profiler {
private:
  lbcrypto::CryptoContext<lbcrypto::DCRTPoly> context;
  unique_ptr<lbcrypto::LPKeyPair<lbcrypto::DCRTPoly>> keyPair;

  unsigned int cols;
  std::string name;

  lbcrypto::Ciphertext<lbcrypto::DCRTPoly>
  rand_ctext(vector<double> &args) const {
    size_t slot_count = context->GetEncodingParams()->GetBatchSize();
    srand(time(NULL));

    args.clear();
    args.resize(slot_count);

    for (size_t i = 0; i < args.size(); i++) {
      // args[i] = (double)rand() / RAND_MAX;
      args[i] = rand_plain_val();
    }

    lbcrypto::Plaintext p = context->MakeCKKSPackedPlaintext(args);

    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> c =
        context->Encrypt(keyPair->publicKey, p);
    return c;
  }

  lbcrypto::Ciphertext<lbcrypto::DCRTPoly>
  custom_ctext(vector<double> &args, vector<double> &row) const {
    size_t slot_count = context->GetEncodingParams()->GetBatchSize();
    args.clear();
    args.resize(slot_count);

    int row_size = row.size();

    for (size_t i = 0; i < args.size(); i++) {
      args[i] = row[i % row_size];
    }

    lbcrypto::Plaintext p = context->MakeCKKSPackedPlaintext(args);

    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> c =
        context->Encrypt(keyPair->publicKey, p);

    return c;
  }

  // lbcrypto::Plaintext getCustomMessage(const double value) const {
  //
  //   lbcrypto::Plaintext p1;
  //   for (int i = 0; i < p1.size(); i++) {
  //     p1[i] = value;
  //   }
  //   return p1;
  // }

public:
  static const unsigned int EDGE_MODULUS_BITS = 60;

  PALISADEProfiler(const HEParams &params_in) : Profiler(params_in) {

    name =
        _params.optimized() ? "PALISADEProfiler_optimized" : "PALISADEProfiler";
    lbcrypto::RescalingTechnique rescaleTech =
        _params.optimized() ? lbcrypto::APPROXRESCALE : lbcrypto::EXACTRESCALE;

    if (this->_params.bootstrap()) {
      uint32_t n = getDegreeFromBSLevel(this->_params.bootstrap());
      
      if (n < (1 << 16)) {
        throw std::runtime_error("Need n >= 2^16");
      }
      
      uint32_t slots = n / 2;
      uint32_t levelsRemaining = this->_params.depth() - 1;

      // Code examples shamelessly stolen from the PALISADE CKKS Bootstrapping
      // example
      std::vector<uint32_t> dim1 = {0, 0};
      std::vector<uint32_t> levelBudget = {4, 4};

      /*
      #if NATIVEINT == 128
            lbcrypto::RescalingTechnique rescaleTech = lbcrypto::APPROXRESCALE;
      #else
            lbcrypto::RescalingTechnique rescaleTech = lbcrypto::EXACTRESCALE;
      #endif
      */
      // computes how many levels are needed for
      uint32_t approxModDepth = 9;
      uint32_t r = 6;
      MODE mode = OPTIMIZED;
      if (mode == OPTIMIZED) {
        if (rescaleTech == lbcrypto::APPROXRESCALE) {
          approxModDepth += r - 1;
        } else {
          approxModDepth += r;
        }
      }

      usint depth =
          levelsRemaining + approxModDepth + levelBudget[0] + levelBudget[1];
#if NATIVEINT == 128
      usint dcrtBits = 78;
#else
      usint dcrtBits = 59;
#endif

      this->cols = 3;
      // See
      // https://gitlab.com/palisade/palisade-development/-/blob/master/src/pke/lib/cryptocontextfactory.cpp#L621
      lbcrypto::DltCryptoContext<lbcrypto::DCRTPoly> boot_cc =
          lbcrypto::DltCryptoContextFactory<lbcrypto::DCRTPoly>::
              genCryptoContextCKKS(depth, dcrtBits, slots,
                                   lbcrypto::HEStd_128_classic, n, rescaleTech,
                                   lbcrypto::HYBRID, this->cols, 2,
#if NATIVEINT == 128
                                   89,
#else
                                   60,
#endif
                                   0, mode);

      // Turn on features
      boot_cc->Enable(ENCRYPTION);
      boot_cc->Enable(SHE);
      boot_cc->Enable(LEVELEDSHE);
      boot_cc->Enable(ADVANCEDSHE);
      boot_cc->Enable(FHE);

      const shared_ptr<lbcrypto::LPCryptoParametersCKKS<lbcrypto::DCRTPoly>>
          cryptoParams = std::static_pointer_cast<
              lbcrypto::LPCryptoParametersCKKS<lbcrypto::DCRTPoly>>(
              boot_cc->GetCryptoParameters());
      boot_cc->EvalBTSetup(levelBudget, dim1, slots);
      auto boot_keys = boot_cc->KeyGen();
      int32_t sparseFlag = (slots < n / 2) ? 1 : 0;
      boot_cc->EvalBTKeyGen(boot_keys.secretKey, sparseFlag);
      boot_cc->EvalMultKeyGen(boot_keys.secretKey);

      context = boot_cc;
      keyPair = unique_ptr<lbcrypto::LPKeyPair<lbcrypto::DCRTPoly>>(
          new lbcrypto::LPKeyPair<lbcrypto::DCRTPoly>(context->KeyGen()));
    } else {
      unsigned int desired_depth = params_in.depth();
      // Thanks to Yuiry for pointing this out
      if (desired_depth > 3) {
        // If more than 4 towers, use 3 digits
        this->cols = 3;
      } else if ((desired_depth >= 1) && (desired_depth <= 3)) {
        // If 2, 3 or 4 towers, use 2 digits
        this->cols = 2;
      } else {
        // if there is only 1 tower, use one digit
        this->cols = 1;
      }
    }

    if (!_params.optimized()) {
      // See
      // https://gitlab.com/palisade/palisade-development/-/blob/master/src/pke/lib/cryptocontextfactory.cpp#L621
      context = lbcrypto::CryptoContext<lbcrypto::DCRTPoly>(
          lbcrypto::CryptoContextFactory<lbcrypto::DCRTPoly>::
              genCryptoContextCKKS(_params.depth(), params_in.scale_bits(), 0,
                                   params_security(params_in), 0, rescaleTech,
                                   lbcrypto::HYBRID, 0, 2));

    } else {
      unsigned int num_digits = 0, log2_N = 0;
      assign_optimized_params(_params.depth(), num_digits, log2_N);
      unsigned int scaleDigits = _params.scale_bits();
      unsigned int relinWindow = 0;

      unsigned int cyclOrder = 1 << (log2_N + 1);
      unsigned int batchsize = cyclOrder >> 2;
      MODE mode = OPTIMIZED;
      // See
      // https://gitlab.com/palisade/palisade-development/-/blob/master/src/pke/lib/cryptocontextfactory.cpp#L582
      context = lbcrypto::CryptoContext<lbcrypto::DCRTPoly>(
          lbcrypto::CryptoContextFactory<lbcrypto::DCRTPoly>::
              genCryptoContextCKKSWithParamsGen(
                  cyclOrder, num_digits, scaleDigits, relinWindow, batchsize,
                  mode, _params.depth(), 2 /*Num. relin keys*/));
    }

    context->Enable(ENCRYPTION);
    context->Enable(SHE);
    context->Enable(LEVELEDSHE);

    keyPair = unique_ptr<lbcrypto::LPKeyPair<lbcrypto::DCRTPoly>>(
        new lbcrypto::LPKeyPair<lbcrypto::DCRTPoly>(context->KeyGen()));
    context->EvalMultKeyGen(keyPair->secretKey);
    // New bootstrapping PALISADE needs this for rotdot
    context->EvalSumKeyGen(keyPair->secretKey);
  }

  void reset_params(const HEParams &params_in) { return; }

  string profiler_name() const { return this->name; }

  // Tests should take in a set of parameters and return a list of long double,
  // representing runtimes for each function
  // need depth 5 at least for this function
  // implemented using
  // https://gitlab.com/palisade/palisade-release/-/blob/master/src/pke/examples/polynomial_evaluation.cpp

  TestResult test_poly_eval_native(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;

    ret.runtimes.reserve(iters);
    ret.batchsize = context->GetEncodingParams()->GetBatchSize();
    ret.errors.reserve(ret.batchsize * iters);

    double x = 1.0f;
    // depth has to be ceil(log2(degree))
    const vector<double> &coeffs = tparams.polynomial_coeffs;
    size_t degree = coeffs.size() - 1;

#ifdef PROF_DEBUG
    cout << "Degree of Polynomial = " << degree << endl;
    cout << "x = " << x << endl;
#endif

    std::vector<std::complex<double>> input({x});
    size_t encodedLength = input.size();

    std::vector<lbcrypto::Plaintext> plain_coeffs(degree + 1);

    int counter = 0;
    for (size_t i = 0; i < degree + 1; i++) {
      // lbcrypto::Plaintext tmp = this->getCustomMessage(coefficients[i]);
      // plain_coeffs[i] = tmp;
#ifdef PROF_DEBUG
      cout << "x^" << counter << " * (" << coeffs[i] << ")"
           << ", ";
#endif
      counter++;
    }

#ifdef PROF_DEBUG
    cout << "\n Encryption done Native \n" << endl;
#endif

    lbcrypto::Plaintext plaintext = context->MakeCKKSPackedPlaintext(input);
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> ciphertext =
        context->Encrypt(keyPair->publicKey, plaintext);

    for (unsigned int k = 0; k < iters; k++) {

      start = steady_clock::now();
      lbcrypto::Ciphertext<lbcrypto::DCRTPoly> result =
          context->EvalPoly(ciphertext, coeffs);
      end = steady_clock::now();

      lbcrypto::Plaintext plaintextDec;
      context->Decrypt(keyPair->secretKey, result, &plaintextDec);
      plaintextDec->SetLength(encodedLength);
      double realResult = getPlainPolyEval(x, coeffs);

      auto enc_result = plaintextDec->GetRealPackedValue();
#ifdef PROF_DEBUG
      std::cout << std::setprecision(60) << std::endl;

      std::cout << "\n Decrypted Plaintext #1: \n";
      std::cout << enc_result[0] << std::endl;
      std::cout << "Real result : " << realResult << endl;
#endif

      double error = abs(enc_result[0] - realResult) / abs(realResult);

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.push_back(error);
    }

    return ret;
  }

  TestResult test_poly_eval_basic(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;

    // setup results
    ret.runtimes.reserve(iters);
    ret.batchsize = context->GetEncodingParams()->GetBatchSize();
    ret.errors.reserve(ret.batchsize * iters);

    // to be implemented ----->>>>  get x, degree and coeffs vector from the
    // arguments

    double x = 1.0f;
    // depth has to be ceil(log2(degree))
    const vector<double> &coefficients = tparams.polynomial_coeffs;
    size_t degree = coefficients.size() - 1;

#ifdef PROF_DEBUG
    cout << "Degree of Polynomial = " << degree << endl;
    cout << "Evaluated value (x) = " << x << endl;
#endif

    // std::vector<double> input({x});
    std::vector<std::complex<double>> input({x});
    lbcrypto::Plaintext ptx = context->MakeCKKSPackedPlaintext(input);
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> ctx =
        context->Encrypt(keyPair->publicKey, ptx);

    std::vector<lbcrypto::Plaintext> plain_coeffs(degree + 1);

    int counter = 0;
    for (size_t i = 0; i < degree + 1; i++) {
      // coefficients[i] = (double)rand() / RAND_MAX;
      std::vector<std::complex<double>> tmpV({coefficients[i]});
      lbcrypto::Plaintext tmpP = context->MakeCKKSPackedPlaintext(tmpV);
      plain_coeffs[i] = tmpP;
#ifdef PROF_DEBUG
      cout << "x^" << counter << " * (" << coefficients[i] << ")"
           << ", ";
#endif
      counter++;
    }
    lbcrypto::Plaintext plain_result;

    vector<lbcrypto::Ciphertext<lbcrypto::DCRTPoly>> powers(degree + 1);

    for (unsigned int k = 0; k < iters; k++) {
      start = steady_clock::now();

      powers.resize(degree + 1);
      powers[1] = ctx;

      vector<int> levels(degree + 1, 0);
      levels[1] = 0;
      levels[0] = 0;

      for (size_t i = 2; i <= degree; i++) {
        // compute x^i
        int minlevel = i;
        int cand = -1;
        for (size_t j = 1; j <= i / 2; j++) {
          int k = i - j;
          //
          int newlevel = max(levels[j], levels[k]) + 1;
          if (newlevel < minlevel) {
            cand = j;
            minlevel = newlevel;
          }
        }
        levels[i] = minlevel;
        // use cand
        if (cand < 0)
          throw runtime_error("error");
#ifdef PROF_DEBUG
        cout << "levels " << i << " = " << levels[i] << endl;
#endif
        // cand <= i - cand by definition
        lbcrypto::Ciphertext<lbcrypto::DCRTPoly> temp;

        int levelDiff = powers[cand]->GetElements()[0].GetNumOfElements() -
                        powers[i - cand]->GetElements()[0].GetNumOfElements();

        for (int idx = 0; idx < levelDiff; idx++) {
          powers[cand] = context->LevelReduce(powers[cand], nullptr);
        }

        powers[i] = context->EvalMult(powers[cand], powers[i - cand]);
        context->RescaleInPlace(powers[i]);
      }

      lbcrypto::Ciphertext<lbcrypto::DCRTPoly> enc_result =
          context->Encrypt(keyPair->publicKey, plain_coeffs[0]);

      lbcrypto::Ciphertext<lbcrypto::DCRTPoly> temp;

      for (size_t i = 1; i <= degree; i++) {

        lbcrypto::Ciphertext<lbcrypto::DCRTPoly> temp =
            context->EvalMult(powers[i], plain_coeffs[i]);
        context->RescaleInPlace(temp);

        int levelDiff1 = enc_result->GetElements()[0].GetNumOfElements() -
                         temp->GetElements()[0].GetNumOfElements();

        for (int idx = 0; idx < levelDiff1; idx++) {
          enc_result = context->LevelReduce(enc_result, nullptr);
        }

        enc_result = context->EvalAdd(enc_result, temp);
      }
      end = steady_clock::now();

      lbcrypto::Plaintext plaintextDec;
      context->Decrypt(keyPair->secretKey, enc_result, &plaintextDec);

      size_t encodedLength = input.size();
      plaintextDec->SetLength(encodedLength);
      double realResult = getPlainPolyEval(x, coefficients);

      auto result = plaintextDec->GetRealPackedValue();
#ifdef PROF_DEBUG
      std::cout << std::setprecision(25) << std::endl;

      std::cout << "\n Decrypted Plaintext: \n";
      std::cout << result[0] << std::endl;
      std::cout << "Real result : " << realResult << endl;
#endif

      vector<double> errors;
      errors.push_back(abs(result[0] - realResult) / abs(realResult));

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

    return ret;
  }

  TestResult test_poly_eval(const TestParams &tparams) const {
    if (tparams.native_alg) {
      return test_poly_eval_native(tparams);
    } else {
      return test_poly_eval_basic(tparams);
    }
  }

  // ------------------------Native implementation from
  // scratch-------------------------------------------------------- start
  // timing here for (unsigned int i = 0; i < iters; i++) {
  //
  //   start = steady_clock::now();
  //   lbcrypto::Plaintext plaintext1 =
  //   context->MakeCKKSPackedPlaintext(input);
  //
  //   auto x = context->Encrypt(keyPair->publicKey, plaintext1);
  //   // auto result = context->EvalPoly(x, coefficients);
  //
  //   std::vector<lbcrypto::Ciphertext<lbcrypto::DCRTPoly>> powers(
  //       coefficients.size() - 1);
  //   std::vector<int32_t> indices(coefficients.size() - 1, 0);
  //
  //   // set the indices for the powers of x that need to be computed to 1
  //   for (size_t i = coefficients.size() - 1; i > 0; i--) {
  //     if (IsPowerOfTwo(i)) {
  //       indices[i - 1] = 1;
  //     } else { // non-power of 2
  //       if (coefficients[i] != 0) {
  //         indices[i - 1] = 1;
  //         int64_t powerOf2 = 1 << (int64_t)std::floor(std::log2(i));
  //         int64_t rem = i % powerOf2;
  //         if (indices[rem - 1] == 0)
  //           indices[rem - 1] = 1;
  //         // while rem is not a power of 2, set indices required to compute
  //         rem
  //         // to
  //         // 1
  //         while (!IsPowerOfTwo(rem)) {
  //           powerOf2 = 1 << (int64_t)std::floor(std::log2(rem));
  //           rem = rem % powerOf2;
  //           if (indices[rem - 1] == 0)
  //             indices[rem - 1] = 1;
  //         }
  //       }
  //     }
  //   }
  //
  //   powers[0] = lbcrypto::Ciphertext<lbcrypto::DCRTPoly>(
  //       new lbcrypto::CiphertextImpl<lbcrypto::DCRTPoly>(*x));
  //
  //   auto cc = x->GetCryptoContext();
  //
  //   // computes all powers for x
  //   for (size_t i = 2; i < coefficients.size(); i++) {
  //     if (IsPowerOfTwo(i)) {
  //       powers[i - 1] = cc->EvalMult(powers[i / 2 - 1], powers[i / 2 - 1]);
  //       cc->ModReduceInPlace(powers[i - 1]);
  //     } else { // non-power of 2
  //       if (indices[i - 1] == 1) {
  //         int64_t powerOf2 = 1 << (int64_t)std::floor(std::log2(i));
  //         int64_t rem = i % powerOf2;
  //
  //         int levelDiff =
  //             powers[powerOf2 - 1]->GetElements()[0].GetNumOfElements() -
  //             powers[rem - 1]->GetElements()[0].GetNumOfElements();
  //         for (int idx = 0; idx < levelDiff; idx++) {
  //           powers[rem - 1] = cc->LevelReduce(powers[rem - 1], nullptr);
  //         }
  //
  //         powers[i - 1] = cc->EvalMult(powers[powerOf2 - 1], powers[rem -
  //         1]); cc->ModReduceInPlace(powers[i - 1]);
  //       }
  //     }
  //   }
  //
  //   // gets the highest depth (lowest number of CRT limbs)
  //   int64_t limbs =
  //       powers[coefficients.size() -
  //       2]->GetElements()[0].GetNumOfElements();
  //
  //   // brings all powers of x to the same level
  //   for (size_t i = 1; i < coefficients.size() - 1; i++) {
  //     if (indices[i - 1] == 1) {
  //       int levelDiff =
  //           limbs - powers[i - 1]->GetElements()[0].GetNumOfElements();
  //       for (int idx = 0; idx < levelDiff; idx++) {
  //         powers[i - 1] = cc->LevelReduce(powers[i - 1], nullptr);
  //       }
  //     }
  //   }
  //
  //   // perform scalar multiplication for the highest-order term
  //   auto result = cc->EvalMult(powers[coefficients.size() - 2],
  //                              coefficients[coefficients.size() - 1]);
  //
  //   // perform scalar multiplication for all other terms and sum them up
  //   for (size_t i = 0; i < coefficients.size() - 2; i++) {
  //     if (coefficients[i + 1] != 0) {
  //       result =
  //           cc->EvalAdd(result, cc->EvalMult(powers[i], coefficients[i +
  //           1]));
  //     }
  //   }
  //
  //   // Do rescaling after scalar multiplication
  //   result = cc->ModReduce(result);
  //
  //   // adds the free term (at x^0)
  //   if (coefficients[0] != 0) {
  //     if (coefficients[0] < 0)
  //       result = cc->EvalSub(result, std::fabs(coefficients[0]));
  //     else
  //       result = cc->EvalAdd(result, coefficients[0]);
  //   }
  //
  //   lbcrypto::Plaintext plaintextDec;
  //   context->Decrypt(keyPair->secretKey, result, &plaintextDec);
  //   plaintextDec->SetLength(encodedLength);
  //
  //   std::cout << std::setprecision(15) << std::endl;
  //
  //   std::cout << "\n Original Plaintext #1: \n";
  //   std::cout << plaintext1 << std::endl;
  //
  //   std::cout << plaintextDec << std::endl;
  //
  //   end = steady_clock::now();
  //
  //   long double d = duration_cast<measure_typ>(end - start).count();
  //   ret.runtimes.push_back(d);
  // }

  TestResult test_rotdot_basic(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;

    // setup results
    ret.runtimes.reserve(iters);
    ret.batchsize = context->GetEncodingParams()->GetBatchSize();
    ret.errors.reserve(ret.batchsize * iters);

    // Vectors of input
    std::vector<double> inputs;

    inputs.reserve(ret.batchsize);

    for (size_t i = 0; i < ret.batchsize; i++) {
      inputs.push_back(i + 0.001 * i);
    }

    lbcrypto::Plaintext pt_inputs = context->MakeCKKSPackedPlaintext(inputs);
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> ct_inputs =
        context->Encrypt(keyPair->publicKey, pt_inputs);

    // 500D weight vector
    std::vector<double> weights;
    weights.reserve(ret.batchsize);

    for (size_t i = 0; i < ret.batchsize; i++) {
      weights.push_back((ret.batchsize & 1) ? -1.0 : 2.00);
    }

    lbcrypto::Plaintext pt_weights = context->MakeCKKSPackedPlaintext(weights);
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> ct_weights =
        context->Encrypt(keyPair->publicKey, pt_weights);

    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> ct_temp, ct_final;
    size_t slot_count = context->GetEncodingParams()->GetBatchSize();

    vector<int> rot_indices;

    for (size_t i = 1; i <= slot_count / 2; i <<= 1) {
      rot_indices.push_back(i);
    }

    context->EvalAtIndexKeyGen(
        keyPair->secretKey, rot_indices); // not timing this because it is
                                          // calculated earlier in other schemes

    for (unsigned int k = 0; k < iters; k++) {

      lbcrypto::Ciphertext<lbcrypto::DCRTPoly> ct_inputs =
          context->Encrypt(keyPair->publicKey, pt_inputs);

      start = steady_clock::now();

      ct_final = context->EvalMult(ct_inputs, ct_weights);
      context->RescaleInPlace(ct_final);

      // Sum the slots of ciphertext using log(D) rotations
      for (size_t i = 1; i <= slot_count / 2; i <<= 1) {
        ct_temp = context->EvalAtIndex(ct_final, i);
        ct_final = context->EvalAdd(ct_final, ct_temp);
      }

      end = steady_clock::now();

      lbcrypto::Plaintext pt;
      context->Decrypt(keyPair->secretKey, ct_final, &pt);

      vector<double> enc_result;
      enc_result = pt->GetRealPackedValue();

      double realResult = std::inner_product(inputs.begin(), inputs.end(),
                                             weights.begin(), 0.0);

      double error = abs(enc_result[0] - realResult) / abs(realResult);

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.push_back(error);
    }

    lbcrypto::Plaintext pt;
    context->Decrypt(keyPair->secretKey, ct_final, &pt);

    vector<double> enc_result;
    enc_result = pt->GetRealPackedValue();

#ifdef PROF_DEBUG

    std::cout << "Result: " << enc_result[0] << endl;
    cout << "True result: "
         << std::inner_product(inputs.begin(), inputs.end(), weights.begin(),
                               0.0)
         << endl;
#endif

    return ret;
  }

  TestResult test_single_dot_native(const TestParams &tparams) const {
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;

    // setup results
    ret.runtimes.reserve(iters);
    ret.batchsize = context->GetEncodingParams()->GetBatchSize();
    ret.errors.reserve(ret.batchsize * iters);

    // setup
    vector<double> arg0, arg1;
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> c0, c1, res;

    for (unsigned int i = 0; i < iters; i++) {
      // get random input
      c0 = this->rand_ctext(arg0);
      c1 = this->rand_ctext(arg1);

      // run the test
      start = steady_clock::now();
      res = context->EvalInnerProduct(c0, c1, ret.batchsize);
      end = steady_clock::now();

      lbcrypto::Plaintext pt;
      context->Decrypt(keyPair->secretKey, res, &pt);

      vector<double> enc_result;
      enc_result = pt->GetRealPackedValue();

      vector<double> errors(arg0.size());

      double realResult =
          std::inner_product(arg0.begin(), arg0.end(), arg1.begin(), 0.0);

      for (size_t j = 0; j < arg0.size(); j++) {
        errors[j] = abs(realResult - enc_result[j]) / abs(realResult);
      }

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

    return ret;
  }

  TestResult test_rotdot_product_basic(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;

    // setup results
    ret.runtimes.reserve(iters);
    ret.batchsize = context->GetEncodingParams()->GetBatchSize();
    ret.errors.reserve(ret.batchsize * iters);

    // Vectors of input

    std::vector<double> inputs;

    inputs.reserve(ret.batchsize);

    for (size_t i = 0; i < ret.batchsize; i++) {
      inputs.push_back(i + 0.001 * i);
    }

    lbcrypto::Plaintext pt_inputs = context->MakeCKKSPackedPlaintext(inputs);
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> ct_inputs =
        context->Encrypt(keyPair->publicKey, pt_inputs);

    // 500D weight vector
    std::vector<double> weights;
    weights.reserve(ret.batchsize);

    for (size_t i = 0; i < ret.batchsize; i++) {
      weights.push_back((ret.batchsize & 1) ? -1.0 : 2.00);
    }

    lbcrypto::Plaintext pt_weights = context->MakeCKKSPackedPlaintext(weights);
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> ct_weights =
        context->Encrypt(keyPair->publicKey, pt_weights);

    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> ct_temp, ct_final;
    size_t slot_count = context->GetEncodingParams()->GetBatchSize();

    vector<int> rot_indices;

    for (size_t i = 1; i <= slot_count / 2; i <<= 1) {
      rot_indices.push_back(i);
    }

    context->EvalAtIndexKeyGen(
        keyPair->secretKey, rot_indices); // not timing this because it is
                                          // calculated earlier in other schemes

    for (unsigned int i = 0; i < iters; i++) {

      lbcrypto::Ciphertext<lbcrypto::DCRTPoly> ct_inputs =
          context->Encrypt(keyPair->publicKey, pt_inputs);

      start = steady_clock::now();

      ct_final = context->EvalMult(ct_inputs, ct_weights);
      context->RescaleInPlace(ct_final);

      // Sum the slots of ciphertext using log(D) rotations
      for (size_t i = 1; i <= slot_count / 2; i <<= 1) {
        ct_temp = context->EvalAtIndex(ct_final, i);
        ct_final = context->EvalAdd(ct_final, ct_temp);
      }

      end = steady_clock::now();

      lbcrypto::Plaintext pt;
      context->Decrypt(keyPair->secretKey, ct_final, &pt);

      vector<double> enc_result;
      enc_result = pt->GetRealPackedValue();

      double realResult = std::inner_product(inputs.begin(), inputs.end(),
                                             weights.begin(), 0.0);

      double error = abs(enc_result[0] - realResult) / abs(realResult);

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.push_back(error);
    }

    lbcrypto::Plaintext pt;
    context->Decrypt(keyPair->secretKey, ct_final, &pt);

    vector<double> enc_result;
    enc_result = pt->GetRealPackedValue();

#ifdef PROF_DEBUG

    std::cout << "Result: " << enc_result[0] << endl;
    cout << "True result: "
         << std::inner_product(inputs.begin(), inputs.end(), weights.begin(),
                               0.0)
         << endl;
#endif

    return ret;
  }

  TestResult test_rotdot_product(const TestParams &tparams) const {
    if (tparams.native_alg) {
      return test_single_dot_native(tparams);
    } else {
      return test_rotdot_product_basic(tparams);
    }
  }

  TestResult test_linear_transform(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;

    // setup results
    ret.runtimes.reserve(iters);
    ret.batchsize = context->GetEncodingParams()->GetBatchSize();
    ret.errors.reserve(ret.batchsize * iters);

    size_t dimension = tparams.dimension(); // max. 96

    vector<vector<double>> input_matrix = generateUVector(dimension);

    double input_size = input_matrix[0].size();
    vector<vector<double>> diagonal_matrix(input_size,
                                           vector<double>(input_size));

    diagonal_matrix = getDiagonalMatrix(input_matrix);

#ifdef PROF_DEBUG

    cout << "Diagonal matrix" << endl;

    double dsize = diagonal_matrix[0].size();
    for (size_t i = 0; i < dsize; i++) {
      for (size_t j = 0; j < dsize; j++) {
        cout << '(' << diagonal_matrix[i][j] << ")";
      }
      cout << "\n";
    }
#endif

    vector<vector<double>> transpose_matrix(input_size,
                                            vector<double>(input_size));

    for (size_t i = 0; i < input_size; ++i)
      for (size_t j = 0; j < input_size; ++j) {
        transpose_matrix[j][i] = diagonal_matrix[i][j];
      }

#ifdef PROF_DEBUG

    cout << "Diagonal-transpose_matrix" << endl;

    for (size_t i = 0; i < input_size; i++) {
      for (size_t j = 0; j < input_size; j++) {
        cout << '(' << transpose_matrix[i][j] << ")";
      }
      cout << "\n";
    }
#endif

    unsigned int dot_len = input_size;

    vector<lbcrypto::Ciphertext<lbcrypto::DCRTPoly>> diagonalCt_vec;
    diagonalCt_vec.resize(dot_len);

    vector<vector<double>> args2(diagonalCt_vec.size());
    for (size_t i = 0; i < diagonalCt_vec.size(); i++) {

      diagonalCt_vec[i] =
          this->custom_ctext(args2[i], transpose_matrix[i % dimension]);
    }

    std::vector<double> input_vector;

    input_vector.reserve(dimension);

    for (size_t i = 0; i < dimension; i++) {
      input_vector.push_back(i + 0.001 * i);
    }

    // vector<double> input_vector{0, 1, 2, 3, 4};
    vector<lbcrypto::Ciphertext<lbcrypto::DCRTPoly>> inputCt_vec;
    inputCt_vec.resize(dot_len);
    vector<vector<double>> args3(inputCt_vec.size());

    for (size_t i = 0; i < inputCt_vec.size(); i++) {

      inputCt_vec[i] = this->custom_ctext(args3[i], input_vector);
    }

    // initializing plaintext with 0s for ct_prime
    vector<double> args;
    args.resize(diagonalCt_vec.size());

    for (size_t i = 0; i < args.size(); i++) {
      args[i] = 0.0;
    }
    lbcrypto::Plaintext p = context->MakeCKKSPackedPlaintext(args);
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> ct_prime;

    vector<int> rot_indices(diagonalCt_vec.size() - 1);
    // Calculate rotation keys, if not already known
    for (size_t i = 1; i < diagonalCt_vec.size(); i++) {
      rot_indices[i - 1] = i;
    }

    context->EvalAtIndexKeyGen(
        keyPair->secretKey, rot_indices); // not timing this because it is
                                          // calculated earlier in other schemes

    for (unsigned int k = 0; k < iters; k++) {

      ct_prime = context->Encrypt(keyPair->publicKey, p);
      start = steady_clock::now();
      // doing the actual multplication and addition of diagonals and input
      // vector
      vector<lbcrypto::Ciphertext<lbcrypto::DCRTPoly>> ct_result(
          diagonalCt_vec.size());
      ct_result[0] = context->EvalMult(inputCt_vec[0], diagonalCt_vec[0]);

      lbcrypto::Ciphertext<lbcrypto::DCRTPoly> temp_rot;

      // takes n rotations, multiplications, and additions, and has depth of one
      // multiplication, one rotation, and n additions
      for (size_t j = 1; j < diagonalCt_vec.size(); j++) {
        temp_rot = context->EvalAtIndex(inputCt_vec[j], j);
        ct_result[j] = context->EvalMult(temp_rot, diagonalCt_vec[j]);
        // Manual rescaling
        context->RescaleInPlace(ct_result[j]);
      }

      for (size_t j = 0; j < ct_result.size(); j++) {
        ct_prime = context->EvalAdd(ct_prime, ct_result[j]);
      }

      end = steady_clock::now();

      lbcrypto::Plaintext pt;
      context->Decrypt(keyPair->secretKey, ct_prime, &pt);

      vector<double> enc_result;
      enc_result = pt->GetRealPackedValue();

      std::vector<double> realResult =
          getLinearTransformVector(input_matrix, input_vector);

      vector<double> errors(dimension);
      for (size_t j = 0; j < dimension; j++) {
        errors[j] = abs(realResult[j] - enc_result[j]) / abs(realResult[j]);
      }

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

    // std::cout << "Result: " << enc_result[0] << endl;

#ifdef PROF_DEBUG

    lbcrypto::Plaintext pt;
    context->Decrypt(keyPair->secretKey, ct_prime, &pt);

    vector<double> enc_result;
    enc_result = pt->GetRealPackedValue();

    for (size_t i = 0; i < input_size; i++) {

      std::cout << "Result: " << enc_result[i] << endl;
    }
    std::cout << "Slot count:: " << ret.batchsize << endl;
    std::cout << "Dimension:: " << dimension << endl;
#endif

    return ret;
  }

  // FHE Primitivies
  TestResult test_add(const TestParams &tparams) const {
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;

    // setup results
    ret.runtimes.reserve(iters);
    ret.batchsize = context->GetEncodingParams()->GetBatchSize();
    ret.errors.reserve(ret.batchsize * iters);

    // setup
    vector<double> arg0, arg1;
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> c0, c1, c2;

    for (unsigned int i = 0; i < iters; i++) {
      // get random input
      c0 = this->rand_ctext(arg0);
      c1 = this->rand_ctext(arg1);

      // run the test
      start = steady_clock::now();
      c2 = context->EvalAdd(c0, c1);
      end = steady_clock::now();

      lbcrypto::Plaintext pt;
      context->Decrypt(keyPair->secretKey, c2, &pt);

      vector<double> enc_result;
      enc_result = pt->GetRealPackedValue();

      vector<double> errors(arg0.size());

      for (size_t j = 0; j < arg0.size(); j++) {
        double realResult = arg0[j] + arg1[j];
        errors[j] = abs(realResult - enc_result[j]) / abs(realResult);
      }

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

    return ret;
  }

  TestResult test_plain_add(const TestParams &tparams) const {
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;

    ret.runtimes.reserve(iters);
    ret.batchsize = context->GetEncodingParams()->GetBatchSize();
    ret.errors.reserve(ret.batchsize * iters);

    vector<double> arg0;
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> c0, c2;
    for (unsigned int i = 0; i < iters; i++) {
      // get random input
      c0 = this->rand_ctext(arg0);
      lbcrypto::Plaintext p = context->MakeCKKSPackedPlaintext(arg0);

      // run the test
      start = steady_clock::now();
      c2 = context->EvalAdd(c0, p);
      end = steady_clock::now();

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
    }

    return ret;
  }

  TestResult test_plain_mult(const TestParams &tparams) const {
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;

    ret.runtimes.reserve(iters);
    ret.batchsize = context->GetEncodingParams()->GetBatchSize();
    ret.errors.reserve(ret.batchsize * iters);

    vector<double> arg0;
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> c0, c2;
    for (unsigned int i = 0; i < iters; i++) {
      // get random input
      c0 = this->rand_ctext(arg0);
      lbcrypto::Plaintext p = context->MakeCKKSPackedPlaintext(arg0);

      // run the test
      start = steady_clock::now();
      c2 = context->EvalMult(c0, p);
      end = steady_clock::now();

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
    }

    return ret;
  }

  TestResult test_chain(const TestParams &tparams) const {
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;

    ret.runtimes.reserve(iters);
    unsigned int slot_count = context->GetEncodingParams()->GetBatchSize();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // setup
    vector<double> arg0;
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> c0;

    unsigned int depth = this->_params.depth() - 1;

    for (unsigned int i = 0; i < iters; i++) {
      c0 = this->rand_ctext(arg0);

      start = steady_clock::now();
      for (unsigned int j = 0; j < depth; j++) {
        c0 = context->EvalMult(c0, c0);
      }
      end = steady_clock::now();

      for (unsigned int j = 0; j < depth; j++) {
        for (double &d : arg0) {
          d *= d;
        }
      }

      lbcrypto::Plaintext pt;
      context->Decrypt(keyPair->secretKey, c0, &pt);

      vector<double> enc_result;
      enc_result = pt->GetRealPackedValue();

      vector<double> errors(arg0.size());

      for (size_t j = 0; j < arg0.size(); j++) {
        double realResult = arg0[j] * arg0[j];
        errors[j] = abs(realResult - enc_result[j]) / abs(realResult);
      }

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }
    return ret;
  }

  TestResult test_multiply(const TestParams &tparams) const {
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;

    ret.runtimes.reserve(iters);
    unsigned int slot_count = context->GetEncodingParams()->GetBatchSize();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // setup
    vector<double> arg0, arg1;
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> c0, c1, c2;

    for (unsigned int i = 0; i < iters; i++) {
      c0 = this->rand_ctext(arg0);
      c1 = this->rand_ctext(arg1);

      start = steady_clock::now();
      c2 = context->EvalMult(c0, c1);
      context->RescaleInPlace(c2);
      end = steady_clock::now();

      lbcrypto::Plaintext pt;
      context->Decrypt(keyPair->secretKey, c2, &pt);

      vector<double> enc_result;
      enc_result = pt->GetRealPackedValue();

      vector<double> errors(arg0.size());

      for (size_t j = 0; j < arg0.size(); j++) {
        double realResult = arg0[j] * arg1[j];
        errors[j] = abs(realResult - enc_result[j]) / abs(realResult);
      }

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }
    return ret;
  }

  TestResult test_multiply_no_relin(const TestParams &tparams) const {
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;

    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = context->GetEncodingParams()->GetBatchSize();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // setup
    vector<double> arg0, arg1;
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> c0, c1, c2;

    // Run and time tests
    for (unsigned int i = 0; i < iters; i++) {

      c0 = this->rand_ctext(arg0);
      c1 = this->rand_ctext(arg1);

      start = steady_clock::now();
      c2 = context->EvalMultNoRelin(c0, c1);
      end = steady_clock::now();

      lbcrypto::Plaintext pt;
      context->Decrypt(keyPair->secretKey, c2, &pt);

      vector<double> enc_result;
      enc_result = pt->GetRealPackedValue();

      vector<double> errors(arg0.size());
      for (size_t j = 0; j < arg0.size(); j++) {
        double realResult = arg0[j] * arg1[j];
        errors[j] = abs(realResult - enc_result[j]) / abs(realResult);
      }

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }
    return ret;
  }

  TestResult test_rotate(const TestParams &tparams) const {
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = context->GetEncodingParams()->GetBatchSize();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // setup
    vector<double> args;
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> c, dest;

    const size_t rotate_amount = 1;

    context->EvalAtIndexKeyGen(keyPair->secretKey, {1});

    for (unsigned int i = 0; i < iters; i++) {

      c = rand_ctext(args);

      start = steady_clock::now();
      dest = context->EvalAtIndex(c, rotate_amount);
      end = steady_clock::now();

      lbcrypto::Plaintext pt;
      context->Decrypt(keyPair->secretKey, dest, &pt);

      vector<double> hom_rotated;
      hom_rotated = pt->GetRealPackedValue();

      vector<double> errors(args.size());
      assert(args.size() == slot_count);
      assert(hom_rotated.size() == slot_count);

      for (size_t j = 0; j < errors.size(); j++) {
        errors[j] =
            abs(hom_rotated[j] - args[(j + rotate_amount) % slot_count]) /
            args[(j + rotate_amount) % slot_count];
      }

      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
    }

    return ret;
  }
  TestResult test_relin(const TestParams &tparams)
      const { // Probably should have a block like this
              // at the start of every runtime test
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;

    ret.runtimes.reserve(iters);
    unsigned int slot_count = context->GetEncodingParams()->GetBatchSize();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // setup
    vector<double> args;
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> c0 = this->rand_ctext(args),
                                             c1 = this->rand_ctext(args), c2,
                                             c3;
    c2 = context->EvalMultNoRelin(c0, c1);

    // Run and time tests
    for (unsigned int i = 0; i < iters; i++) {

      start = steady_clock::now();
      c3 = context->Relinearize(c2);
      end = steady_clock::now();

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
    }

    return ret;
  }

  TestResult test_rescale(const TestParams &tparams) const {
    // Probably should have a block like this at the start of every runtime test
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;

    ret.runtimes.reserve(iters);
    unsigned int slot_count = context->GetEncodingParams()->GetBatchSize();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // Setup
    vector<double> args;
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> c0 = this->rand_ctext(args),
                                             c1 = this->rand_ctext(args);
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> c2, cmult;

    cmult = context->EvalMultNoRelin(c0, c1);
    c2 = context->Relinearize(cmult);

    // Run and time tests
    for (unsigned int i = 0; i < iters; i++) {

      start = steady_clock::now();
      context->RescaleInPlace(c2);
      end = steady_clock::now();

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
    }

    return ret;
  }

  TestResult test_encode_error(const TestParams &tparams) const {

    unsigned int iters = tparams.iterations();
    TestResult ret;

    // setup results
    ret.batchsize = context->GetEncodingParams()->GetBatchSize();
    ret.errors.reserve(ret.batchsize * iters);

    // setup
    vector<double> arg0;
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> c0;

    for (unsigned int i = 0; i < iters; i++) {
      // get random input
      c0 = this->rand_ctext(arg0);

      lbcrypto::Plaintext pt;
      context->Decrypt(keyPair->secretKey, c0, &pt);

      vector<double> enc_result;
      enc_result = pt->GetRealPackedValue();

      vector<double> errors(arg0.size());

      for (size_t j = 0; j < arg0.size(); j++) {
        errors[j] = abs(arg0[j] - enc_result[j]) / abs(arg0[j]);
      }

      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

    return ret;
  }

  static uint32_t getDegreeFromBSLevel(const unsigned int lev) {
    if (lev > 0 && lev <= 3) {
      return 1 << (14 + lev);
    }
    throw std::runtime_error("Bad BS level");
    return 0;
  }

  TestResult test_bootstrap(const TestParams &tparams) const {
    // Probably should have a block like this at the start of every runtime test
    TestResult ret;

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();

    uint32_t n = getDegreeFromBSLevel(this->_params.bootstrap());
    if (n < (1 << 16)) {
      throw std::runtime_error("Need n >= 2^16");
    }
    uint32_t slots = n / 2;
    ret.batchsize = slots;
    uint32_t levelsRemaining = this->_params.depth() - 1;

    // Code examples shamelessly stolen from the PALISADE CKKS Bootstrapping
    // example
    std::vector<uint32_t> dim1 = {0, 0};
    std::vector<uint32_t> levelBudget = {4, 4};
#if NATIVEINT == 128
    lbcrypto::RescalingTechnique rescaleTech = lbcrypto::APPROXRESCALE;
#else
    lbcrypto::RescalingTechnique rescaleTech = lbcrypto::EXACTRESCALE;
#endif
    // computes how many levels are needed for
    uint32_t approxModDepth = 9;
    uint32_t r = 6;
    MODE mode = OPTIMIZED;
    if (mode == OPTIMIZED) {
      if (rescaleTech == lbcrypto::APPROXRESCALE) {
        approxModDepth += r - 1;
      } else {
        approxModDepth += r;
      }
    }

    usint depth =
        levelsRemaining + approxModDepth + levelBudget[0] + levelBudget[1];
#if NATIVEINT == 128
    usint dcrtBits = 78;
#else
    usint dcrtBits = 59;
#endif
    lbcrypto::DltCryptoContext<lbcrypto::DCRTPoly> boot_cc =
        lbcrypto::DltCryptoContextFactory<lbcrypto::DCRTPoly>::
            genCryptoContextCKKS(depth, dcrtBits, slots,
                                 lbcrypto::HEStd_128_classic, n, rescaleTech,
                                 lbcrypto::HYBRID, 3, 2,
#if NATIVEINT == 128
                                 89,
#else
                                 60,
#endif
                                 0, mode);

    // Turn on features
    boot_cc->Enable(ENCRYPTION);
    boot_cc->Enable(SHE);
    boot_cc->Enable(LEVELEDSHE);
    boot_cc->Enable(ADVANCEDSHE);
    boot_cc->Enable(FHE);

    const shared_ptr<lbcrypto::LPCryptoParametersCKKS<lbcrypto::DCRTPoly>>
        cryptoParams = std::static_pointer_cast<
            lbcrypto::LPCryptoParametersCKKS<lbcrypto::DCRTPoly>>(
            boot_cc->GetCryptoParameters());
    boot_cc->EvalBTSetup(levelBudget, dim1, slots);
    auto boot_keys = boot_cc->KeyGen();
    int32_t sparseFlag = (slots < n / 2) ? 1 : 0;
    boot_cc->EvalBTKeyGen(boot_keys.secretKey, sparseFlag);
    boot_cc->EvalMultKeyGen(boot_keys.secretKey);

    // Get a random vector
    default_random_engine gen{random_device()()};
    std::vector<std::complex<double>> input(slots);
    uniform_real_distribution<long double> dist(-1.0L, 1.0L);
    for (size_t i = 0; i < input.size(); i++) {
      input[i] = dist(gen);
    }

    size_t encodedLength = input.size();
    lbcrypto::Plaintext plaintext1 =
        boot_cc->MakeCKKSPackedPlaintext(input, 1, depth - 1);

    auto ciphertext1 = boot_cc->Encrypt(boot_keys.publicKey, plaintext1);

    for (unsigned int i = 0; i < iters; i++) {
      start = steady_clock::now();
      auto ciphertextAfter = boot_cc->EvalBT(ciphertext1);
      end = steady_clock::now();

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);

      lbcrypto::Plaintext result;

      boot_cc->Decrypt(boot_keys.secretKey, ciphertextAfter, &result);

      result->SetLength(encodedLength);
      plaintext1->SetLength(encodedLength);

      for (size_t i = 0; i < encodedLength; i++) {
        ret.errors.push_back(
            +std::fabs((result->GetCKKSPackedValue()[i].real() -
                        plaintext1->GetCKKSPackedValue()[i].real()) /
                       plaintext1->GetCKKSPackedValue()[i].real()));
      }
    }

    return ret;
  }

  // End-to-end functions
  lbcrypto::Ciphertext<lbcrypto::DCRTPoly>
  single_dot(const lbcrypto::Ciphertext<lbcrypto::DCRTPoly> &ct0,
             const lbcrypto::Ciphertext<lbcrypto::DCRTPoly> &ct1) const {
    unsigned int slot_count = context->GetEncodingParams()->GetBatchSize();
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> ct_temp;
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> ct_final =
        context->EvalMult(ct0, ct1);
    context->RescaleInPlace(ct_final);
    for (size_t i = 1; i <= slot_count / 2; i <<= 1) {
      ct_temp = context->EvalAtIndex(ct_final, i);
      ct_final = context->EvalAdd(ct_final, ct_temp);
    }
    return ct_final;
  }

  TestResult test_dot(const TestParams &tparams) const {
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;

    ret.runtimes.reserve(iters);

    unsigned int slot_count = context->GetEncodingParams()->GetBatchSize();
    unsigned int dot_len =
        tparams.dot_len / slot_count + (tparams.dot_len % slot_count ? 1 : 0);
    vector<lbcrypto::Ciphertext<lbcrypto::DCRTPoly>> v1, v2;
    v1.resize(dot_len);
    v2.resize(dot_len);

    vector<int> rot_indices;

    // Calculate rotation keys, if not already known
    for (size_t i = 1; i <= slot_count / 2; i <<= 1) {
      rot_indices.push_back(i);
    }

    context->EvalAtIndexKeyGen(
        keyPair->secretKey, rot_indices); // not timing this because it is
                                          // calculated earlier in other schemes

    for (unsigned int j = 0; j < iters; j++) {
      lbcrypto::Ciphertext<lbcrypto::DCRTPoly> dot_result, tmp;

      vector<vector<double>> args1(v1.size());
      vector<vector<double>> args2(v2.size());

      for (size_t i = 0; i < v1.size(); i++) {
        v1[i] = this->rand_ctext(args1[i]);
        v2[i] = this->rand_ctext(args2[i]);
      }

      start = steady_clock::now();
      for (size_t i = 0; i < v1.size(); i++) {
        if (!i) {
          dot_result = single_dot(v1[i], v2[i]);
        } else {
          tmp = single_dot(v1[i], v2[i]);
          context->EvalAddInPlace(dot_result, tmp);
        }
      }

      // No need to relinearize or rescale - EvalMult handles it
      end = steady_clock::now();

      lbcrypto::Plaintext pt;
      context->Decrypt(keyPair->secretKey, dot_result, &pt);
      vector<double> enc_result;
      enc_result = pt->GetRealPackedValue();

      // Plain result for errors
      double realResult = 0.0f;
      for (size_t i = 0; i < args1.size(); i++) {
        realResult += plain_dot(args1[i], args2[i]);
      }

      ret.errors.push_back(abs(realResult - enc_result[0]) / abs(realResult));

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
    }

    return ret;
  }

  // Sizes
  // Returns only a single element in their vector
  // Technically there's a precision loss with floating-point, but the results
  // shouldn't be nearly large enough for issues
  TestResult ciphertext_size(const TestParams &tparams) const {
    stringstream ss;
    vector<double> tmp;

    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> c = this->rand_ctext(tmp);
    lbcrypto::Serial::Serialize(c, ss, lbcrypto::SerType::BINARY);

    // might be a better way to do this
    TestResult t;
    t.runtimes = {(long double)ss.str().size()};
    return t;
  }

  TestResult secret_key_size(const TestParams &tparams) const {

    stringstream ss;
    lbcrypto::Serial::Serialize(keyPair->secretKey, ss,
                                lbcrypto::SerType::BINARY);
    TestResult t;

    t.runtimes = {(long double)ss.str().size()};
    return t;
  }

  TestResult public_key_size(const TestParams &tparams) const {

    stringstream ss;
    lbcrypto::Serial::Serialize(keyPair->publicKey, ss,
                                lbcrypto::SerType::BINARY);
    TestResult t;

    t.runtimes = {(long double)ss.str().size()};
    return t;
  }

  TestResult bsk_size(const TestParams &tparams) const {
    TestResult t;
    return t;
    // TODO
  }

  TestResult params_size(const TestParams &tparams) const {
    stringstream ss;
    lbcrypto::Serial::Serialize(context, ss, lbcrypto::SerType::BINARY);
    TestResult t;
    t.runtimes = {(long double)ss.str().size()};
    return t;
  }

  TestResult ctext_modulus_bits() const {
    TestResult t;
    auto pa = context->GetCryptoParameters();
    auto qp = pa->GetElementParams();
    t.runtimes = {(long double)qp->GetModulus().GetMSB()};

    /*
    const auto cryptoParamsCKKS =
      std::static_pointer_cast<lbcrypto::LPCryptoParametersCKKS<lbcrypto::DCRTPoly>>(
          context->GetCryptoParameters());
    //Was originally GetExtendedElementParams - is this still correct?
    auto paramsQP = cryptoParamsCKKS->GetElementParams();
    auto paramsQ = context->GetElementParams()->GetParams();
    auto QBitLength = context->GetModulus().GetLengthForBase(2);
    lbcrypto::BigInteger P = lbcrypto::BigInteger(1);
    for (uint32_t i = 0; i < paramsQP->GetParams().size(); i++) {
      if (i > paramsQ.size()) {
        P = P * lbcrypto::BigInteger(paramsQP->GetParams()[i]->GetModulus());
      }
    }

    auto PBitLength = P.GetLengthForBase(2);
    std::stringstream ss;
    ss << QBitLength + PBitLength;
    unsigned int x;
    ss >> x;
    t.runtimes = {(long double) x};
    */

    return t;
  }

  TestResult scaling_factor() const {
    TestResult t;
    vector<double> arg0;
    lbcrypto::Ciphertext<lbcrypto::DCRTPoly> c0;
    c0 = this->rand_ctext(arg0);
    const auto scale_bits = log2(c0->GetScalingFactor());
    t.runtimes = {(long double)scale_bits};
    return t;
  }

  TestResult num_columns() const {
    TestResult t;
    unsigned int cols = 0; // TODO

    t.runtimes = {(long double)cols};
    return t;
  }
};

#endif
