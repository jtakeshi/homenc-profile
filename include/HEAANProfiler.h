// $ g++ src/main.cpp -std=c++17 -Wall -Werror /usr/local/lib/libseal-3.6.a -I
// /usr/local/include/SEAL-3.6/ -pthread -Wl,-rpath=/usr/local/lib/ -l HEaaN -I
// /usr/local/include/HEaaN/
// ./a.out testName -l HEAAN -i $#ofIterations
// g++ keygen.cpp -std=c++17 -Wall -Werror -I /usr/local/include/SEAL-3.6/
// -pthread -Wl,-rpath=/usr/local/lib/ -l HEaaN -I /usr/local/include/HEaaN/
#ifndef HEAANPROFILER_H
#define HEAANPROFILER_H

#include <time.h>

#include <algorithm>
#include <chrono>
#include <cstdint>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <random>
#include <sstream>

#include "linearAlgebra.h"
#include <heaan.h>
#include <seal/seal.h>

using namespace std;
using namespace std::chrono;
using HEaaN::ParameterPreset;
using HEaaN::u64;
using std::unique_ptr;

string keyDirPath;

// const ParameterPreset preset =
// ParameterPreset::CUSTOM; // use custom for rotdot to get correct results, FHE
// presets poly. eval

// default_random_engine gen{random_device()()};

void fillRandomReal(HEaaN::Message &msg) {

  // uniform_real_distribution<long double> dist(-1.0L, 1.0L);

  for (u64 i = 0; i < msg.getSize(); i++) {
    // msg[i] = dist(gen);
    msg[i] = rand_plain_val();
  }
}

void fillCustomReal(HEaaN::Message &msg, const double value) {

  for (u64 i = 0; i < msg.getSize(); i++) {
    msg[i] = value;
  }
}

// https://stackoverflow.com/questions/15495756/how-can-i-find-the-size-of-all-files-located-inside-a-folder
long long int getFolderSize(string path) {
  // command to be executed
  std::string cmd("du -sb ");
  cmd.append(path);
  cmd.append(" | cut -f1 2>&1");

  // execute above command and get the output
  FILE *stream = popen(cmd.c_str(), "r");
  if (stream) {
    const int max_size = 256;
    char readbuf[max_size];
    if (fgets(readbuf, max_size, stream) != NULL) {
      return atoll(readbuf);
    }
    pclose(stream);
  }
  // return error val
  return -1;
}

inline std::complex<double> plain_dot(const HEaaN::Message &v1,
                                      const HEaaN::Message &v2) {
  size_t len = std::min(v1.size(), v2.size());
  std::complex<double> ret = 0.0;
  for (size_t i = 0; i < len; i++) {
    ret += v1[i] * v2[i];
  }
  return ret;
}

class HEAANProfiler : public Profiler {

private:
  std::unique_ptr<HEaaN::Context> context;
  std::unique_ptr<HEaaN::Encryptor> enc;
  std::unique_ptr<HEaaN::Decryptor> dec;

  std::unique_ptr<HEaaN::SecretKey> sk;
  std::unique_ptr<HEaaN::KeyPack> pack;
  std::unique_ptr<HEaaN::HomEvaluator> eval;
  std::unique_ptr<HEaaN::KeyGenerator> keygen;
  std::unique_ptr<HEaaN::EnDecoder> encoder;

  static inline HEaaN::ParameterPreset
  FHEParamsFromDegree(const unsigned int log_poly_mod_deg) {
    switch (log_poly_mod_deg) {
    case 17: {
      return HEaaN::ParameterPreset::FVa;
    }
    case 16: {
      return HEaaN::ParameterPreset::FGa;
    }
    case 15: {
      return HEaaN::ParameterPreset::FTa;
    }
    default: {
      string errstr = "Bad poly. degree: ";
      errstr += std::to_string(log_poly_mod_deg);
      throw std::runtime_error(errstr);
    }
    }
    // Should not occur, but prevents the compiler complaining about control
    // flow
    return HEaaN::ParameterPreset::FTa;
  }

  static inline HEaaN::ParameterPreset
  FHEParamsFromBSLevel(const unsigned int bs_level) {
    if (bs_level < 1 || bs_level > 3) {
      string errstr = "Bad boostrapping level: ";
      errstr += std::to_string(bs_level);
      throw std::runtime_error(errstr);
    }
    return FHEParamsFromDegree(14 + bs_level);
  }

  static inline HEaaN::ParameterPreset
  SHEParamsFromDepth(const unsigned int depth) {
    if (depth <= 3) {
      return HEaaN::ParameterPreset::SD3;
    } else if (depth <= 7) {
      return HEaaN::ParameterPreset::ST7;
    } else if (depth == 8) {
      return HEaaN::ParameterPreset::ST8;
    } else if (depth <= 11) {
      return HEaaN::ParameterPreset::ST11;
    } else if (depth <= 14) {
      return HEaaN::ParameterPreset::ST14;
    } else if (depth <= 19) {
      return HEaaN::ParameterPreset::ST19;
    }

    if (depth > 19) {
      throw runtime_error("Depth exceeded maximum size");
    }

    throw runtime_error("Error");
  }

  static inline HEaaN::ParameterPreset
  SelectParams(const unsigned int depth, const unsigned int bs_level) {
    if (!bs_level) {
      return SHEParamsFromDepth(depth);
    }
    return FHEParamsFromBSLevel(bs_level);
  }

  HEaaN::Ciphertext custom_ctext(vector<double> &args,
                                 vector<double> &row) const {

    const u64 logSlots = HEaaN::getLogFullSlots(*context);
    unsigned int slot_count = 1 << logSlots;
    args.clear();
    args.resize(slot_count);

    int row_size = row.size();

    for (size_t i = 0; i < args.size(); i++) {
      args[i] = row[i % row_size];
    }

    HEaaN::Message m1(logSlots);
    for (u64 i = 0; i < m1.getSize(); i++) {
      m1[i] = args[i];
    }

    // HEaaN::Plaintext p1(*context);
    // p1 = encoder->encode(m1);

    HEaaN::Ciphertext ct(*context);
    enc->encrypt(m1, *pack, ct);

    return ct;
  }

  HEaaN::Message getCustomMessage(const double value) const {
    const u64 logSlots = HEaaN::getLogFullSlots(*context);
    HEaaN::Message m1(logSlots);
    for (u64 i = 0; i < m1.getSize(); i++) {
      m1[i] = value;
    }
    return m1;
  }

public:
  const static unsigned int GADGET_RANK_DEFAULT = 2;

  HEAANProfiler(const HEParams &params_in) : Profiler(params_in) {

    unsigned int desired_depth = params_in.depth();
    unsigned int b_level = params_in.bootstrap();

    HEaaN::Context *tmp = new HEaaN::Context;
    if (desired_depth == 1 && !b_level) {
      // Special parameters suggested by Cryptolabs
      *tmp = HEaaN::makeContext(13, 2, 60, 40, 60, 2);
    } else {
      HEaaN::ParameterPreset preset = SelectParams(desired_depth, b_level);
      *tmp = HEaaN::makeContext(preset);
    }
    context = std::unique_ptr<HEaaN::Context>(tmp);
    if (b_level) {
      HEaaN::makeBootstrappable(*context);
    }

    // Total bits in the ciphertext modulus
    //     unsigned int ctext_modulus_bits =
    //         (2 * params_in.edge_modulus_bits()) +
    //         (desired_depth - 1) *
    //             params_in
    //                 .scale_bits(); // 2*edgebitsSize + (depth -1)*qpsize =
    //                 (number
    //                                // of depth + 1)
    //                                // Get an appropriate polynomial modulus
    //                                degree
    //     unsigned int log_poly_mod_deg = 10;
    //
    //     while (seal::CoeffModulus::MaxBitCount(
    //                1 << log_poly_mod_deg,
    //                static_cast<seal::sec_level_type>(params_in.security())) <
    //            (int)ctext_modulus_bits) {
    //       log_poly_mod_deg++;
    //     }
    //
    // #ifdef PROF_DEBUG
    //     std::cout << "log_poly_mod_deg: " << log_poly_mod_deg << endl;
    //     std::cout << "1 << log_poly_mod_deg: " << (1 << log_poly_mod_deg) <<
    //     endl;
    // #endif
    //
    //     HEaaN::Context *tmp = new HEaaN::Context;
    //     if (preset != ParameterPreset::CUSTOM) {
    //       *tmp = HEaaN::makeContext(preset);
    //       context = std::unique_ptr<HEaaN::Context>(tmp);
    //     } else {
    //       *tmp = HEaaN::makeContext(
    //           log_poly_mod_deg, desired_depth + 1,
    //           params_in.edge_modulus_bits(), params_in.scale_bits(),
    //           params_in.edge_modulus_bits(), GADGET_RANK_DEFAULT);
    //       context = std::unique_ptr<HEaaN::Context>(tmp);
    //     }
    //
    //     if (desired_depth >= 3) {
    //       HEaaN::makeBootstrappable(*context);
    //     }

    keyDirPath = "./HEAAN_keys/keys-custom";

    sk = std::unique_ptr<HEaaN::SecretKey>(new HEaaN::SecretKey(*context));
    keygen = std::unique_ptr<HEaaN::KeyGenerator>(
        new HEaaN::KeyGenerator(*context, *sk));
    keygen->genCommonKeys();
    // genCommonKeys calls all of these:
    // keygen->genEncryptionKey();
    // keygen->genMultiplicationKey();
    // keygen->genConjugationKey();
    // keygen->genRotationKeyBundle();

    pack = std::unique_ptr<HEaaN::KeyPack>(
        new HEaaN::KeyPack(keygen->getKeyPack()));
    enc = std::unique_ptr<HEaaN::Encryptor>(new HEaaN::Encryptor(*context));
    dec = std::unique_ptr<HEaaN::Decryptor>(new HEaaN::Decryptor(*context));
    encoder = std::unique_ptr<HEaaN::EnDecoder>(new HEaaN::EnDecoder(*context));
    eval = std::unique_ptr<HEaaN::HomEvaluator>(
        new HEaaN::HomEvaluator(*context, *pack));
  }

  void reset_params(const HEParams &params_in) { return; }

  // have to use CUSTOM presets for dimension > 15
  TestResult test_linear_transform(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    const u64 logSlots = HEaaN::getLogFullSlots(*context);

    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = 1 << logSlots;
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    size_t dimension = tparams.dimension(); // max 96

    vector<vector<double>> input_matrix = generateUVector(dimension);

    double input_size = input_matrix[0].size();
    vector<vector<double>> diagonal_matrix(input_size,
                                           vector<double>(input_size));

    diagonal_matrix = getDiagonalMatrix(input_matrix);

#ifdef PROF_DEBUG
    cout << "Diagonal matrix" << endl;
    double dsize = diagonal_matrix[0].size();
    for (size_t i = 0; i < dsize; i++) {
      for (size_t j = 0; j < dsize; j++) {
        cout << '(' << diagonal_matrix[i][j] << ")";
      }
      cout << "\n";
    }
#endif

    vector<vector<double>> transpose_matrix(input_size,
                                            vector<double>(input_size));

    for (size_t i = 0; i < input_size; ++i)
      for (size_t j = 0; j < input_size; ++j) {
        transpose_matrix[j][i] = diagonal_matrix[i][j];
      }

#ifdef PROF_DEBUG
    cout << "Diagonal-transpose_matrix" << endl;

    for (size_t i = 0; i < input_size; i++) {
      for (size_t j = 0; j < input_size; j++) {
        cout << '(' << transpose_matrix[i][j] << ")";
      }
      cout << "\n";
    }
#endif

    unsigned int dot_len = input_size;

    std::unique_ptr<vector<HEaaN::Ciphertext>> diagonalCt_vec = nullptr;
    vector<vector<double>> args(dot_len);

    for (unsigned int i = 0; i < dot_len; i++) {
      HEaaN::Ciphertext tmp =
          this->custom_ctext(args[i], transpose_matrix[i % dimension]);
      if (diagonalCt_vec == nullptr) {
        diagonalCt_vec = std::unique_ptr<vector<HEaaN::Ciphertext>>(
            new vector<HEaaN::Ciphertext>(
                1, tmp)); // Vector constructor - 1st arg is size, 2nd argument
                          // is value
        diagonalCt_vec->reserve(dot_len);
      } else {
        diagonalCt_vec->emplace_back(tmp);
      }
    }

    std::vector<double> input_vector;
    input_vector.reserve(dimension);

    for (size_t i = 0; i < dimension; i++) {
      input_vector.push_back(i + 0.001 * i);
    }

    // vector<double> input_vector{0, 1, 2, 3, 4};
    std::unique_ptr<vector<HEaaN::Ciphertext>> inputCt_vec = nullptr;
    vector<vector<double>> args3(dot_len);

    for (unsigned int i = 0; i < dot_len; i++) {
      HEaaN::Ciphertext tmp = this->custom_ctext(args3[i], input_vector);
      if (inputCt_vec == nullptr) {
        inputCt_vec = std::unique_ptr<vector<HEaaN::Ciphertext>>(
            new vector<HEaaN::Ciphertext>(
                1, tmp)); // Vector constructor - 1st arg is size, 2nd argument
                          // is value
        inputCt_vec->reserve(dot_len);
      } else {
        inputCt_vec->emplace_back(tmp);
      }
    }

    HEaaN::Message msg(logSlots);
    fillCustomReal(msg, 0);
    HEaaN::Plaintext pt(*context);
    pt = encoder->encode(msg);
    HEaaN::Ciphertext ct_prime(*context);

    for (unsigned int k = 0; k < iters; k++) {

      // initializing final accumulation ciphertext ct_prime with 0
      enc->encrypt(pt, *pack, ct_prime);

      start = steady_clock::now();
      // doing the actual multplication and addition of diagonals and input
      // vector
      std::unique_ptr<vector<HEaaN::Ciphertext>> ct_result_vec = nullptr;
      HEaaN::Ciphertext tmp_ct(*context);
      eval->mult(inputCt_vec->at(0), diagonalCt_vec->at(0), tmp_ct);
      ct_result_vec = std::unique_ptr<vector<HEaaN::Ciphertext>>(
          new vector<HEaaN::Ciphertext>(
              1, tmp_ct)); // Vector constructor - 1st arg is size, 2nd argument
                           // is value

      // takes n rotations, multiplications, and additions, and has depth of one
      // multiplication, one rotation, and n additions
      for (size_t i = 1; i < dot_len; i++) {

        eval->leftRotate(inputCt_vec->at(i), i, tmp_ct);
        eval->mult(tmp_ct, diagonalCt_vec->at(i), tmp_ct);
        ct_result_vec->emplace_back(tmp_ct);
      }

      for (size_t i = 0; i < ct_result_vec->size(); i++) {
        eval->add(ct_prime, ct_result_vec->at(i), ct_prime);
      }

      end = steady_clock::now();

      HEaaN::Message dmsg;
      dec->decrypt(ct_prime, *sk, dmsg);
      std::vector<double> realResult =
          getLinearTransformVector(input_matrix, input_vector);

      vector<double> errors(dimension);
      for (size_t j = 0; j < dimension; j++) {
        errors[j] = abs(realResult[j] - dmsg[j]) / abs(realResult[j]);
      }

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

#ifdef PROF_DEBUG
    // testing the results
    HEaaN::Message dmsg;
    dec->decrypt(ct_prime, *sk, dmsg);
    for (size_t i = 0; i < input_size; i++) {

      std::cout << "Result: " << dmsg[i] << endl;
    }
    std::vector<double> realResult =
        getLinearTransformVector(input_matrix, input_vector);
#endif

    return ret;
  }

  // need FHE presets OR enough depth must be provided with SHE or CUSTOM
  // presets
  TestResult test_poly_eval(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    const u64 logSlots = HEaaN::getLogFullSlots(*context);

    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = 1 << logSlots;
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // to be implemented ----->>>>  get x, degree and coeffs vector from the
    // arguments
    double x = 1.0f;
    // depth has to be ceil(log2(degree))
    const vector<double> &coeffs = tparams.polynomial_coeffs;
    size_t degree = coeffs.size() - 1;

    HEaaN::Message ptx(logSlots);
    fillCustomReal(ptx, x);

    std::shared_ptr<HEaaN::Ciphertext> ctx =
        std::make_shared<HEaaN::Ciphertext>(*context);
    enc->encrypt(ptx, *pack, *ctx);

    vector<HEaaN::Message> plain_coeffs(degree + 1);

#ifdef PROF_DEBUG
    cout << "Degree of Polynomial = " << degree << endl;
    cout << "Polynomial = ";
    int counter = 0;
#endif

    for (unsigned int i = 0; i < degree + 1; i++) {
      HEaaN::Message tmp = this->getCustomMessage(coeffs[i]);
      plain_coeffs[i] = tmp;
#ifdef PROF_DEBUG
      cout << "x^" << counter << " * (" << coeffs[i] << ")"
           << ", ";
      counter++;
#endif
    }

    HEaaN::Message plain_result;
    vector<double> result;

    HEaaN::Message msg(logSlots);
    fillCustomReal(msg, 0);
    HEaaN::Plaintext pt(*context);
    pt = encoder->encode(msg);

    std::shared_ptr<HEaaN::Ciphertext> ctx0 =
        std::make_shared<HEaaN::Ciphertext>(*context);
    enc->encrypt(pt, *pack, *ctx0);

    for (unsigned int k = 0; k < iters; k++) {
      start = steady_clock::now();

      vector<std::shared_ptr<HEaaN::Ciphertext>> powers(degree + 1, nullptr);
      // start timing here
      powers[0] = ctx0;
      powers[1] = ctx;

      /*
      if(powers == nullptr){
        powers = std::unique_ptr<vector<HEaaN::Ciphertext> >(new
      vector<HEaaN::Ciphertext>(1, ctx)); //Vector constructor - 1st arg is
      size, 2nd argument is value powers->reserve(degree+1);
      }
      */

      vector<int> levels(degree + 1, 0);
      levels[1] = 0;
      levels[0] = 0;

      for (size_t i = 2; i <= degree; i++) {
        // compute x^i
        int minlevel = i;
        int cand = -1;
        for (size_t j = 1; j <= i / 2; j++) {
          int k = i - j;
          //
          int newlevel = max(levels[j], levels[k]) + 1;
          if (newlevel < minlevel) {
            cand = j;
            minlevel = newlevel;
          }
        }
        levels[i] = minlevel;
        // use cand
        if (cand < 0) {
          throw runtime_error("error");
        }
#ifdef PROF_DEBUG
        cout << "levels " << i << " = " << levels[i] << endl;
#endif
        // cand <= i - cand by definition
        // HEaaN::Ciphertext temp = *powers[cand];

        HEaaN::Ciphertext temp(*context);
        u64 currLevel = powers[i - cand]->getLevel();
        eval->levelDown(*powers[cand], currLevel, temp);

        HEaaN::Iphertext itxt_mult(*context);

        // assert(powers[i-cand] != nullptr);
        eval->Tensor(*powers[i - cand], temp, itxt_mult);
        if (powers[i] == nullptr) {
          powers[i] = std::shared_ptr<HEaaN::Ciphertext>(
              new HEaaN::Ciphertext(*context));
        }
        eval->Relinearize(itxt_mult, *powers[i]);
        eval->rescale(*powers[i]);
      }

      HEaaN::Ciphertext enc_result(*context);
      HEaaN::Plaintext plain_coeffs_pt(*context);
      plain_coeffs_pt = encoder->encode(plain_coeffs[0]);
      enc->encrypt(plain_coeffs_pt, *pack, enc_result);

      // testing
      HEaaN::Message dmsg;
#ifdef PROF_DEBUG
      for (size_t i = 1; i <= degree; i++) {
        dec->decrypt(*powers[i], *sk, dmsg);
        std::cout << "power  = " << dmsg[0] << endl;
      }
#endif

      for (size_t i = 1; i <= degree; i++) {

        HEaaN::Ciphertext temp(*context);
        plain_coeffs_pt = encoder->encode(plain_coeffs[i]);
        eval->mult(*powers[i], plain_coeffs_pt, temp);

        // dec->decrypt(temp, *sk, dmsg);

        // bringing them to the same level
        u64 currLevel1 = temp.getLevel();
        eval->levelDown(enc_result, currLevel1, enc_result);

        // setting the same scale for ciphertexts
        enc_result.setRescaleCounter(this->_params.scale());
        temp.setRescaleCounter(this->_params.scale());

        // finally accumulating the sum in enc_result
        eval->add(enc_result, temp, enc_result);
      }

      powers.clear();
      end = steady_clock::now();

      double realResult = getPlainPolyEval(x, coeffs);
      dec->decrypt(enc_result, *sk, dmsg);

#ifdef PROF_DEBUG
      cout << "Evaluation done" << endl;
      cout << "Calculated result : " << dmsg[0] << endl;
      cout << "Real result : " << realResult << endl;
#endif

      // vector<double> errors;
      // errors.push_back(abs(dmsg[0] - realResult));

      double error = abs(dmsg[0] - realResult) / abs(realResult);

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.push_back(error);
    }

    return ret;
  }

  // need CUSTOM presets for dimension = slot_count / 6
  TestResult test_rotdot_product(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    const u64 logSlots = HEaaN::getLogFullSlots(*context);

    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = 1 << logSlots;
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    HEaaN::Message msg(logSlots);

#ifdef PROF_DEBUG
    cout << "Slotcount: " << slot_count << endl;
#endif

    std::vector<double> inputsV;
    inputsV.reserve(slot_count);

    for (size_t i = 0; i < slot_count; i++) {
      inputsV.push_back(i + 0.001 * i);
      msg[i] = inputsV[i];
    }

    HEaaN::Plaintext pt(*context);
    pt = encoder->encode(msg);
    auto inputs = pt;

    HEaaN::Message weight_msg(logSlots);

    std::vector<double> weightsV;
    weightsV.reserve(slot_count);

    for (size_t i = 0; i < slot_count; i++) {
      weightsV.push_back((slot_count & 1) ? -1.0 : 2.00);
      weight_msg[i] = weightsV[i];
    }

    HEaaN::Plaintext pt1(*context);
    pt1 = encoder->encode(weight_msg);
    auto weights = pt1;

    HEaaN::Ciphertext temp_ct(*context);
    HEaaN::Ciphertext ct(*context), ct_result(*context), ct_rot(*context),
        ct_weight(*context);
    HEaaN::Iphertext itxt_mult(*context);

    enc->encrypt(weights, *pack, ct_weight);

    for (unsigned int k = 0; k < iters; k++) {

      enc->encrypt(inputs, *pack, ct);

      start = steady_clock::now();

      eval->mult(ct, ct_weight, ct_result);
      // eval->mult(ct, weight_msg, ct_result);

      // Sum the slots of ciphertext using log(D) rotations
      for (size_t i = 1; i <= slot_count / 2; i <<= 1) {

        eval->leftRotate(ct_result, i, temp_ct);
        eval->add(ct_result, temp_ct, ct_result);
      }

      end = steady_clock::now();

      HEaaN::Message dmsg;
      dec->decrypt(ct_result, *sk, dmsg);

      double realResult = std::inner_product(inputsV.begin(), inputsV.end(),
                                             weightsV.begin(), 0.0);

      // vector<double> errors;
      // errors.push_back(abs(dmsg[0] - realResult));

      double error = abs(dmsg[0] - realResult) / abs(realResult);

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.push_back(error);
    }

#ifdef PROF_DEBUG
    HEaaN::Message dmsg;
    dec->decrypt(ct_result, *sk, dmsg);
    // testing
    cout << "Result: " << dmsg[0] << endl;
    cout << "True result: "
         << std::inner_product(inputsV.begin(), inputsV.end(), weightsV.begin(),
                               0.0)
         << endl;
#endif

    return ret;
  }

  string profiler_name() const { return "HEAANProfiler"; }

  // FHE Primitivies
  TestResult test_add(const TestParams &tparams) const {
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    const u64 logSlots = HEaaN::getLogFullSlots(*context);

    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = 1 << logSlots;
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // setting up msg objects
    HEaaN::Message msg1(logSlots);
    HEaaN::Message msg2(logSlots);
    HEaaN::Message msg_add(logSlots);
    HEaaN::Message dmsg_add;

    // setting up the ciphertext objects
    HEaaN::Ciphertext ctxt1(*context);
    HEaaN::Ciphertext ctxt2(*context);
    HEaaN::Ciphertext ctxt_add(*context);

    // ------------------------  Running profiling test  -----------------------

    for (unsigned int i = 0; i < iters; i++) {

      fillRandomReal(msg1);
      fillRandomReal(msg2);

      HEaaN::Plaintext pt1(*context);
      HEaaN::Plaintext pt2(*context);
      pt1 = encoder->encode(msg1);
      pt2 = encoder->encode(msg2);

      enc->encrypt(pt1, *pack, ctxt1);
      enc->encrypt(pt2, *pack, ctxt2);

      start = steady_clock::now();
      eval->add(ctxt1, ctxt2, ctxt_add);
      end = steady_clock::now();

      dec->decrypt(ctxt_add, *sk, dmsg_add);
      eval->add(msg1, msg2, msg_add);

      vector<double> errors(msg1.getSize());

      for (u64 j = 0; j < dmsg_add.getSize(); j++) {
        errors[j] = abs(dmsg_add[j] - msg_add[j]) / abs(msg_add[j]);
      }

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

    return ret;
  }

  TestResult test_plain_add(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    const u64 logSlots = HEaaN::getLogFullSlots(*context);

    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = 1 << logSlots;
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // setting up msg objects
    HEaaN::Message msg1(logSlots);
    HEaaN::Message msg2(logSlots);
    HEaaN::Message msg_add(logSlots);
    HEaaN::Message dmsg_add;

    // setting up the ciphertext objects
    HEaaN::Ciphertext ctxt1(*context);
    HEaaN::Ciphertext ctxt_add(*context);

    // ------------------------  Running profiling test  -----------------------

    for (unsigned int i = 0; i < iters; i++) {

      fillRandomReal(msg1);
      fillRandomReal(msg2);

      HEaaN::Plaintext pt1(*context);
      HEaaN::Plaintext pt2(*context);
      pt1 = encoder->encode(msg1);
      pt2 = encoder->encode(msg2);

      enc->encrypt(pt1, *pack, ctxt1);

      start = steady_clock::now();
      eval->add(ctxt1, pt2, ctxt_add);
      end = steady_clock::now();

      /*
      dec->decrypt(ctxt_add, *sk, dmsg_add);
      eval->add(msg1, msg2, msg_add);

      vector<double> errors(msg1.getSize());
      for (u64 j = 0; j < dmsg_add.getSize(); j++) {
        errors[j] = abs(dmsg_add[j] - msg_add[j]) / abs(msg_add[j]);
      }
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
      */

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
    }

    return ret;
  }

  TestResult test_plain_mult(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    const u64 logSlots = HEaaN::getLogFullSlots(*context);

    ret.runtimes.reserve(iters);
    unsigned int slot_count = 1 << logSlots;
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // setting up msg objects
    HEaaN::Message msg1(logSlots);
    HEaaN::Message msg2(logSlots);
    HEaaN::Message msg_mult(logSlots);
    HEaaN::Message dmsg_mult;

    // setting up the ciphertext objects
    HEaaN::Ciphertext ctxt1(*context);
    HEaaN::Ciphertext ctxt_mult(*context);

    // ------------------------  Running profiling test  -----------------------

    for (unsigned int i = 0; i < iters; i++) {

      fillRandomReal(msg1);
      fillRandomReal(msg2);

      HEaaN::Plaintext pt1(*context);
      HEaaN::Plaintext pt2(*context);
      pt1 = encoder->encode(msg1);
      pt2 = encoder->encode(msg2);

      enc->encrypt(pt1, *pack, ctxt1);

      start = steady_clock::now();
      eval->mult(ctxt1, pt2, ctxt_mult);
      end = steady_clock::now();

      /*
      dec->decrypt(ctxt_mult, *sk, dmsg_mult);
      eval->mult(msg1, msg2, msg_mult);


      vector<double> errors(msg1.getSize());
      for (u64 j = 0; j < msg_mult.getSize(); j++) {
        errors[j] = abs(msg_mult[j] - dmsg_mult[j]) / abs(msg_mult[j]);
      }
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
      */

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
    }

    return ret;
  }

  TestResult test_chain(const TestParams &tparams) const {
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    const u64 logSlots = HEaaN::getLogFullSlots(*context);

    ret.runtimes.reserve(iters);
    unsigned int slot_count = 1 << logSlots;
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    unsigned int depth = this->_params.depth() - 1;

    // setting up msg objects
    HEaaN::Message msg1(logSlots);
    HEaaN::Ciphertext ctxt1(*context);
    HEaaN::Message dmsg_mult;
    HEaaN::Iphertext itxt_mult(*context); // required to perform relinearization

    for (unsigned int i = 0; i < iters; i++) {

      fillRandomReal(msg1);

      HEaaN::Plaintext pt1(*context);
      pt1 = encoder->encode(msg1);

      enc->encrypt(pt1, *pack, ctxt1);

      start = steady_clock::now();
      for (unsigned int j = 0; j < depth; j++) {
        eval->square(ctxt1, ctxt1);
        // I don't think we need these with the squaring function
        /*
        eval->Relinearize(itxt_mult, ctxt1);
        eval->rescale(ctxt1);
        */
      }
      end = steady_clock::now();

      // Plain computation
      for (unsigned int k = 0; k < depth; k++) {
        eval->mult(msg1, msg1, msg1);
      }

      dec->decrypt(ctxt1, *sk, dmsg_mult);

      vector<double> errors(msg1.getSize());

      for (u64 j = 0; j < msg1.getSize(); j++) {
        errors[j] = abs(msg1[j] - dmsg_mult[j]) / abs(msg1[j]);
      }

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

    return ret;
  }

  TestResult test_multiply(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    const u64 logSlots = HEaaN::getLogFullSlots(*context);

    ret.runtimes.reserve(iters);
    unsigned int slot_count = 1 << logSlots;
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // setting up msg objects
    HEaaN::Message msg1(logSlots);
    HEaaN::Message msg2(logSlots);
    HEaaN::Message msg_mult(logSlots);
    HEaaN::Message dmsg_mult;

    // setting up the ciphertext objects
    HEaaN::Ciphertext ctxt1(*context);
    HEaaN::Ciphertext ctxt2(*context);
    HEaaN::Ciphertext ctxt_mult(*context);
    HEaaN::Iphertext itxt_mult(*context); // required to perform relinearization

    // ------------------------  Running profiling test  -----------------------

    for (unsigned int i = 0; i < iters; i++) {

      fillRandomReal(msg1);
      fillRandomReal(msg2);

      HEaaN::Plaintext pt1(*context);
      HEaaN::Plaintext pt2(*context);
      pt1 = encoder->encode(msg1);
      pt2 = encoder->encode(msg2);

      enc->encrypt(pt1, *pack, ctxt1);
      enc->encrypt(pt2, *pack, ctxt2);

      start = steady_clock::now();

      eval->Tensor(ctxt1, ctxt2, itxt_mult);
      eval->Relinearize(itxt_mult, ctxt_mult);
      eval->rescale(ctxt_mult);

      end = steady_clock::now();

      dec->decrypt(ctxt_mult, *sk, dmsg_mult);
      eval->mult(msg1, msg2, msg_mult);

      vector<double> errors(msg1.getSize());

      for (u64 j = 0; j < msg_mult.getSize(); j++) {
        errors[j] = abs(msg_mult[j] - dmsg_mult[j]) / abs(msg_mult[j]);
      }

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

    return ret;
  }

  // currently we have to perform relinearization to get ciphertext back from
  // the iphertext to test accuracy
  TestResult test_multiply_no_relin(const TestParams &tparams) const {

    // only getting the tensor.. no relinearization and no rescaling
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    const u64 logSlots = HEaaN::getLogFullSlots(*context);

    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = 1 << logSlots;
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // setting up msg objects
    HEaaN::Message msg1(logSlots);
    HEaaN::Message msg2(logSlots);
    HEaaN::Message msg_mult(logSlots);
    HEaaN::Message dmsg_mult;

    // setting up the ciphertext objects
    HEaaN::Ciphertext ctxt1(*context);
    HEaaN::Ciphertext ctxt2(*context);
    HEaaN::Ciphertext ctxt_mult(*context);
    HEaaN::Iphertext itxt_mult(*context); // required to perform relinearization

    // ------------------------  Running profiling test  -----------------------

    for (unsigned int i = 0; i < iters; i++) {

      fillRandomReal(msg1);
      fillRandomReal(msg2);

      HEaaN::Plaintext pt1(*context);
      HEaaN::Plaintext pt2(*context);
      pt1 = encoder->encode(msg1);
      pt2 = encoder->encode(msg2);

      enc->encrypt(pt1, *pack, ctxt1);
      enc->encrypt(pt2, *pack, ctxt2);

      start = steady_clock::now();

      eval->Tensor(ctxt1, ctxt2, itxt_mult);

      end = steady_clock::now();

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
    }

    return ret;
  }

  // currently rotates left only
  TestResult test_rotate(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    const u64 logSlots = HEaaN::getLogFullSlots(*context);

    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = 1 << logSlots;
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // setting up msg objects
    HEaaN::Message msg(logSlots);
    HEaaN::Message msg_rot(logSlots);
    HEaaN::Message dmsg, dmsg_rot;

    // setting up the ciphertext object
    HEaaN::Ciphertext ctxt(*context);
    HEaaN::Ciphertext ctxt_rot(*context);

    // ------------------------  Running profiling test  -----------------------

    const int rot_num = 1;

    for (unsigned int i = 0; i < iters; i++) {

      fillRandomReal(msg);

      HEaaN::Plaintext pt(*context);
      pt = encoder->encode(msg);

      // int rot_num = rand() % (msg.getSize());
      enc->encrypt(pt, *pack, ctxt);

      start = steady_clock::now();
      eval->leftRotate(ctxt, rot_num, ctxt_rot);
      end = steady_clock::now();

      dec->decrypt(ctxt_rot, *sk, dmsg_rot);
      eval->leftRotate(msg, rot_num, msg_rot);

      vector<double> errors(msg.getSize());

      for (u64 j = 0; j < dmsg_rot.getSize(); j++) {
        errors[j] = abs(dmsg_rot[j] - msg_rot[j]) / abs(msg_rot[j]);
      }

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

    return ret;
  }

  TestResult test_relin(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    const u64 logSlots = HEaaN::getLogFullSlots(*context);

    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = 1 << logSlots;
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // setting up msg objects
    HEaaN::Message msg1(logSlots);
    HEaaN::Message msg2(logSlots);

    // basically encoding of the random real numbers into msg objects
    fillRandomReal(msg1);
    fillRandomReal(msg2);

    HEaaN::Plaintext pt1(*context);
    HEaaN::Plaintext pt2(*context);
    pt1 = encoder->encode(msg1);
    pt2 = encoder->encode(msg2);

    // setting up the ciphertext objects
    HEaaN::Ciphertext ctxt1(*context);
    HEaaN::Ciphertext ctxt2(*context);
    HEaaN::Ciphertext ctxt_mult(*context);
    HEaaN::Iphertext itxt_mult(*context); // required to perform relinearization

    // encrypting msg1 snd msg2 into ctxt1 and ctxt2 using key pack
    enc->encrypt(pt1, *pack, ctxt1);
    enc->encrypt(pt2, *pack, ctxt2);
    eval->Tensor(ctxt1, ctxt2, itxt_mult);

    // ------------------------  Running profiling test  -----------------------

    for (unsigned int i = 0; i < iters; i++) {

      start = steady_clock::now();
      eval->Relinearize(itxt_mult, ctxt_mult);
      end = steady_clock::now();

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
    }

    return ret;
    // return {0.0};
  }

  TestResult test_rescale(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    const u64 logSlots = HEaaN::getLogFullSlots(*context);

    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = 1 << logSlots;
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // setting up msg objects
    HEaaN::Message msg1(logSlots);
    fillRandomReal(msg1);

    // setting up the ciphertext objects
    HEaaN::Ciphertext ctxt1(*context);

    // encrypting msg1  into ctxt1  using key pack

    // ------------------------  Running profiling test  -----------------------

    for (unsigned int i = 0; i < iters; i++) {

      HEaaN::Plaintext pt1(*context);
      pt1 = encoder->encode(msg1);

      enc->encrypt(pt1, *pack, ctxt1);
      start = steady_clock::now();
      // need to make sure a ciphertext is rescalable -> make iters < #. of
      // primes in coeffmodulus can use ctxt1.isRescaleNeeded() to determine
      // whether their ciphertext requires rescaling or not
      eval->rescale(ctxt1);
      end = steady_clock::now();

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
    }

    return ret;
  }

  TestResult test_bootstrap(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    const u64 logSlots = HEaaN::getLogFullSlots(*context);

    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = 1 << logSlots;
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // setting up msg objects
    HEaaN::Message msg(logSlots);
    HEaaN::Message dmsg, dmsg_boot;

    // setting up the ciphertext objects
    HEaaN::Ciphertext ctxt(*context);
    HEaaN::Ciphertext ctxt_boot(*context, logSlots);

    // HEaaN::makeBootstrappable(*context);  -> enable bootstrapping in the curr
    // context, need to do it before creating the HomEvaluator object

    // ------------------------  Running profiling test  -----------------------

    for (unsigned int i = 0; i < iters; i++) {
      fillRandomReal(msg);

      HEaaN::Plaintext pt(*context);
      pt = encoder->encode(msg);

      enc->encrypt(pt, *pack, ctxt);

      start = steady_clock::now();
      // Remaining depth after bootstrapping can be found using
      // ctxt_boot.getLevel() - eval.getMinLevelForBootstrap()
      eval->bootstrap(ctxt, ctxt_boot);
      end = steady_clock::now();

      dec->decrypt(ctxt_boot, *sk, dmsg_boot);

      vector<double> errors(msg.getSize());

      for (u64 j = 0; j < msg.getSize(); j++) {
        errors[j] = abs(dmsg_boot[j] - msg[j]) / abs(msg[j]);
      }

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

    return ret;
  }

  TestResult test_encode_error(const TestParams &tparams) const {

    unsigned int iters = tparams.iterations();
    TestResult ret;
    const u64 logSlots = HEaaN::getLogFullSlots(*context);

    // Make sure to reserve!
    unsigned int slot_count = 1 << logSlots;
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // setting up msg objects
    HEaaN::Message msg1(logSlots);
    HEaaN::Message dmsg;
    HEaaN::Message msg;

    // setting up the ciphertext objects
    HEaaN::Ciphertext ctxt1(*context);

    // ------------------------  Running profiling test  -----------------------

    for (unsigned int i = 0; i < iters; i++) {

      fillRandomReal(msg1);

      HEaaN::Plaintext pt1(*context);
      pt1 = encoder->encode(msg1);

      enc->encrypt(pt1, *pack, ctxt1);

      dec->decrypt(ctxt1, *sk, dmsg);
      msg = encoder->decode(pt1);

      vector<double> errors(msg1.getSize());

      for (u64 j = 0; j < dmsg.getSize(); j++) {
        errors[j] = abs(dmsg[j] - msg[j]) / abs(msg[j]);
      }

      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

    return ret;
  }

  // End-to-end functions

  // Helper
  HEaaN::Ciphertext single_dot(const HEaaN::Ciphertext &ct0,
                               const HEaaN::Ciphertext &ct1) const {
    const u64 logSlots = HEaaN::getLogFullSlots(*context);
    HEaaN::Ciphertext ret(*context);
    HEaaN::Ciphertext tmp(*context);
    unsigned int slot_count = 1 << logSlots;
    this->eval->mult(ct0, ct1, ret);
    for (size_t i = 1; i <= slot_count / 2; i <<= 1) {
      this->eval->leftRotate(ret, i, tmp);
      this->eval->add(ret, tmp, ret);
    }
    return ret;
  }

  TestResult test_dot(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    const u64 logSlots = HEaaN::getLogFullSlots(*context);

    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = 1 << logSlots;
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    unsigned int dot_vec_len =
        tparams.dot_len / slot_count + (tparams.dot_len % slot_count ? 1 : 0);

    vector<HEaaN::Ciphertext> v1, v2;
    vector<HEaaN::Message> m1, m2;

    v1.reserve(dot_vec_len);
    v2.reserve(dot_vec_len);

    for (unsigned int i = 0; i < dot_vec_len; i++) {

      HEaaN::Message temp_msg1(logSlots);
      HEaaN::Ciphertext temp_ctxt(*context);
      fillRandomReal(temp_msg1);
      HEaaN::Plaintext pt1(*context);
      pt1 = encoder->encode(temp_msg1);

      enc->encrypt(pt1, *pack, temp_ctxt);
      v1.push_back(temp_ctxt);
      m1.push_back(temp_msg1);

      fillRandomReal(temp_msg1);
      pt1 = encoder->encode(temp_msg1);

      enc->encrypt(pt1, *pack, temp_ctxt);
      v2.push_back(temp_ctxt);
      m2.push_back(temp_msg1);
    }

    HEaaN::Message
        dfinal_ctxt; // TODO fix the naming - don't name a message "ctxt"

    HEaaN::Iphertext itxt_mult(*context); // required to perform relinearization
    HEaaN::Iphertext temp_itxt(*context);

    std::complex<double> plain_result = 0.0f;
    // plain dot product
    for (size_t i = 0; i < m1.size(); i++) {
      plain_result += plain_dot(m1[i], m2[i]);
    }

    HEaaN::Ciphertext final_ctxt(*context);
    HEaaN::Ciphertext tmp(*context);
    for (unsigned int j = 0; j < iters; j++) {
      start = steady_clock::now();
      for (size_t i = 0; i < v1.size(); i++) {
        if (!i) {
          final_ctxt = single_dot(v1[i], v2[i]);
        } else {
          tmp = single_dot(v1[i], v2[i]);
          eval->add(final_ctxt, tmp, final_ctxt);
        }
      }
      end = steady_clock::now();

      dec->decrypt(final_ctxt, *sk, dfinal_ctxt);
      vector<double> errors(dfinal_ctxt.getSize());

      for (u64 j = 0; j < dfinal_ctxt.getSize(); j++) {
        errors[j] = abs(dfinal_ctxt[j] - plain_result) / abs(plain_result);
      }

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

    return ret;
  }

  // Sizes
  // Returns only a single element in their vector
  // Technically there's a precision loss with floating-point, but the results
  // shouldn't be nearly large enough for issues
  TestResult ciphertext_size(const TestParams &tparams) const {

    stringstream ss;

    const u64 logSlots = HEaaN::getLogFullSlots(*context);
    HEaaN::Message msg1(logSlots);
    fillRandomReal(msg1);
    HEaaN::Plaintext pt1(*context);
    pt1 = encoder->encode(msg1);

    HEaaN::Ciphertext ctxt1(*context);
    enc->encrypt(pt1, *pack, ctxt1);

    // void HEaaN::Ciphertext::save	(	std::ostream & 	stream	)
    // const
    ctxt1.save(ss);

    // https://stackoverflow.com/questions/4432793/size-of-stringstream
    ss.seekp(0, ios::end);
    stringstream::pos_type offset = ss.tellp();

    TestResult t;
    t.runtimes = {(long double)offset};
    return t;
  }

  TestResult secret_key_size(const TestParams &tparams) const {

    string saved_key_path = ".heaan_sk.bin";
    sk->save(saved_key_path);
    //
    // // std::filesystem::path p = saved_key_path;
    // // std::filesystem::path pa = p / std::filesystem::current_path();
    // // unsigned int file_size = std::filesystem::file_size(pa);
    //
    // // std::filesystem::remove(pa);
    //
    // //
    // https://www.tutorialspoint.com/how-can-i-get-a-file-s-size-in-cplusplus
    ifstream in_file(saved_key_path, ios::binary);
    in_file.seekg(0, ios::end);
    int file_size = in_file.tellg();

    remove(saved_key_path.c_str());

    TestResult t;
    t.runtimes = {(long double)file_size};
    return t;
  }

  TestResult public_key_size(const TestParams &tparams) const {

    string saved_key_path = keyDirPath;
    keygen->save(saved_key_path);

    long long int file_size = getFolderSize(keyDirPath);

    keygen->flush();

    TestResult t;
    t.runtimes = {(long double)file_size};
    return t;
  }

  TestResult bsk_size(const TestParams &tparams) const {
    TestResult t;
    t.runtimes = {0.0};
    return t;
  }

  TestResult params_size(const TestParams &tparams) const {
    TestResult t;
    t.runtimes = {0.0};
    return t;
  }

  TestResult ctext_modulus_bits() const {
    TestResult t;
    t.runtimes = {1};
    return t;
  }

  TestResult scaling_factor() const {
    // In HEAAN we can get the scaling factor with an empty ciphertext
    TestResult t;
    HEaaN::Ciphertext ctxt1(*context);

    const auto scale_bits = ctxt1.getCurrentScaleFactor();
    t.runtimes = {(long double)scale_bits};
    return t;
  }

  TestResult num_columns() const {
    TestResult t;
    unsigned int cols = 0; // TODO

    t.runtimes = {(long double)cols};
    return t;
  }
};

#endif
