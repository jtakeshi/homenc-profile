#ifndef LINEARALGEBRA_H
#define LINEARALGEBRA_H

#include <vector>
using namespace std;

vector<double>
getLinearTransformVector(const vector<vector<double>> &input_matrix,
                         const vector<double> &input_vector) {

  double input_size = input_matrix[0].size();
  std::vector<double> result_vector(input_size, 0);

  for (int i = 0; i < input_size; i++) {
    for (int j = 0; j < input_size; j++) {
      result_vector[i] += (input_matrix[i][j] * input_vector[j]);
    }
  }

#ifdef PROF_DEBUG
  cout << "Result Vector :: " << endl;
  for (int i = 0; i < input_size; i++) {
    cout << result_vector[i] << "  " << endl;
  }
#endif

  return result_vector;
}

// only square matrices
vector<vector<double>> generateUVector(const size_t dimension) {

  vector<vector<double>> vec(dimension, vector<double>(dimension));

  // random fillings
  for (size_t i = 0; i < dimension; i++) {
    for (size_t j = 0; j < dimension; j++) {
      vec[i][j] = j + i + 1;
    }
  }

  return vec;
}

vector<vector<double>>
getDiagonalMatrix(const vector<vector<double>> &input_matrix) {

  double input_size = input_matrix[0].size();
  vector<vector<double>> diagonal_matrix(input_size,
                                         vector<double>(input_size));

  for (size_t j = 0; j < input_size; j++) {
    for (size_t i = 0; i < input_size; i++) {

      size_t itr = j + i;
      if (itr >= input_size) {
        itr = abs(input_size - itr);
      }

      diagonal_matrix[j][i] = input_matrix[j][itr];
    }
  }

  return diagonal_matrix;
}

double getPlainPolyEval(const double x, const vector<double> &f) {

  double ret = 0;
  double x_pow = 1;

  for (size_t i = 0; i < f.size(); i++) {
    ret += f[i] * x_pow;
    x_pow *= x;
  }

  return ret;
}

#endif
