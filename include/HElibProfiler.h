#ifndef HELIBPROFILER_H
#define HELIBPROFILER_H

#include "Profiler.h"
#include "linearAlgebra.h"
#include <cassert>
#include <cmath>
#include <cstdint>
#include <fstream>
#include <helib/helib.h>
#include <seal/seal.h>

void assign_optimized_params(const unsigned int depth, unsigned int &m,
                             unsigned int &bits, unsigned int &c) {
  if (depth < 2) {
    m = 16384;
    bits = 119;
    c = 2;
  } else if (depth < 5) {
    m = 32768;
    bits = 239;
    c = 2;
  } else if (depth <= 8) {
    m = 32768;
    bits = 358;
    c = 6;
  } else if (depth <= 10) {
    m = 65536;
    bits = 613;
    c = 3;
  } else if (depth <= 15) {
    m = 65536;
    bits = 669;
    c = 4;
  } else if (depth == 16) {
    m = 65536;
    bits = 717;
    c = 6;
  } else if (depth <= 19) {
    m = 131072;
    bits = 1329;
    c = 4;
  }
  // for depth 17 and 18 if too much error occurs after testing
  // m = 131072;
  // bits = 1387;
  // c = 5;
  if (depth > 19) {
    throw runtime_error("Depth exceeded maximum size");
  }
}

class HElibProfiler : public Profiler {
private:
  unique_ptr<helib::Context> context;
  unique_ptr<helib::SecKey> secret_key;
  unique_ptr<helib::PubKey> public_key;
  unsigned int scale_bits = 0;
  unsigned int cols;

  helib::Ctxt rand_ctext(vector<double> &args) const {
    size_t slot_count = context->getNSlots();
    srand(time(NULL));
    args.clear();
    args.resize(slot_count);
    for (size_t i = 0; i < args.size(); i++) {
      // args[i] = (double)rand() / RAND_MAX;
      args[i] = rand_plain_val();
    }
    helib::Ptxt<helib::CKKS> p(*context, args);
    helib::Ctxt c(*public_key);
    public_key->Encrypt(c, p);

    return c;
  }

  helib::Ctxt custom_ctext(vector<double> &args, vector<double> &row) const {
    size_t slot_count = context->getNSlots();
    args.clear();
    args.resize(slot_count);

    int row_size = row.size();

    for (size_t i = 0; i < args.size(); i++) {
      args[i] = row[i % row_size];
    }
    helib::PtxtArray p(*context, args);

    helib::Ctxt c(*public_key);
    p.encrypt(c);
    return c;
  }

public:
  static const unsigned int COLUMNS = 2; // Smallest workable value

  HElibProfiler(const HEParams &params_in) : Profiler(params_in) {

    // TODO - how to get params based on what HElib uses
    // NOTE: HElib does not support bootstrapping in CKKS from what I can tell
    helib::ContextBuilder<helib::CKKS> builder;

    /*
    //Old code using SEAL - Nirajan's brute-force method is above

    // Shamelessly stealing SEAL's method
    // Get modulus chain
    //
    https://github.com/microsoft/SEAL/blob/main/native/examples/4_ckks_basics.cpp
    unsigned int desired_depth = params_in.depth();

    // Total bits in the ciphertext modulus
    unsigned int ctext_modulus_bits = (2 * params_in.edge_modulus_bits()) +
                                      (desired_depth)*params_in.scale_bits();

    //cerr << "Requesting " << ctext_modulus_bits << " q bits" << endl;

    // Get an appropriate polynomial modulus degree
    unsigned int log_poly_mod_deg = 10;
    while (seal::CoeffModulus::MaxBitCount(
               1 << log_poly_mod_deg,
               static_cast<seal::sec_level_type>(params_in.security())) <
           (int)ctext_modulus_bits) {
      log_poly_mod_deg++;
    }

    //cerr << "Requesting " << (1 << (log_poly_mod_deg)) << " N" << endl;

    builder.m(1 << (log_poly_mod_deg + 1));
    builder.bits(ctext_modulus_bits);
    builder.scale(scale_bits);
    builder.c(COLUMNS);
    */

    unsigned int m = 0, bits = 0, c = 0;
    assign_optimized_params(params_in.depth(), m, bits, c);
    this->cols = c;

    builder.m(m);
    builder.bits(bits);
    builder.c(c);
    scale_bits = params_in.scale_bits();
    builder.scale(scale_bits);

    context = unique_ptr<helib::Context>(builder.buildPtr());

    secret_key = unique_ptr<helib::SecKey>(new helib::SecKey(*context));
    secret_key->GenSecKey();

    helib::addSome1DMatrices(*secret_key);

    public_key = unique_ptr<helib::PubKey>(new helib::PubKey(
        *secret_key)); // weird way to do it, but I think it works
  }

  void reset_params(const HEParams &params_in) { return; }

  string profiler_name() const { return "HElibProfiler"; }

  // Tests should take in a set of parameters and return a list of long double,
  // representing runtimes for each function

  // FHE Primitives
  TestResult test_add(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = context->getNSlots();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    vector<double> arg0, arg1;
    helib::Ctxt c0(*public_key), c1(*public_key), c2(*public_key);

    for (unsigned int i = 0; i < iters; i++) {
      // Get random input
      c0 = this->rand_ctext(arg0);
      c1 = this->rand_ctext(arg1);
      // Run the test
      start = steady_clock::now();
      c2 = c0;
      c2 += c1;
      end = steady_clock::now();
      // Get the result of the homomorphic computation
      helib::PtxtArray pt(*context);

      pt.decrypt(c2, *secret_key);

      vector<double> enc_result;
      pt.store(enc_result);

      vector<double> errors(arg0.size());

      for (size_t j = 0; j < arg0.size(); j++) {
        errors[j] =
            abs((arg0[j] + arg1[j]) - enc_result[j]) / abs(arg0[j] + arg1[j]);
      }

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }

    return ret;
  }

  TestResult test_plain_add(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = context->getNSlots();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    vector<double> arg0, arg1;
    helib::Ctxt c0(*public_key), c2(*public_key);

    arg1.clear();
    arg1.resize(slot_count);
    for (size_t i = 0; i < arg1.size(); i++) {
      arg1[i] = rand_plain_val();
    }
    helib::Ptxt<helib::CKKS> pt1(*context, arg1);

    for (unsigned int i = 0; i < iters; i++) {
      // Get random input
      c0 = this->rand_ctext(arg0);

      // Run the test
      start = steady_clock::now();
      c2 = c0;
      c2 += pt1;
      end = steady_clock::now();
      // Get the result of the homomorphic computation
      /*
      helib::PtxtArray pt(*context);

      pt.decrypt(c2, *secret_key);

      vector<double> enc_result;
      pt.store(enc_result);


      vector<double> errors(arg0.size());
      for (size_t j = 0; j < arg0.size(); j++) {
        errors[j] =
            abs((arg0[j] + arg1[j]) - enc_result[j]) / abs(arg0[j] + arg1[j]);
      }
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
      */

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
    }

    return ret;
  }

  TestResult test_plain_mult(const TestParams &tparams) const {

    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = context->getNSlots();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    vector<double> arg0, arg1;
    helib::Ctxt c0(*public_key), c2(*public_key);

    arg1.clear();
    arg1.resize(slot_count);
    for (size_t i = 0; i < arg1.size(); i++) {
      arg1[i] = rand_plain_val();
    }
    helib::Ptxt<helib::CKKS> pt1(*context, arg1);

    for (unsigned int i = 0; i < iters; i++) {
      // Get random input
      c0 = this->rand_ctext(arg0);

      // Run the test
      start = steady_clock::now();
      c2 = c0;
      c2 *= pt1;
      end = steady_clock::now();
      // Get the result of the homomorphic computation
      /*
      helib::PtxtArray pt(*context);

      pt.decrypt(c2, *secret_key);

      vector<double> enc_result;
      pt.store(enc_result);

      vector<double> errors(arg0.size());

      for (size_t j = 0; j < arg0.size(); j++) {
        errors[j] =
            abs((arg0[j] + arg1[j]) - enc_result[j]) / abs(arg0[j] + arg1[j]);
      }
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
      */

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      
    }

    return ret;
  }

  TestResult test_chain(const TestParams &tparams) const {
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = context->getNSlots();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    vector<double> arg0;
    helib::Ctxt c0(*public_key);

    unsigned int depth = this->_params.depth() - 1;

    for (unsigned int i = 0; i < iters; i++) {
      // Get random input
      c0 = this->rand_ctext(arg0);
      // Run the test
      start = steady_clock::now();
      for (unsigned int j = 0; j < depth; j++) {
        c0 *= c0;
      }
      end = steady_clock::now();

      for (unsigned int j = 0; j < depth; j++) {
        for (double &d : arg0) {
          d *= d;
        }
      }

      // Get the result of the homomorphic computation
      helib::PtxtArray pt(*context);

      pt.decrypt(c0, *secret_key);

      vector<double> enc_result;
      pt.store(enc_result);

      vector<double> errors(arg0.size());

      for (size_t j = 0; j < arg0.size(); j++) {
        errors[j] = abs((arg0[j]) - enc_result[j]) / abs(arg0[j]);
      }

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }
    return ret;
  }

  TestResult test_multiply(const TestParams &tparams) const {
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = context->getNSlots();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    vector<double> arg0, arg1;
    helib::Ctxt c0(*public_key), c1(*public_key), c2(*public_key, 1);

    for (unsigned int i = 0; i < iters; i++) {
      // Get random input
      c0 = this->rand_ctext(arg0);
      c1 = this->rand_ctext(arg1);
      // Run the test
      start = steady_clock::now();
      c2 = c0;
      c2 *= c1;
      end = steady_clock::now();
      // Get the result of the homomorphic computation
      helib::PtxtArray pt(*context);

      pt.decrypt(c2, *secret_key);

      vector<double> enc_result;
      pt.store(enc_result);

      vector<double> errors(arg0.size());

      for (size_t j = 0; j < arg0.size(); j++) {
        errors[j] =
            abs((arg0[j] * arg1[j]) - enc_result[j]) / abs(arg0[j] * arg1[j]);
      }

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }
    return ret;
  }

  TestResult test_multiply_no_relin(const TestParams &tparams) const {
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = context->getNSlots();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    vector<double> arg0, arg1;
    helib::Ctxt c0(*public_key), c1(*public_key), c2(*public_key, 1);

    for (unsigned int i = 0; i < iters; i++) {
      // Get random input
      c0 = this->rand_ctext(arg0);
      c1 = this->rand_ctext(arg1);
      // Run the test
      start = steady_clock::now();
      c2 = c0;
      c2.multLowLvl(c1);
      end = steady_clock::now();
      // Get the result of the homomorphic computation
      helib::PtxtArray pt(*context);

      pt.decrypt(c2, *secret_key);

      vector<double> enc_result;
      pt.store(enc_result);

      vector<double> errors(arg0.size());
      for (size_t j = 0; j < arg0.size(); j++) {
        errors[j] =
            abs((arg0[j] * arg1[j]) - enc_result[j]) / abs(arg0[j] * arg1[j]);
      }

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);

      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
    }
    return ret;
  }

  TestResult test_rotate(const TestParams &tparams) const {
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;
    // Make sure to reserve!
    ret.runtimes.reserve(iters);
    unsigned int slot_count = context->getNSlots();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    vector<double> args;
    helib::Ctxt c(*public_key), dest(*public_key);

    const size_t rotate_amount = 1;

    for (unsigned int i = 0; i < iters; i++) {

      c = rand_ctext(args);

      start = steady_clock::now();
      helib::rotate(c, rotate_amount);
      end = steady_clock::now();

      helib::PtxtArray pt(*context);
      pt.decrypt(c, *secret_key);

      vector<double> hom_rotated;
      pt.store(hom_rotated);

      vector<double> errors(args.size());
      assert(args.size() == slot_count);
      assert(hom_rotated.size() == slot_count);

      for (size_t j = 0; j < errors.size(); j++) {
        errors[j] =
            abs(hom_rotated[j] - args[(j + rotate_amount) % slot_count]) /
            abs(args[(j + rotate_amount) % slot_count]);
      }

      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
    }
    return ret;
  }

  TestResult test_relin(const TestParams &tparams) const {
    steady_clock::time_point start, end;
    unsigned int iters = tparams.iterations();
    TestResult ret;

    ret.runtimes.reserve(iters);
    unsigned int slot_count = context->getNSlots();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // setup
    vector<double> args;
    helib::Ctxt c0 = this->rand_ctext(args), c1 = this->rand_ctext(args),
                c2(*public_key), c3(*public_key);
    c2 = c1;
    c2.multLowLvl(c0);

    // Run and time tests
    for (unsigned int i = 0; i < iters; i++) {

      c3 = c2;
      start = steady_clock::now();
      c3.reLinearize();
      end = steady_clock::now();

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
    }

    return ret;
  }
  TestResult test_rescale(const TestParams &tparams) const {
    // Probably should have a block like this at the start of every runtime test
    // steady_clock::time_point start, end;
    // unsigned int iters = tparams.iterations();
    TestResult ret;

    /*
    ret.runtimes.reserve(iters);
    unsigned int slot_count = context->getNSlots();
    ret.batchsize = slot_count;
    ret.errors.reserve(slot_count * iters);

    // Setup
    vector<double> args;
    helib::Ctxt c0 = this->rand_ctext(args), c1 = this->rand_ctext(args),
                c2(*public_key), cmult(*public_key);

    cmult = c1;
    cmult.multLowLvl(c0);

    // Run and time tests
    for (unsigned int i = 0; i < iters; i++) {

      c2 = cmult;
      c2.reLinearize();

      start = steady_clock::now();
      context->RescaleInPlace(c2); //NB: HElib doesn't have rescaling?
      end = steady_clock::now();

      long double d = duration_cast<measure_typ>(end - start).count();
      ret.runtimes.push_back(d);
    }
    */

      return ret;
    }

    TestResult test_bootstrap(const TestParams &tparams) const {
      // Probably should have a block like this at the start of every runtime
      // test
      TestResult ret;
      // unsigned int iters = tparams.iterations();
      //  Make sure to reserve!
      //  ret.errors.reserve(iters);
      return ret;
    }

    helib::Ctxt single_dot(const helib::Ctxt &ct0, const helib::Ctxt &ct1)
        const {
      unsigned int slot_count = context->getNSlots();
      helib::Ctxt ret(*public_key);
      helib::Ctxt tmp(*public_key);
      ret = ct0;
      ret *= ct1;
      for (size_t i = 1; i <= slot_count / 2; i <<= 1) {
        tmp = ret;
        helib::rotate(tmp, i);
        ret += tmp;
      }
      return ret;
    }

    // End-to-end functions
    TestResult test_dot(const TestParams &tparams) const {
      steady_clock::time_point start, end;
      unsigned int iters = tparams.iterations();
      TestResult ret;

      ret.runtimes.reserve(iters);

      unsigned int slot_count = context->getNSlots();
      unsigned int dot_vec_len =
          tparams.dot_len / slot_count + (tparams.dot_len % slot_count ? 1 : 0);

      vector<helib::Ctxt> v1, v2;
      v1.reserve(dot_vec_len);
      v2.reserve(dot_vec_len);
      for (size_t i = 0; i < dot_vec_len; i++) {
        v1.push_back(helib::Ctxt(*public_key));
        v2.push_back(helib::Ctxt(*public_key));
      }

      for (unsigned int j = 0; j < iters; j++) {
        helib::Ctxt dot_result(*public_key), tmp1(*public_key),
            tmp2(*public_key);

        vector<vector<double>> args1(v1.size());
        vector<vector<double>> args2(v2.size());

        for (size_t i = 0; i < v1.size(); i++) {
          v1[i] = this->rand_ctext(args1[i]);
          v2[i] = this->rand_ctext(args2[i]);
        }

        start = steady_clock::now();
        for (size_t i = 0; i < v1.size(); i++) {
          if (!i) {
            dot_result = single_dot(v1[i], v2[i]);
          }

          else {
            tmp1 = single_dot(v1[i], v2[i]);
            dot_result += tmp1;
          }
        }

        dot_result.reLinearize();

        // context->RescaleInPlace(tmp2);
        // ASK ABOUT RESCALING

        end = steady_clock::now();

        helib::PtxtArray pt(*context);
        pt.decrypt(dot_result, *secret_key);
        vector<double> enc_result;
        pt.store(enc_result);

        double realResult = 0.0f;
        for (size_t i = 0; i < v1.size(); i++) {
          realResult += plain_dot(args1[i], args2[i]);
        }
        ret.errors.push_back(abs(realResult - enc_result[0]) / abs(realResult));

        long double d = duration_cast<measure_typ>(end - start).count();
        ret.runtimes.push_back(d);
      }

      return ret;
    }

    TestResult test_linear_transform(const TestParams &tparams) const {

      steady_clock::time_point start, end;
      unsigned int iters = tparams.iterations();
      TestResult ret;
      // Make sure to reserve!
      ret.runtimes.reserve(iters);

      unsigned int slot_count = context->getNSlots();
      ret.batchsize = slot_count;
      ret.errors.reserve(slot_count * iters);

      size_t dimension = tparams.dimension();

      vector<vector<double>> input_matrix = generateUVector(dimension);
      // vector<vector<double>> input_matrix
      // {
      //   {0, 1, 2, 3, 4},
      //   {5, 6, 7, 8, 9},
      //   {10, 11, 12, 13, 14},
      //   {15, 16, 17, 18, 19},
      //   {20, 21, 22, 23, 24}
      // };

      double input_size = input_matrix[0].size();
      vector<vector<double>> diagonal_matrix(input_size,
                                             vector<double>(input_size));

      diagonal_matrix = getDiagonalMatrix(input_matrix);

#ifdef PROF_DEBUG
      std::cout << "Diagonal matrix" << std::endl;

      double dsize = diagonal_matrix[0].size();
      for (size_t i = 0; i < dsize; i++) {
        for (size_t j = 0; j < dsize; j++) {
          std::cout << '(' << diagonal_matrix[i][j] << ")";
        }
        std::cout << "\n";
      }
#endif

      vector<vector<double>> transpose_matrix(input_size,
                                              vector<double>(input_size));

      for (size_t i = 0; i < input_size; ++i) {
        for (size_t j = 0; j < input_size; ++j) {
          transpose_matrix[j][i] = diagonal_matrix[i][j];
        }
      }

#ifdef PROF_DEBUG
      std::cout << "Diagonal-transpose_matrix" << std::endl;

      for (size_t i = 0; i < input_size; i++) {
        for (size_t j = 0; j < input_size; j++) {
          std::cout << '(' << transpose_matrix[i][j] << ")";
        }
        std::cout << "\n";
      }
#endif

      // unsigned int iters = tparams.iterations();  -> was causing the 2X mult.
      unsigned int dot_len = input_size;

      vector<helib::Ctxt> diagonalCt_vec;
      for (size_t i = 0; i < dot_len; i++) {
        diagonalCt_vec.push_back(helib::Ctxt(*public_key));
      }

      vector<vector<double>> args2(diagonalCt_vec.size());
      for (size_t i = 0; i < diagonalCt_vec.size(); i++) {

        diagonalCt_vec[i] =
            this->custom_ctext(args2[i], transpose_matrix[i % dimension]);
      }

      std::vector<double> input_vector;

      input_vector.reserve(dimension);

      for (size_t i = 0; i < dimension; i++) {
        input_vector.push_back(i + 0.001 * i);
      }

      // vector<double> input_vector{0, 1, 2, 3, 4};
      vector<helib::Ctxt> inputCt_vec;
      for (size_t i = 0; i < dot_len; i++) {
        inputCt_vec.push_back(helib::Ctxt(*public_key));
      }
      vector<vector<double>> args3(inputCt_vec.size());

      for (size_t i = 0; i < inputCt_vec.size(); i++) {

        inputCt_vec[i] = this->custom_ctext(args3[i], input_vector);
      }

      helib::Ctxt ct_prime(*public_key);
      for (unsigned int k = 0; k < iters; k++) {

        start = steady_clock::now();
        // doing the actual multplication and addition of diagonals and input
        // vector
        vector<helib::Ctxt> ct_result;
        for (size_t i = 0; i < diagonalCt_vec.size(); i++) {
          ct_result.push_back(helib::Ctxt(*public_key));
        }
        ct_result[0] = inputCt_vec[0];
        ct_result[0] *= diagonalCt_vec[0];
        // helib::Ctxt temp_rot = inputCt_vec[i];

        // takes n rotations, multiplications, and additions, and has depth of
        // one multiplication, one rotation, and n additions
        for (size_t i = 1; i < diagonalCt_vec.size(); i++) {

          helib::Ctxt temp_rot = inputCt_vec[i];
          helib::rotate(temp_rot, i);
          ct_result[i] = diagonalCt_vec[i];
          ct_result[i] *= temp_rot;
        }

        helib::Ctxt ct_prime(*public_key);
        for (size_t i = 0; i < ct_result.size(); i++) {
          ct_prime += ct_result[i];
        }

        end = steady_clock::now();
        long double d = duration_cast<measure_typ>(end - start).count();
        ret.runtimes.push_back(d);
      }

      helib::PtxtArray result_pt(*context);

      result_pt.decrypt(ct_prime, *secret_key);

      vector<double> realResult =
          getLinearTransformVector(input_matrix, input_vector);
      vector<double> errors(realResult.size());

      std::vector<double> vec_result;
      result_pt.store(vec_result);
      for (size_t i = 0; i < input_size; i++) {
        errors[i] = abs(realResult[i] - vec_result[i]) / abs(realResult[i]);
#ifdef PROF_DEBUG
        std::cout << "Result: " << vec_result[i] << std::endl;
#endif
      }

      ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());

      return ret;
    }

    TestResult test_poly_eval(const TestParams &tparams) const {

      steady_clock::time_point start, end;
      unsigned int iters = tparams.iterations();
      TestResult ret;
      // Make sure to reserve!
      ret.runtimes.reserve(iters);
      unsigned int slot_count = context->getNSlots();
      ret.batchsize = slot_count;
      ret.errors.reserve(slot_count * iters);

      // generate random input.
      double x = 1.0f;
      helib::PtxtArray ptx(*context, x);
      helib::Ctxt ctx(*public_key);
      ptx.encrypt(ctx);
#ifdef PROF_DEBUG
      std::cout << "x = " << x << std::endl;
#endif
      // depth has to be ceil(log2(degree))
      const vector<double> &coeffs = tparams.polynomial_coeffs;
      size_t degree = coeffs.size() - 1;

      vector<helib::PtxtArray> plain_coeffs;
      for (size_t i = 0; i < degree + 1; i++) {
        plain_coeffs.push_back(helib::PtxtArray(*context));
      }

#ifdef PROF_DEBUG
      std::cout << "Polynomial = ";
      int counter = 0;
#endif

      for (size_t i = 0; i < degree + 1; i++) {
        plain_coeffs[i] = helib::PtxtArray(*context, coeffs[i]);
#ifdef PROF_DEBUG
        std::cout << "x^" << counter << " * (" << coeffs[i] << ")"
                  << ", ";
        counter++;
#endif
      }

#ifdef PROF_DEBUG
      cout << '\n';
#endif

      helib::PtxtArray plain_result(*context);
      vector<double> result;

      vector<helib::Ctxt> powers;
      for (size_t i = 0; i < degree + 1; i++) {
        powers.push_back(helib::Ctxt(*public_key));
      }

      // start timing here
#ifdef PROF_DEBUG
      vector<bool> assigned(degree + 1, false);
#endif

      for (unsigned int k = 0; k < iters; k++) {

        start = steady_clock::now();

        powers[1] = ctx;
#ifdef PROF_DEBUG
        assigned[0] = true;
        assigned[1] = true;
#endif

        vector<int> levels(degree + 1, 0);
        levels[1] = 0;
        levels[0] = 0;

        for (size_t i = 2; i <= degree; i++) {
          // compute x^i
          int minlevel = i;
          int cand = -1;
          for (size_t j = 1; j <= i / 2; j++) {
            int k = i - j;
            //
            int newlevel = std::max(levels[j], levels[k]) + 1;
            if (newlevel < minlevel) {
              cand = j;
              minlevel = newlevel;
            }
          }
          levels[i] = minlevel;
          // use cand
          if (cand < 0) {
            throw std::runtime_error("error");
          }
          // cout << "levels " << i << " = " << levels[i] << endl;
          // cand <= i - cand by definition
          helib::Ctxt temp = powers[cand];
          // temp.modDownToSet(powers[i - cand].getPrimeSet());
          temp *= powers[i - cand];

          // JST I think this is the right thing to add
          // Looks like an assignment was just missed
          powers[i] = temp;
#ifdef PROF_DEBUG
          assigned.at(i) = true;
#endif

          // no rescale, covered under *=?
          // evaluator->rescale_to_next_inplace(powers[i]);
        }

#ifdef PROF_DEBUG
        for (size_t w = 0; w < assigned.size(); w++) {
          if (!assigned[w]) {
            cout << "ERROR: power " << w << " not assigned" << endl;
          }
          assert(assigned[w]);
        }
#endif

        // std::cout << "All powers computed " << std::endl;

        helib::Ctxt enc_result(*public_key);
        // std::cout << "Encrypting first coeff...";
        plain_coeffs[0].encrypt(enc_result);
        // std::cout << "done.." << std::endl;
        helib::Ctxt temp(*public_key);

        // for (size_t i = 1; i <= degree; i++){
        //     decryptor->decrypt(powers[i], plain_result);
        //     ckks_encoder->decode(plain_result, result);
        //     std::cout << "power  = " << result[0] << endl;
        // }

        // result += a[i]*x[i]
        for (size_t i = 1; i <= degree; i++) {

          temp = powers[i];
          temp *= plain_coeffs[i];
#ifdef PROF_DEBUG
          helib::PtxtArray coeff_pta(*context);
          coeff_pta.decrypt(temp, *secret_key);
          vector<double> coeff_vec;
          coeff_pta.store(coeff_vec);
          double expected = coeffs[i];
          for (unsigned int j = 0; j < i - 1; j++) {
            expected *= coeffs[i];
          }

          cout << "\tProduct i: " << i << endl;
          cout << "\tIndicated product: " << coeff_vec[0] << endl;
          cout << "\tExpected: " << expected << endl;
          cout << "\tDifference: " << abs(coeff_vec[0] - expected) << '\n'
               << endl;

          coeff_pta.decrypt(powers[i], *secret_key);
          coeff_pta.store(coeff_vec);
          expected = x;
          for (unsigned int j = 0; j < i; j++) {
            expected *= x;
          }

          cout << "\tPower i: " << i << endl;
          cout << "\tIndicated power: " << coeff_vec[0] << endl;
          cout << "\tExpected: " << expected << endl;
          cout << "\tDifference: " << abs(coeff_vec[0] - expected) << '\n'
               << endl;
#endif

          enc_result.modDownToSet(temp.getPrimeSet());

          /* is this just rescaling with extra steps?
           *  enc_result.scale() = this->_params.scale();
           *  temp.scale() = this->_params.scale();
           */

          enc_result += temp;
        }

        // std::cout << "evaluation done" << std::endl;

        plain_result.decrypt(enc_result, *secret_key);
        plain_result.store(result);

#ifdef PROF_DEBUG
        std::cout << "Actual : " << result[0] << std::endl;
#endif
        // cout << "Actual : " << result[0] << ", Expected : " <<
        // expected_result
        // << ", diff : " << abs(result[0] - expected_result) << endl;

        end = steady_clock::now();
        long double d = duration_cast<measure_typ>(end - start).count();
        ret.runtimes.push_back(d);

        double realResult = getPlainPolyEval(x, coeffs);

        ret.errors.push_back(abs(realResult - result[0]) / abs(realResult));
      }

      return ret;
    }

    TestResult test_rotdot_product(const TestParams &tparams) const {

      steady_clock::time_point start, end;
      unsigned int iters = tparams.iterations();
      TestResult ret;
      // Make sure to reserve!
      ret.runtimes.reserve(iters);
      unsigned int slot_count = context->getNSlots();
      ret.batchsize = slot_count;
      ret.errors.reserve(slot_count * iters);

      std::vector<double> inputs;

      inputs.reserve(slot_count);

      for (size_t i = 0; i < slot_count; i++) {
        inputs.push_back(i + 0.001 * i);
      }

      helib::PtxtArray pt(*context, inputs);

      std::vector<double> weights;
      weights.reserve(slot_count);

      double realResult = 0.0f;
      for (size_t i = 0; i < slot_count; i++) {
        weights.push_back((slot_count & 1) ? -1.0 : 2.00);
        realResult += inputs.at(i) * weights.back();
      }

      helib::PtxtArray weight_pt(*context, weights);

      helib::Ctxt temp_ct(*public_key);
      helib::Ctxt ct(*public_key);
      helib::Ctxt weight_ct(*public_key);
      weight_pt.encrypt(weight_ct);

      for (unsigned int k = 0; k < iters; k++) {

        pt.encrypt(ct);

        start = steady_clock::now();

        ct *= weight_ct;
        // evaluator->multiply_plain_inplace(ct, weight_pt); -> does a SIMD
        // multiplication , for C-P dot product only

        // don't have rescaling
        // evaluator->rescale_to_next_inplace(ct);

        // Sum the slots of ciphertext using log(D) rotations
        for (size_t i = 1; i <= slot_count / 2; i <<= 1) {
          temp_ct = ct;
          helib::rotate(temp_ct, i);
          ct += temp_ct;
        }

        end = steady_clock::now();
        long double d = duration_cast<measure_typ>(end - start).count();
        ret.runtimes.push_back(d);

        helib::PtxtArray pt(*context);
        pt.decrypt(ct, *secret_key);
        vector<double> enc_result;
        pt.store(enc_result);
        double err = abs(realResult - enc_result[0]) / abs(realResult);
        ret.errors.push_back(err);
      }

      helib::PtxtArray result_pt(*context);
      result_pt.decrypt(ct, *secret_key);

      std::vector<double> vec_result;
      result_pt.store(vec_result);
#ifdef PROF_DEBUG
      std::cout << "Result: " << vec_result[0] << std::endl;
      std::cout << "True result: "
                << std::inner_product(inputs.begin(), inputs.end(),
                                      weights.begin(), 0.0)
                << std::endl;
#endif

      return ret;
    }

    TestResult test_encode_error(const TestParams &tparams) const {

      unsigned int iters = tparams.iterations();
      TestResult ret;
      // Make sure to reserve!
      unsigned int slot_count = context->getNSlots();
      ret.batchsize = slot_count;
      ret.errors.reserve(slot_count * iters);

      vector<double> arg0;
      helib::Ctxt c0(*public_key);

      for (unsigned int i = 0; i < iters; i++) {
        // Get random input
        c0 = this->rand_ctext(arg0);

        helib::PtxtArray pt(*context);

        pt.decrypt(c0, *secret_key);

        vector<double> enc_result;
        pt.store(enc_result);

        vector<double> errors(arg0.size());

        for (size_t j = 0; j < arg0.size(); j++) {
          errors[j] = abs(arg0[j] - enc_result[j]) / abs(arg0[j]);
        }

        ret.errors.insert(ret.errors.end(), errors.begin(), errors.end());
      }

      return ret;
    }

    // Sizes
    // Returns only a single element in their vector
    // Technically there's a precision loss with floating-point, but the results
    // shouldn't be nearly large enough for issues
    TestResult ciphertext_size(const TestParams &tparams) const {
      stringstream ss;
      vector<double> tmp;

      helib::Ctxt c = this->rand_ctext(tmp);
      c.writeTo(ss);

      // might be a better way to do this
      TestResult t;
      t.runtimes = {(long double)ss.str().size()};
      return t;
    }
    TestResult public_key_size(const TestParams &tparams) const {
      stringstream ss;
      public_key->writeTo(ss);
      TestResult t;

      t.runtimes = {(long double)ss.str().size()};
      return t;
    }
    TestResult secret_key_size(const TestParams &tparams) const {
      stringstream ss;
      secret_key->writeTo(ss);
      TestResult t;

      t.runtimes = {(long double)ss.str().size()};
      return t;
    }
    TestResult bsk_size(const TestParams &tparams) const {
      TestResult t;
      return t;
      // TODO
    }
    TestResult params_size(const TestParams &tparams) const {
      stringstream ss;
      context->writeTo(ss);
      TestResult t;
      t.runtimes = {(long double)ss.str().size()};
      return t;
    }

    TestResult ctext_modulus_bits() const {
      TestResult t;
      t.runtimes = {(long double)context->bitSizeOfQ()};
      return t;
      /*
      unsigned int cnt = 0;
      for(const auto & x : iset){
        cnt += log2(x);
      }
      t.runtimes = {(long double) cnt};
      return t;
      */
    }

    TestResult scaling_factor() const {
      TestResult t;
      t.runtimes = {(long double)scale_bits};
      return t;
    }

    TestResult num_columns() const {
      TestResult t;
      t.runtimes = {(long double)this->cols};
      return t;
    }
  };

#endif
